## SimPLoID

SimPLoID is a  simulation environment for network-based models of disease transmission together with a domain specific language for use by epidemiologists.
It was originally created by Ameen Almiftah and Dr. Felix Weitkämper as part of the former author's undergraduate thesis.

A detailed introduction to the system with a comprehensive treatment of the DSL and several examples can be found in the Ameen Almiftah's thesis. 


## Abstract

Combining probabilistic logic with network-based modeling can provide a comprehensive
and precise depiction of disease transmission and facilitate improved comprehension of the
fundamental dynamics of an epidemic. Probabilistic logic allows for the inclusion of uncer-
tainty and probability distributions in modeling, which can capture the stochastic nature
of individual behavior and the transmission dynamics of the disease. On the other hand,
network-based modeling allows for the modeling of interactions between individuals and
the spread of the disease through social connections. By combining these two modeling ap-
proaches, an epidemic’s underlying dynamics can be better understood. Additionally, the
integration of probabilistic logic and network-based modeling can help to address some of
the limitations of traditional compartmental models, such as the assumption of homoge-
neous mixing within the population.

In response to the emergence of SARS-CoV-2 and its various mutations, there was an
urgent need to simulate disease adaptation quickly. As a result, a domain-specific language
(Epidemical-DSL) and a simulation program (SimPLoID) were developed to create and
simulate compartmental models of infectious diseases. The aim of this thesis is to introduce
our DSL and show how it is constructed and translated to Problog, a probabilistic logic
programming framework. Additionally, how to define the dynamics of infectious diseases
and generate network-based models in combination with compartmental models. It also
discusses the key components of the simulation program and how it uses a combination of
Problog with the power of network-based modeling. In this work, we propose a domain-
specific language and a simulation program with integrated compartment models that uses
network-based modeling.
