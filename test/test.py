from problog import get_evaluatable
from problog.program import PrologString

model_type = """0.3::a.  query(a)."""
result = get_evaluatable().create_from(PrologString(model_type)).evaluate()
print(result)
