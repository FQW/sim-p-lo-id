from setuptools import setup

setup(
    name='epidemical-dsl',
    version='1.0',
    packages=['module'],
    url='',
    license='',
    author='ameen almiftah',
    author_email='ameen.almiftah@gmail.com',
    description='domain specific language for epidemiologists'
)
