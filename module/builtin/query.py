import time as ti

import _history

mst = ti.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <query>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                         ti.perf_counter()))
import _model_parser
import period
import person as psn
import _parser

query_inputs = {'query': {}}
query_list = []


def person(personX):
    """
    :usage: add person to query group, which is used in show module to filter the results
    :param personX: person to query
    :return: None
    """
    global query_list
    query_list.append(_parser.name(personX))
    return


def remove(person):
    """
    :usage: remove person from query group, which is used in show module to filter the results
    :param person: person to remove
    :return: None
    """
    global query_list
    query_list.remove(person)
    return


def group():
    """
    :usage: show query group, which is used in show module to filter the results
    :return: None
    """
    for person in query_list:
        print(person)
    return


def population(person='default'):
    """
    :usage: queries a specific/all person for the whole period
    :example: query population * all.person
              query population person1
    :param person: person to be queried
    :keyword person: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <susceptible> <{}> <susceptible={}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person, person))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_person(p, i)
        if not option == 'all':
            return
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_person(p, i)
    elif option == 'standard':
        _model_parser.qs('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_person(person, i)
    query_inputs['query']['population'] = person
    _history.add_param(query_inputs)
    return


def susceptible(person='default'):
    """
    :usage: queries susceptible for a specific person for the whole period
    :example: query susceptible * all.person
              query susceptible person1
    :param person: person to be queried
    :keyword person: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <susceptible> <{}> <susceptible={}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person, person))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_susceptible(p, i)
        if not option == 'all':
            return
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_susceptible(p, i)
    elif option == 'standard':
        _model_parser.qs('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_susceptible(person, i)
    query_inputs['query']['susceptible'] = person
    _history.add_param(query_inputs)
    return


def infected(person='default'):
    """
    :usage: queries infected for a specific person for the whole period
    :example: query infected * all.person
              query infected person1
    :param person: person to be queried
    :keyword person: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <infected> <{}> <infected={}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person, person))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_infected(p, i)
        if not option == 'all':
            return
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_infected(p, i)
    elif option == 'standard':
        _model_parser.qi('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_infected(person, i)
    query_inputs['query']['infected'] = person
    _history.add_param(query_inputs)
    return


def recovered(person='default'):
    """
    :usage: queries recovered for a specific person for the whole period
    :example: query recovered * all.person
              query recovered person1
    :param person: person to be queried
    :keyword person: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <recovered> <{}> <recovered={}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person, person))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_recovered(p, i)
        if not option == 'all':
            return
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_recovered(p, i)
    elif option == 'standard':
        _model_parser.qr('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_recovered(person, i)
    query_inputs['query']['recovered'] = person
    _history.add_param(query_inputs)
    return


def resistant(person='default'):
    """
    :usage: queries resistant for a specific person for the whole period
    :example: query resistant * all.person
              query resistant person1
    :param person: person to be queried
    :keyword person: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <resistant> <{}> <resistant={}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person, person))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_resistant(p, i)
        if not option == 'all':
            return
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_resistant(p, i)
    elif option == 'standard':
        _model_parser.qr('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_resistant(person, i)
    query_inputs['query']['resistant'] = person
    _history.add_param(query_inputs)
    return


def deceased(person='default'):
    """
    :usage: queries deceased for a specific person for the whole period
    :example: query deceased * all.person
              query deceased person1
    :param person: person to be queried
    :keyword person: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <deceased> <{}> <deceased={}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person, person))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_deceased(p, i)
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_deceased(p, i)
    elif option == 'standard':
        _model_parser.qd('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_deceased(person, i)
    query_inputs['query']['deceased'] = person
    _history.add_param(query_inputs)
    return


def vaccinated(person='default'):
    """
    :usage: queries vaccinated for a specific person for the whole period
    :example: query vaccinated * all.person
              query vaccinated person1
    :param person: person to be queried
    :keyword person: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <vaccinated> <{}> <vaccinated={}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person, person))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_vaccinated(p, i)
        if not option == 'all':
            return
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_vaccinated(p, i)
    elif option == 'standard':
        _model_parser.qv('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_vaccinated(person, i)
    query_inputs['query']['vaccinated'] = person
    _history.add_param(query_inputs)
    return


def mdi(person='default'):
    """
    :usage: queries mdi for a specific person for the whole period
    :example: query mdi * all.person
              query mdi person1
    :param person: person to be queried
    :keyword mdi: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <mdi> <{}> <mdi={}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person, mdi))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_mdi(p, i)
        if not option == 'all':
            return
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_mdi(p, i)
    elif option == 'standard':
        _model_parser.qm('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_mdi(person, i)
    query_inputs['query']['mdi'] = person
    _history.add_param(query_inputs)
    return


def exposed(person='default'):
    """
    :usage: queries exposed for a specific person for the whole period
    :example: query exposed * all.person
              query exposed person1
    :param person: person to be queried
    :keyword person: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <exposed> <{}> <exposed={}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person, person))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_exposed(p, i)
        if not option == 'all':
            return
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_exposed(p, i)
    elif option == 'standard':
        _model_parser.qe('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_exposed(person, i)
    query_inputs['query']['exposed'] = person
    _history.add_param(query_inputs)
    return


def carrier(person='default'):
    """
    :usage: queries carrier for a specific person for the whole period
    :example: query carrier * all.person
              query carrier person1
    :param person: person to be queried
    :keyword person: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <carrier> <{}> <carrier={}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person, person))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_carrier(p, i)
        if not option == 'all':
            return
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_carrier(p, i)
    elif option == 'standard':
        _model_parser.qc('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_carrier(person, i)
    query_inputs['query']['carrier'] = person
    _history.add_param(query_inputs)
    return


def carrier_infected(person='default'):
    """
    :usage: queries infected from a carier person for a specific person (by given person=name or all as default) for the whole period
    :example: query carrier_infected * all.person
              query carrier_infected person1
    :param person: person to be queried
    :keyword person: takes a name to query,  all to query all entered names or standard, to query everthing, its faster but might not give ordered results
    :return: None
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <query> <carrier_infected> <{}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, person))
    global modeltext
    option = person.lower()
    if option == 'all' or option == 'true' or option == 'positive' or option == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_infected_from_carrier(p, i)
        if not option == 'all':
            return
    if option == 'all' or option == 'none' or option == 'false' or option == 'negative':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for p in psn.population:
                _model_parser.query_not_infected_from_carrier(p, i)
    elif option == 'standard':
        _model_parser.qi('_', '_')
    else:
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.query_infected_from_carrier(person, i)
    query_inputs['query']['carrier_infected'] = person
    _history.add_param(query_inputs)
    return


def all(person='default'):
    model_letters = _parser.compartments(_model_parser.modeltext)
    if 'M' in model_letters:
        mdi(person=person)
    if 'S' in model_letters:
        susceptible(person=person)
    if 'E' in model_letters:
        exposed(person=person)
    if 'I' in model_letters:
        infected(person=person)
    if 'C' in model_letters:
        carrier(person=person)
        carrier_infected(person=person)
    if 'R' in model_letters:
        recovered(person=person)
        resistant(person=person)
    if 'V' in model_letters:
        vaccinated(person=person)
    if 'D' in model_letters:
        deceased(person=person)
    return


mft = ti.perf_counter()
_history._logger('{}[INFO]{}: 🟨 finished <query> :{} sec.'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter(), (mft - mst)))
