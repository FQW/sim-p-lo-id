import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <resistant>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                             time.perf_counter()))
import csv
import re
import _model_parser
import period as _period
import graph as grf
import person as prsn
import _parser
import recovered as rec

path = 'epidemical-dsl/module/data/'
resistant_population = []
countX = 0
x = 'X'
resistant_inputs = {'resistant': {}}


def activate():
    """
    :usage: adds recovered and resistant to the model
    :example: resistant activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <resistant> <activate> '.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    rec.activate()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <resistant> <activate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))
    return


def deactivate():
    """
    usage: remove recovered/resistant from the model
    example: resistant deactivate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <resistant> <deactivate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               time.perf_counter()))
    rec.deactivate()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <resistant> <deactivate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))
    return


def _activate(x):
    """
    :usage: adds recovered and resistant to the model
    :example: resistant activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <resistant> <_activate> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                   st, x))
    rec._activate(x)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <resistant> <_activate> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))
    return


def rate(chanceY, resistant='this'):
    """
    :usage: assigns the rate of resistance to the infection
    :example: resistant chance 1
    :param chanceY: the chance of getting resistance to the infection
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <resistant> <chance> <{}> <recovered={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, chanceY, resistant))
    rec.chance(chanceY, recovered=resistant)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <resistant> <chance> <{}> <recovered={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, chanceY, resistant, (ft - st)))
    return


def period(duration, resistant='this'):
    """
    :usage: assigns the average period length for getting resistance to the infection
    :example: resistant period 24
    :param duration: the average period length for getting resistance
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <resistant> <period> <{}> <recovered={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, duration, resistant))
    rec.period(duration, period=resistant)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <resistant> <period> <{}> <recovered={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), duration, resistant, (ft - st)))
    return


def plot(graph='pos', cases='default', save='default'):
    """
    :usage: graph (plot) resistant result data
    :example: resistant plot positive-days
    :example: resistant plot negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <resistant> <plot> <resistant={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.resistant0(), 'plot', cases=cases, save=save)
        grf._neg_graph(_model_parser.resistant0(), 'plot', cases=cases, save=save)
        grf._post_graph(_model_parser.resistant0(), 'plot', cases=cases, save=save)
        grf._negt_graph(_model_parser.resistant0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.resistant0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.resistant0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.resistant0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.resistant0(), 'plot', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <resistant> <plot> <resistant={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def bar(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) resistant result data
    :example: resistant bar positive-weeks
    :example: resistant bar negative-months
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <resistant> <bar> <resistant={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.resistant0(), 'bar', cases=cases, save=save)
        grf._neg_graph(_model_parser.resistant0(), 'bar', cases=cases, save=save)
        grf._post_graph(_model_parser.resistant0(), 'bar', cases=cases, save=save)
        grf._negt_graph(_model_parser.resistant0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.resistant0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.resistant0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.resistant0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.resistant0(), 'bar', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <resistant> <bar> <resistant={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def pie(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) resistant result data
    :example: resistant bar positive
    :example: resistant bar negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <resistant> <pie> <resistant={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.resistant0(), 'pie', cases=cases, save=save)
        grf._neg_graph(_model_parser.resistant0(), 'pie', cases=cases, save=save)
        grf._post_graph(_model_parser.resistant0(), 'pie', cases=cases, save=save)
        grf._negt_graph(_model_parser.resistant0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.resistant0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.resistant0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.resistant0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.resistant0(), 'pie', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <resistant> <pie> <resistant={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def scatter(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) resistant result data
    :example: resistant bar positive-times
    :example: resistant bar negative-times
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <resistant> <scatter> <resistant={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.resistant0(), 'scatter', cases=cases, save=save)
        grf._neg_graph(_model_parser.resistant0(), 'scatter', cases=cases, save=save)
        grf._post_graph(_model_parser.resistant0(), 'scatter', cases=cases, save=save)
        grf._negt_graph(_model_parser.resistant0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.resistant0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.resistant0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.resistant0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.resistant0(), 'scatter', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <resistant> <scatter> <resistant={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def person(personX, population='add'):
    """
    :usage: adds a resistant person to the population, at period start
    :example: resistant person p1
    :param personX: person to be added
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <resistant> <person> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  st,
                                                                  personX))
    global countX
    population = population.lower()
    countX += 1
    personX = _parser.name(personX)
    resistant_population.append(personX)
    if population == 'add' or population == 'append':
        prsn.population.append(personX)
    _model_parser.rs0(personX, _period.startX)
    resistant_inputs['resistant']['person'] = personX
    _history.add_param(resistant_inputs)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <resistant> <person> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), personX, (ft - st)))

    return


def count():
    """
    :usage: prints total resistant count
    :return: None
    """
    _history._logger(
        '{}[INFO]{}: 🟩 <vaccinated> <count> -> count={}'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), countX))
    print('total resistant population count:', countX)


def group(sizeX, population='add'):
    """
    :usage: generates a population of the given size
    :example: population group 100
    :param sizeX: size of the group
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <resistant> <group> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 st, sizeX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            person('t' + str(i), population=population)
        resistant_inputs['resistant']['size'] = sizeX
        _history.add_param(resistant_inputs)
    else:
        print('invalid population size')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <resistant> <group> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), sizeX, (ft - st)))
    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: person file person.csv
    :param p_file: CSV file with a list of names, each name is a resistant person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <resistant> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                st, p_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                person(_parser.name(p_name), population='add')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <resistant> <file> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_file, (ft - st)))
    return


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: person library person.csv
    :param p_library: CSV file with a list of names, each name is a resistant person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <resistant> <library> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                   time.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_file(p_library, 'resistant')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <resistant> <library> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_library, (ft - st)))
    return


def query(persons):
    """
    :usage: queries resistant population from a file for the whole period
    :example: resistant query person.csv
    :param persons: csv file with a list of people
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <resistant> <query> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 st,
                                                                 persons))
    global modeltext
    if not str(persons).endswith('.csv'):
        persons += '.csv'
    try:
        with open(persons) as personsfile:
            subject = csv.reader(personsfile)
            subjectsList = list(subject)
        for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
            for j in range(len(subjectsList)):
                _model_parser.query_resistant(str(subjectsList[j][0]), i)
        resistant_inputs['resistant']['query'] = persons
        _history.add_param(resistant_inputs)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <resistant> <query> <{} :{} sec.>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), persons, (ft - st)))
    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <resistant> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                          time.perf_counter(), (mft - mst)))
