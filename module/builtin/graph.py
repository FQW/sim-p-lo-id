import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <graph>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                         time.perf_counter()))

import re
import matplotlib.pyplot as plt
import _model_parser as mp
import _parser
import period
import run
import show as sho

c_t = time.localtime()
current_time = time.strftime("%y-%m-%d-%H-%M-%S", c_t)


def _axis(dict):
    """
    :usage: sets up the x and y-axis for the plot and sort them in order
    :param dict: takes a dictionary of type {person, time} or {time:person}, the dictionary consists categorized results
    :return: the sorted x and y-axies for the plot
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <graph> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  st, dict))
    x_axis = []
    y_axis = []
    for key, value in dict.items():
        if len(value) > 0:
            x_axis.append(key)
            y_axis.append(len(value))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <{}> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               ft, dict, (ft - st)))
    return x_axis, y_axis


def _pos_graph(query_type, graph_type, cases='default', save='default'):
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <graph> <_pos_graph> <{}> <{}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, query_type, graph_type, save))
    for r in run.results:
        positive_persons_per_week, _, _, _ = _parser.sort(r, query_type)
        print('graphing... 📉')
        print('please wait... ⏳')
        cases = cases.lower()
        cases_list = []
        for v in positive_persons_per_week.values():
            cases_list.append(v)
        x_axis, y_axis = _axis(positive_persons_per_week)
        try:
            if re.search('bar', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of ' + query_type)
                    plt.title(str(query_type).capitalize())
                    plt.bar(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'new' or cases == 'all':
                    y_axis = sho._sum_list(sho._list_new(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of new ' + query_type)
                    plt.title('New ' + str(query_type).capitalize())
                    plt.bar(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'removed' or cases == 'all':
                    y_axis = sho._sum_list(sho._list_remove(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of removed ' + query_type)
                    plt.title('Removed ' + str(query_type).capitalize())
                    plt.bar(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|pos|save', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/positive-' + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/positive-' + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
            if re.search('plot', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of ' + query_type)
                    plt.title(str(query_type).capitalize())
                    plt.plot(x_axis, y_axis)
                    plt.tight_layout()
                if cases == 'new' or cases == 'all':
                    y_axis = sho._sum_list(sho._list_new(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of new ' + query_type)
                    plt.title('New ' + str(query_type).capitalize())
                    plt.plot(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'removed' or cases == 'all':
                    y_axis = sho._sum_list(sho._list_remove(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of removed ' + query_type)
                    plt.title('Removed ' + str(query_type).capitalize())
                    plt.plot(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|pos|save', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/positive-' + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/positive-' + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
            if re.search('sc', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of ' + query_type)
                    plt.title(str(query_type).capitalize())
                    plt.scatter(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'new' or cases == 'all':
                    y_axis = sho._sum_list(sho._list_new(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of new ' + query_type)
                    plt.title('New ' + str(query_type).capitalize())
                    plt.scatter(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'removed' or cases == 'all':
                    y_axis = sho._sum_list(sho._list_remove(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of removed ' + query_type)
                    plt.title('Removed ' + str(query_type).capitalize())
                    plt.scatter(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|pos|save', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/positive-' + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/positive-' + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
            if re.search('pie', graph_type, re.IGNORECASE):
                plt.legend(title=period._base_string(), labels=y_axis, bbox_to_anchor=(1, 0), loc="lower left")
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of ' + query_type)
                    plt.title(str(query_type).capitalize())
                    plt.pie(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'new' or cases == 'all':
                    y_axis = sho._sum_list(sho._list_new(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of new ' + query_type)
                    plt.title('New ' + str(query_type).capitalize())
                    plt.pie(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'removed' or cases == 'all':
                    y_axis = sho._sum_list(sho._list_remove(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of removed ' + query_type)
                    plt.title('Removed ' + str(query_type).capitalize())
                    plt.pie(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|pos|save', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/positive-' + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/positive-' + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
        except ValueError:
            print('value error')
            _history._logger('{}[ERROR]{}: 🛑 ValueError >'.format(
                time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter()))

    if re.search('plot', graph_type, re.IGNORECASE):
        if re.search('graph|true|yes|log|pos|save', save, re.IGNORECASE):
            plt.savefig(
                'epidemical-dsl/module/docs/positive-' + query_type + '-' + graph_type + '-' + current_time + '.png')
            print(
                'epidemical-dsl/module/docs/positive-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
        plt.show()
        print('creating next graph... 📉')
        print('please wait... ⏳')
    print('done ⌛')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <graph> <_pos_graph> <{}> <{}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, query_type, graph_type, save, (ft - st)))
    return


def _neg_graph(query_type, graph_type, cases='default', save='default'):
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <graph> <_neg_graph> <{}> <{}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, query_type, graph_type, save))
    for r in run.results:
        _, negative_persons_per_week, _, _ = _parser.sort(r, query_type)
        print('graphing... 📈')
        print('please wait... ⏳')
        cases = cases.lower()
        cases_list = []
        for v in negative_persons_per_week.values():
            cases_list.append(v)
        x_axis, y_axis = _axis(negative_persons_per_week)
        try:
            if re.search('bar', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.clf()
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of non-' + query_type)
                    plt.title('Non-' + str(query_type).capitalize())
                    plt.bar(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'new' or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._list_new(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of new non-' + query_type)
                    plt.title('New non-' + str(query_type).capitalize())
                    plt.bar(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'removed' or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._list_remove(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of removed non-' + query_type)
                    plt.title('Removed non-' + str(query_type).capitalize())
                    plt.bar(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|neg|save', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/negative-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/negative-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
            if re.search('plot', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of non-' + query_type)
                    plt.title('Non-' + str(query_type).capitalize())
                    plt.plot(x_axis, y_axis)
                    plt.tight_layout()
                if cases == 'new' or cases == 'all':
                    y_axis = sho._sum_list(sho._list_new(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of new non-' + query_type)
                    plt.title('New non-' + str(query_type).capitalize())
                    plt.plot(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'removed' or cases == 'all':
                    y_axis = sho._sum_list(sho._list_remove(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of removed non-' + query_type)
                    plt.title('Removed non-' + str(query_type).capitalize())
                    plt.plot(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|neg|save', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/negative-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/negative-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
            if re.search('scatter', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.clf()
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of non-' + query_type)
                    plt.title('Non-' + str(query_type).capitalize())
                    plt.scatter(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'new' or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._list_new(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of new non-' + query_type)
                    plt.title('New non-' + str(query_type).capitalize())
                    plt.scatter(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'removed' or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._list_remove(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of removed non-' + query_type)
                    plt.title('Removed non-' + str(query_type).capitalize())
                    plt.scatter(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|neg|save', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/negative-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/negative-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
            if re.search('pie', graph_type, re.IGNORECASE):
                plt.legend(title=period._base_string(), labels=y_axis, bbox_to_anchor=(1, 0), loc="lower left")
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.clf()
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of non-' + query_type)
                    plt.title('Non-' + str(query_type).capitalize())
                    plt.pie(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'new' or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._list_new(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of new non-' + query_type)
                    plt.title('New non-' + str(query_type).capitalize())
                    plt.pie(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if cases == 'removed' or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._list_remove(cases_list))
                    plt.xlabel(period._base_string())
                    plt.ylabel('Number of removed non-' + query_type)
                    plt.title('Removed non-' + str(query_type).capitalize())
                    plt.pie(x_axis, y_axis)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|neg|save', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/negative-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/negative-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
        except ValueError:
            print('value error')
            _history._logger('{}[ERROR]{}: 🛑 ValueError >'.format(
                time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter()))

    if re.search('plot', graph_type, re.IGNORECASE):
        if re.search('graph|true|yes|log|neg|save', save, re.IGNORECASE):
            plt.savefig(
                'epidemical-dsl/module/docs/negative-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png')
            print(
                'epidemical-dsl/module/docs/negative-' + query_type + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
        plt.show()
        print('creating next graph... 📈')
        print('please wait... ⏳')
    print('done ⌛')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <graph> <_neg_graph> <{}> <{}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, query_type, graph_type, save, (ft - st)))
    return


def _post_graph(query_type, graph_type, cases='default', save='default'):
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <graph> <_neg_graph> <{}> <{}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, query_type, graph_type, save))
    for r in run.results:
        _, _, positive_times_per_person, _ = _parser.sort(r, query_type)
        print('graphing... 📊')
        print('please wait... ⏳')
        cases = cases.lower()
        cases_list = []
        for v in positive_times_per_person.values():
            cases_list.append(v)
        x_axis, y_axis = _axis(positive_times_per_person)
        try:
            if re.search('bar', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.clf()
                    plt.xlabel(query_type)
                    plt.ylabel('Number of ' + period._base_string() + 's')
                    plt.title('Positive ' + period._base_string() + 's')
                    plt.bar(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('freq|time', cases, re.IGNORECASE) or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._range(cases_list))
                    plt.xlabel(query_type)
                    plt.ylabel('Frequency')
                    plt.title('Positive')
                    plt.bar(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|pos|save|time', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/positive-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/positive-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
            if re.search('plot', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.clf()
                    plt.xlabel(query_type)
                    plt.ylabel('Number of ' + period._base_string() + 's')
                    plt.title('Positive ' + period._base_string() + 's')
                    plt.plot(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                if re.search('freq|time', cases, re.IGNORECASE) or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._range(cases_list))
                    plt.xlabel(query_type)
                    plt.ylabel('Frequency')
                    plt.title('Positive')
                    plt.plot(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|pos|save|time', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/positive-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/positive-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
            if re.search('scatter', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.clf()
                    plt.xlabel(query_type)
                    plt.ylabel('Number of ' + period._base_string() + 's')
                    plt.title('Positive ' + period._base_string() + 's')
                    plt.scatter(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('freq|time', cases, re.IGNORECASE) or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._range(cases_list))
                    plt.xlabel(query_type)
                    plt.ylabel('Frequency')
                    plt.title('Positive')
                    plt.scatter(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|pos|save|time', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/positive-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/positive-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
            if re.search('pie', graph_type, re.IGNORECASE):
                plt.legend(title=period._base_string(), labels=y_axis, bbox_to_anchor=(1, 0), loc="lower left")
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.clf()
                    plt.xlabel(query_type)
                    plt.ylabel('Number of ' + period._base_string() + 's')
                    plt.title('Positive ' + period._base_string() + 's')
                    plt.pie(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('freq|time', cases, re.IGNORECASE) or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._range(cases_list))
                    plt.xlabel(query_type)
                    plt.ylabel('Frequency')
                    plt.title('Positive')
                    plt.pie(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|pos|save|time', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/positive-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/positive-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
        except ValueError:
            print('value error')
            _history._logger('{}[ERROR]{}: 🛑 ValueError >'.format(
                time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter()))

    if re.search('plot', graph_type, re.IGNORECASE):
        if re.search('graph|true|yes|log|pos|save|time', save, re.IGNORECASE):
            plt.savefig(
                'epidemical-dsl/module/docs/positive-' + period._base_string() + 's-' + query_type + '-' + graph_type + '-' + current_time + '.png')
            print(
                'epidemical-dsl/module/docs/positive-' + period._base_string() + 's-' + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
        plt.show()
        print('creating next graph... 📉')
        print('please wait... ⏳')
    print('done ⌛')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <graph> <_post_graph> <{}> <{}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, query_type, graph_type, save, (ft - st)))
    return


def _negt_graph(query_type, graph_type, cases='default', save='default'):
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <graph> <_neg_graph> <{}> <{}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, query_type, graph_type, save))
    for r in run.results:
        _, _, _, negative_times_per_person = _parser.sort(r, query_type)
        print('graphing... 📊')
        print('please wait... ⏳')
        cases = cases.lower()
        cases_list = []
        for v in negative_times_per_person.values():
            cases_list.append(v)
        x_axis, y_axis = _axis(negative_times_per_person)
        try:
            if re.search('bar', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.clf()
                    plt.xlabel('not ' + query_type)
                    plt.ylabel('Number of ' + period._base_string() + 's')
                    plt.title('Negative ' + period._base_string() + 's')
                    plt.bar(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('freq|time', cases, re.IGNORECASE) or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._range(cases_list))
                    plt.xlabel(query_type)
                    plt.ylabel('Frequency')
                    plt.title('Negative')
                    plt.bar(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|neg|save|time', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/negative-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/negative-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
            if re.search('plot', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.xlabel('not ' + query_type)
                    plt.ylabel('Number of ' + period._base_string() + 's')
                    plt.title('Negative ' + period._base_string() + 's')
                    plt.plot(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                if re.search('freq|time', cases, re.IGNORECASE) or cases == 'all':
                    y_axis = sho._sum_list(sho._range(cases_list))
                    plt.xlabel(query_type)
                    plt.ylabel('Frequency')
                    plt.title('Negative')
                    plt.plot(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
            if re.search('scatter', graph_type, re.IGNORECASE):
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.clf()
                    plt.xlabel('not ' + query_type)
                    plt.ylabel('Number of ' + period._base_string() + 's')
                    plt.title('Negative ' + period._base_string() + 's')
                    plt.scatter(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('freq|time', cases, re.IGNORECASE) or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._range(cases_list))
                    plt.xlabel(query_type)
                    plt.ylabel('Frequency')
                    plt.title('Negative')
                    plt.scatter(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|neg|save|time', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/negative-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/negative-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + '-' + graph_type + '-' + current_time + '.png saved!')

            if re.search('pie', graph_type, re.IGNORECASE):
                plt.legend(title=period._base_string(), labels=y_axis, bbox_to_anchor=(1, 0), loc="lower left")
                if cases == 'default' or cases == 'all' or cases == 'total':
                    plt.clf()
                    plt.xlabel('not ' + query_type)
                    plt.ylabel('Number of ' + period._base_string() + 's')
                    plt.title('Negative ' + period._base_string() + 's')
                    plt.pie(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('freq|time', cases, re.IGNORECASE) or cases == 'all':
                    plt.clf()
                    y_axis = sho._sum_list(sho._range(cases_list))
                    plt.xlabel(query_type)
                    plt.ylabel('Frequency')
                    plt.title('Negative')
                    plt.pie(x_axis, y_axis)
                    plt.xticks(rotation=45)
                    plt.tight_layout()
                    plt.show()
                if re.search('graph|true|yes|log|neg|save|time', save, re.IGNORECASE):
                    plt.savefig(
                        'epidemical-dsl/module/docs/negative-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + '-' + graph_type + '-' + current_time + '.png')
                    print(
                        'epidemical-dsl/module/docs/negative-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
        except ValueError:
            print('value error')
            _history._logger('{}[ERROR]{}: 🛑 ValueError >'.format(
                time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter()))

    if re.search('plot', graph_type, re.IGNORECASE):
        if re.search('graph|true|yes|log|neg|save|time', save, re.IGNORECASE):
            plt.savefig(
                'epidemical-dsl/module/docs/negative-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + '-' + graph_type + '-' + current_time + '.png')
            print(
                'epidemical-dsl/module/docs/negative-' + period._base_string() + 's-' + mp.inf_var_name() + query_type + '-' + graph_type + '-' + current_time + '.png saved!')
        plt.show()
        print('creating next graph... 📈')
        print('please wait... ⏳')
    print('done ⌛')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <graph> <_negt_graph> <{}> <{}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, query_type, graph_type, save, (ft - st)))
    return


def _two_by_two_graph(query_type, pos, neg, t_pos, t_neg, positive='default', negative='default',
                      positive_times='default', negative_times='default'):
    """
    :usage: plots four plots in one figure
    :param query_type:
    :param pos: positive persons results' dictionary
    :param neg: negative persons results' dictionary
    :param t_pos: positive times results' dictionary
    :param t_neg: negative times results' dictionary
    :keyword positive: for the persons positive plot, takes bar, plot, scatter, pie
    :keyword negative: for the times negative plot, takes bar, plot, scatter, pie
    :keyword positive_times: for the persons positive plot, takes bar, plot, scatter, pie
    :keyword negative_times: for the times negative plot, takes bar, plot, scatter, pie
    :return: none
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <_two_by_two_graph> <{}> <pos> <neg> <t_pos> <t_neg>  <positive={}> <negative={}> <positive_times={}> <negative_times={}>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, query_type, positive, negative, positive_times,
            negative_times))
    print('                      📊📉')
    print('creating the graph... 📈📊')
    print('please wait... ⏳')
    figure, axis = plt.subplots(2, 2)
    pos_x, pos_y = _axis(pos)
    neg_x, neg_y = _axis(neg)
    t_pos_x, t_pos_y = _axis(t_pos)
    t_neg_x, t_neg_y = _axis(t_neg)
    axis[0, 0].set_title(str(query_type).capitalize())
    axis[0, 0].set_xlabel(period._base_string())
    axis[0, 0].set_ylabel('Number of ' + query_type)
    axis[0, 1].set_title('Not ' + str(query_type).capitalize())
    axis[0, 1].set_xlabel(period._base_string())
    axis[0, 1].set_ylabel('Number of not ' + query_type)
    axis[1, 0].set_title('Positive ' + period._base_string() + 's')
    axis[1, 0].set_xlabel(query_type)
    axis[1, 0].set_ylabel('Number of ' + period._base_string() + 's')
    axis[1, 1].set_title('Negative ' + period._base_string() + 's')
    axis[1, 1].set_xlabel('Not ' + query_type)
    axis[1, 1].set_ylabel('Number of ' + period._base_string() + 's')

    if re.search('bar', positive, re.IGNORECASE):
        axis[0, 0].bar(pos_x, pos_y)
    elif re.search('plot|default', positive, re.IGNORECASE):
        axis[0, 0].plot(pos_x, pos_y)
    elif re.search('scatter', positive, re.IGNORECASE):
        axis[0, 0].scatter(pos_x, pos_y)
    elif re.search('pie', positive, re.IGNORECASE):
        axis[0, 0].pie(pos_x, labels=pos_y, radius=1.1, autopct='%.1f%%')
        axis[0, 0].legend(title=period._base_string(), labels=pos_y, bbox_to_anchor=(1, 0), loc="lower left")
        axis[0, 0].set_xlabel(' ')
        axis[0, 0].set_ylabel(' ')
    else:
        axis[0, 0].plot(pos_x, pos_y)
    if re.search('bar', negative, re.IGNORECASE):
        axis[0, 1].bar(neg_x, neg_y)
    elif re.search('plot|default', negative, re.IGNORECASE):
        axis[0, 1].plot(neg_x, neg_y)
    elif re.search('scatter', negative, re.IGNORECASE):
        axis[0, 1].scatter(neg_x, neg_y)
    elif re.search('pie', negative, re.IGNORECASE):
        axis[0, 1].pie(neg_x, labels=neg_y, radius=1.1, autopct='%.1f%%')
        axis[0, 1].legend(title=period._base_string(), labels=neg_y, bbox_to_anchor=(1, 0), loc="lower left")
        axis[0, 1].set_xlabel(' ')
        axis[0, 1].set_ylabel(' ')
    else:
        axis[0, 1].plot(neg_x, neg_y)
    if re.search('bar', positive_times, re.IGNORECASE):
        axis[1, 0].bar(t_pos_x, t_pos_y)
    elif re.search('plot|default', positive_times, re.IGNORECASE):
        axis[1, 0].plot(t_pos_x, t_pos_y)
    elif re.search('scatter', positive_times, re.IGNORECASE):
        axis[1, 0].scatter(t_pos_x, t_pos_y)
    elif re.search('pie', positive_times, re.IGNORECASE):
        axis[1, 0].plot(t_pos_x, t_pos_y)
    else:
        axis[1, 0].plot(t_pos_x, t_pos_y)
    if re.search('bar', negative_times, re.IGNORECASE):
        axis[1, 1].bar(t_neg_x, t_neg_y)
    elif re.search('plot|default', negative_times, re.IGNORECASE):
        axis[1, 1].plot(t_neg_x, t_neg_y)
    elif re.search('scatter', negative_times, re.IGNORECASE):
        axis[1, 1].scatter(t_neg_x, t_neg_y)
    elif re.search('pie', negative_times, re.IGNORECASE):
        axis[1, 1].plot(t_neg_x, t_neg_y)
    else:
        axis[1, 1].plot(t_neg_x, t_neg_y)
    plt.tight_layout()
    print('done ⌛')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <_two_by_two_graph> <pos> <neg> <t_pos> <t_neg>  <positive={}> <negative={}> <positive_times={}> <negative_times={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, query_type, positive, negative, positive_times,
            negative_times, (ft - st)))
    return


def query(search_query, negative='default', negativeTimes='default', positive='default', positiveTimes='default',
          save='default', show='default'):
    """
    :usage: graphs susceptible population
    :example: graph query graph scatter.positive negative.plot
    :example: graph query covidgraph new.cases bar,positive plot.negative
    :param search_query: query to graph
    :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
    :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
    :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
    :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
    :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
    :keyword show: takes default, graph, true, yes to show the graph, default is default show
    :return: none
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <query> <{}> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, search_query, negative, negativeTimes, positive,
            positiveTimes))
    for r in run.results:
        susceptible_per_week, not_susceptible_per_week, positive_times_per_susceptible, negative_times_per_susceptible = _parser.sort(
            r, search_query)
        _two_by_two_graph(search_query, susceptible_per_week, not_susceptible_per_week, positive_times_per_susceptible,
                          negative_times_per_susceptible, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print('susceptible-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/susceptible-' + current_time + '.png', bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <query> <{}> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, search_query, negative, negativeTimes, positive,
            positiveTimes, (ft - st)))
    return


def susceptible(negative='default', negativeTimes='default', positive='default', positiveTimes='default',
                save='default', show='default'):
    """
    :usage: graphs susceptible population
    :example: graph susceptible scatter.positive negative.plot
    :example: graph susceptible new.cases bar,positive plot.negative
    :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
    :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
    :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
    :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
    :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
    :keyword show: takes default, graph, true, yes to show the graph, default is default show
    :return: none
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <susceptible> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes))
    for r in run.results:
        susceptible_per_week, not_susceptible_per_week, positive_times_per_susceptible, negative_times_per_susceptible = _parser.sort(
            r, 'susceptible')
        _two_by_two_graph('susceptible', susceptible_per_week, not_susceptible_per_week, positive_times_per_susceptible,
                          negative_times_per_susceptible, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(mp.inf_var_name() + 'susceptible-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + mp.inf_var_name() + 'susceptible-' + current_time + '.png',
                    bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <susceptible> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes,
            (ft - st)))
    return


def infected(negative='default', negativeTimes='default', positive='default', positiveTimes='default', save='default',
             show='default'):
    """
        :usage: graphs infected population
        :example: graph infected scatter.positive negative.plot
        :example: graph infected new.cases bar,positive plot.negative
        :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
        :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
        :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
        :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
        :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
        :keyword show: takes default, graph, true, yes to show the graph, default is default show
        :return: none
        """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <infected> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes))
    for r in run.results:
        graph_per_week, not_graph_per_week, positive_times_per_graph, negative_times_per_graph = _parser.sort(
            r, mp.infected0())
        _two_by_two_graph('infected', graph_per_week, not_graph_per_week,
                          positive_times_per_graph,
                          negative_times_per_graph, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(mp.inf_var_name() + 'infected-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + mp.inf_var_name() + 'infected-' + current_time + '.png',
                    bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <infected> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes,
            (ft - st)))
    return


def recovered(negative='default', negativeTimes='default', positive='default', positiveTimes='default', save='default',
              show='default'):
    """
        :usage: graphs recovered population
        :example: graph recovered scatter.positive negative.plot
        :example: graph recovered new.cases bar,positive plot.negative
        :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
        :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
        :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
        :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
        :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
        :keyword show: takes default, graph, true, yes to show the graph, default is default show
        :return: none
        """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <recovered> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes))
    for r in run.results:
        recovered_per_week, not_recovered_per_week, positive_times_per_recovered, negative_times_per_recovered = _parser.sort(
            r, mp.recovered0())
        _two_by_two_graph('recovered', recovered_per_week, not_recovered_per_week,
                          positive_times_per_recovered,
                          negative_times_per_recovered, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(mp.inf_var_name() + 'recovered-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + mp.inf_var_name() + 'recovered-' + current_time + '.png',
                    bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <recovered> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes,
            (ft - st)))
    return


def vaccinated(negative='default', negativeTimes='default', positive='default', positiveTimes='default', save='default',
               show='default'):
    """
        :usage: graphs vaccinated population
        :example: graph vaccinated scatter.positive negative.plot
        :example: graph vaccinated new.cases bar,positive plot.negative
        :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
        :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
        :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
        :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
        :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
        :keyword show: takes default, graph, true, yes to show the graph, default is default show
        :return: none
        """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <vaccinated> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes))
    for r in run.results:
        vaccinated_per_week, not_vaccinated_per_week, positive_times_per_vaccinated, negative_times_per_vaccinated = _parser.sort(
            r, mp.vaccinated0())
        _two_by_two_graph('vaccinated', vaccinated_per_week, not_vaccinated_per_week,
                          positive_times_per_vaccinated,
                          negative_times_per_vaccinated, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(mp.inf_var_name() + 'vaccinated-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + mp.inf_var_name() + 'vaccinated-' + current_time + '.png',
                    bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <vaccinated> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes,
            (ft - st)))
    return


def deceased(negative='default', negativeTimes='default', positive='default', positiveTimes='default', save='default',
             show='default'):
    """
        :usage: graphs deceased population
        :example: graph deceased scatter.positive negative.plot
        :example: graph deceased new.cases bar,positive plot.negative
        :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
        :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
        :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
        :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
        :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
        :keyword show: takes default, graph, true, yes to show the graph, default is default show
        :return: none
        """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <deceased> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes))
    for r in run.results:
        deceased_per_week, not_deceased_per_week, positive_times_per_deceased, negative_times_per_deceased = _parser.sort(
            r, mp.deceased0())
        _two_by_two_graph('deceased', deceased_per_week, not_deceased_per_week,
                          positive_times_per_deceased,
                          negative_times_per_deceased, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(mp.inf_var_name() + 'deceased-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + mp.inf_var_name() + 'deceased-' + current_time + '.png',
                    bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <deceased> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes,
            (ft - st)))
    return


def mdi(negative='default', negativeTimes='default', positive='default', positiveTimes='default', save='default',
        show='default'):
    """
        :usage: graphs mdi population
        :example: graph mdi scatter.positive negative.plot
        :example: graph mdi new.cases bar,positive plot.negative
        :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
        :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
        :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
        :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
        :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
        :keyword show: takes default, graph, true, yes to show the graph, default is default show
        :return: none
        """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <mdi> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes))
    for r in run.results:
        mdi_per_week, not_mdi_per_week, positive_times_per_mdi, negative_times_per_mdi = _parser.sort(
            r, mp.mdi0())
        _two_by_two_graph('mdi', mdi_per_week, not_mdi_per_week,
                          positive_times_per_mdi,
                          negative_times_per_mdi, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(mp.inf_var_name() + 'mdi-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + mp.inf_var_name() + 'mdi-' + current_time + '.png',
                    bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <mdi> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes,
            (ft - st)))
    return


def exposed(negative='default', negativeTimes='default', positive='default', positiveTimes='default', save='default',
            show='default'):
    """
        :usage: graphs exposed population
        :example: graph exposed scatter.positive negative.plot
        :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
        :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
        :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
        :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
        :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
        :keyword show: takes default, graph, true, yes to show the graph, default is default show
        :return: none
        """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <exposed> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes))
    for r in run.results:
        exposed_per_week, not_exposed_per_week, positive_times_per_exposed, negative_times_per_exposed = _parser.sort(
            r, mp.exposed0())
        _two_by_two_graph('exposed', exposed_per_week, not_exposed_per_week,
                          positive_times_per_exposed,
                          negative_times_per_exposed, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(mp.inf_var_name() + 'exposed-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + mp.inf_var_name() + 'exposed-' + current_time + '.png',
                    bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <exposed> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes,
            (ft - st)))
    return


def carrier(negative='default', negativeTimes='default', positive='default', positiveTimes='default', save='default',
            show='default'):
    """
        :usage: graphs carrier population
        :example: graph carrier scatter.positive negative.plot
        :example: graph carrier new.cases bar,positive plot.negative
        :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
        :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
        :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
        :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
        :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
        :keyword show: takes default, graph, true, yes to show the graph, default is default show
        :return: none
        """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <carrier> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes))
    for r in run.results:
        carrier_per_week, not_carrier_per_week, positive_times_per_carrier, negative_times_per_carrier = _parser.sort(
            r, mp.carrier0())
        _two_by_two_graph('carrier', carrier_per_week, not_carrier_per_week,
                          positive_times_per_carrier,
                          negative_times_per_carrier, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(mp.inf_var_name() + 'carrier-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + mp.inf_var_name() + 'carrier-' + current_time + '.png',
                    bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <carrier> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes,
            (ft - st)))
    return


def carrier_infected(negative='default', negativeTimes='default', positive='default', positiveTimes='default',
                     save='default', show='default'):
    """
        :usage: graphs infected (from a carrier) population
        :example: graph carrier_infected scatter.positive negative.plot
        :example: graph carrier_infected new.cases bar,positive plot.negative
        :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
        :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
        :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
        :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
        :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
        :keyword show: takes default, graph, true, yes to show the graph, default is default show
        :return: none
        """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <carrier_infected> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes))
    for r in run.results:
        infected_from_carrier_per_week, not_infected_from_carrier_per_week, positive_times_per_infected_from_carrier, negative_times_per_infected_from_carrier = _parser.sort(
            r, mp.infected_from_carrier0())
        _two_by_two_graph('carrier_infected', infected_from_carrier_per_week, not_infected_from_carrier_per_week,
                          positive_times_per_infected_from_carrier,
                          negative_times_per_infected_from_carrier, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(mp.inf_var_name() + 'carrier_infected-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + mp.inf_var_name() + 'carrier_infected-' + current_time + '.png',
                    bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <carrier_infected> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes,
            (ft - st)))
    return


def resistant(negative='default', negativeTimes='default', positive='default', positiveTimes='default', save='default',
              show='default'):
    """
        :usage: graphs resistant population
        :example: graph resistant scatter.positive negative.plot
        :example: graph resistant new.cases bar,positive plot.negative
        :keyword cases: takes default, all, total, new, removed, frequency|times ex all.cases
        :keyword positive: positive cases, takes bar, plot, scatter, pie, default is plot
        :keyword negative: negative cases, takes bar, plot, scatter, pie, default is plot
        :keyword positiveTimes: positive times, takes bar, plot, scatter, pie, default is plot
        :keyword negativeTimes: negative times, takes bar, plot, scatter, pie, default is plot
        :keyword save: takes default, graph, true, yes, log to save the graph in /docs, default no save
        :keyword show: takes default, graph, true, yes to show the graph, default is default show
        :return: none
        """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <graph> <resistant> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> '.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes))
    for r in run.results:
        resistant_per_week, not_resistant_per_week, positive_times_per_resistant, negative_times_per_resistant = _parser.sort(
            r, mp.resistant0())
        _two_by_two_graph('resistant', resistant_per_week, not_resistant_per_week,
                          positive_times_per_resistant,
                          negative_times_per_resistant, positive=positive, negative=negative,
                          positive_times=positiveTimes,
                          negative_times=negativeTimes)

    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(mp.inf_var_name() + 'resistant-' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + mp.inf_var_name() + 'resistant-' + current_time + '.png',
                    bbox_inches='tight')
    if re.search('graph|true|yes|default', show, re.IGNORECASE):
        plt.show()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <graph> <resistant> <negative={}> <negativeTimes={}> <positive={}>  <positiveTimes={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, negative, negativeTimes, positive, positiveTimes,
            (ft - st)))
    return


def plot(search_query, graph='pos', cases='default', save='default'):
    """
    :usage: graph (plot) query result data
    :example: graph plot infected positive-days
    :example: graph plot coronainfected negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword save: takes graph|true|yes|log|pos|save|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <graph> <plot> <graph={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        _pos_graph(search_query, 'plot', cases=cases, save=save)
        _neg_graph(search_query, 'plot', cases=cases, save=save)
        _post_graph(search_query, 'plot', cases=cases, save=save)
        _negt_graph(search_query, 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _pos_graph(search_query, 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _neg_graph(search_query, 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _post_graph(search_query, 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _negt_graph(search_query, 'plot', cases=cases, save=save)
    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(search_query + 'plot' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + search_query + 'plot' + current_time + '.png', bbox_inches='tight')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <graph> <plot> <graph={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def bar(search_query, graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) graph result data
    :example: graph bar infected positive-weeks
    :example: graph bar infected negative-months
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword save: takes graph|true|yes|log|pos|save|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <graph> <bar> <graph={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        _pos_graph(search_query, 'bar', cases=cases, save=save)
        _neg_graph(search_query, 'bar', cases=cases, save=save)
        _post_graph(search_query, 'bar', cases=cases, save=save)
        _negt_graph(search_query, 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _pos_graph(search_query, 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _neg_graph(search_query, 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _post_graph(search_query, 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _negt_graph(search_query, 'bar', cases=cases, save=save)
    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(search_query + 'bar' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + search_query + 'bar' + current_time + '.png', bbox_inches='tight')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <graph> <bar> <graph={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def pie(search_query, graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) graph result data
    :example: graph pie infected positive
    :example: graph pie infected negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword save: takes graph|true|yes|log|pos|save|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <graph> <pie> <graph={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        _pos_graph(search_query, 'pie', cases=cases, save=save)
        _neg_graph(search_query, 'pie', cases=cases, save=save)
        _post_graph(search_query, 'pie', cases=cases, save=save)
        _negt_graph(search_query, 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _pos_graph(search_query, 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _neg_graph(search_query, 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _post_graph(search_query, 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _negt_graph(search_query, 'pie', cases=cases, save=save)
    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(search_query + 'pie' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + search_query + 'pie' + current_time + '.png', bbox_inches='tight')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <graph> <pie> <graph={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def scatter(search_query, graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) graph result data
    :example: graph scatter infected positive-times
    :example: graph scatter infected negative-times
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword save: takes graph|true|yes|log|pos|save|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <graph> <scatter> <graph={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        _pos_graph(search_query, 'scatter', cases=cases, save=save)
        _neg_graph(search_query, 'scatter', cases=cases, save=save)
        _post_graph(search_query, 'scatter', cases=cases, save=save)
        _negt_graph(search_query, 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _pos_graph(search_query, 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _neg_graph(search_query, 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _post_graph(search_query, 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        _negt_graph(search_query, 'scatter', cases=cases, save=save)
    if re.search('graph|true|yes|log', save, re.IGNORECASE):
        print(search_query + 'scatter' + current_time + '.png saved in epidemical-dsl/module/docs/')
        plt.savefig('epidemical-dsl/module/docs/' + search_query + 'scatter' + current_time + '.png',
                    bbox_inches='tight')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <graph> <scatter> <graph={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


mft = time.perf_counter()
_history._logger('{}[INFO]{}: 🟨 finished <graph> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                   time.perf_counter(), (mft - mst)))
