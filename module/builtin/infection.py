import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <infection>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                             time.perf_counter()))

import _model_parser
import _parser
import configparser
import infected
import run
import show
import period
import contact
import person

infection_inputs = {'infection': {}}
models_path = 'epidemical-dsl/module/models/'

config = configparser.SafeConfigParser(allow_no_value=True)
try:
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
infection_nameX = config['INFECTION']['name']
infection_variantX = config['INFECTION']['variant']
infection_transmissionX = config['INFECTION']['transmission']

def name(infectionY, name='this'):
    """
    :usage: sets the name of the infection
    :example: infection name covid
              infection name * name.()
    :param name: takes Clear to clear the name
    :param infectionY: sets the name of the infection, if '.' then no name is set
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infection> <name> <{}> <infection={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, infectionY, name))
    option = name.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        _model_parser.infection_name(config['INFECTION']['name'])
        infection_inputs['infection']['name'] = config['INFECTION']['name']
        _history.add_param(infection_inputs)
        print('infection name is set to {}'.format(config['INFECTION']['name']))
    elif option == 'none' or option == 'clear' or option == 'false':
        _model_parser.infection_name('')
        print("infection name is cleared")
        infection_inputs['infection']['name'] = ''
    else:
        infectionY = _parser.name(infectionY)
        _model_parser.infection_name(infectionY)
        print("infection name is set to {0}".format(infectionY))
        infection_inputs['infection']['name'] = infectionY
    _history.add_param(infection_inputs)
    return


def variant(variantY, name='this'):
    """
    :usage: sets the name of the infection variant
    :example: infection variant delta
              infection variant xxxx none.name
    :param variantY: the name of the infection variant, if '.' then no name is set
    :param name: takes Clear to clear the name o
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infection> <variant> <{}> <infection={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, variantY, name))
    option = name.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        _model_parser.infection_variant(config['INFECTION']['name'])
        infection_inputs['infection']['name'] = config['INFECTION']['name']
        _history.add_param(infection_inputs)
        print('infection name is set to {}'.format(config['INFECTION']['name']))
    elif option == 'none' or option == 'clear' or option == 'false':
        _model_parser.infection_variant('')
        infection_inputs['infection']['variant'] = ''
        print("variant name is cleared")
    else:
        variantY = _parser.name(variantY)
        _model_parser.infection_variant(variantY)
        infection_inputs['infection']['variant'] = variantY
        print("variant name is set to {0}".format(variantY))
    _history.add_param(infection_inputs)
    return


def transmission(transmissionY, name='this'):
    """
    :usage: sets the transmission of the infection
    :example: infection transmission droplet
    :param transmissionY: sets the transmission of the infection, if '.' then no type is set
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <infection> <name> <{}> <infection={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, transmissionY, name))
    option = name.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        _model_parser.infection_transmission_type(config['INFECTION']['transmission'])
        infection_inputs['infection']['transmission'] = config['INFECTION']['transmission']
        _history.add_param(infection_inputs)
        print('infection transmission is set to {}'.format(config['INFECTION']['transmission']))
    elif option == 'none' or option == 'clear' or option == 'false':
        _model_parser.infection_transmission_type('')
        print("transmission type is cleared")
        infection_inputs['infection']['transmission'] = ''
    else:
        transmissionY = _parser.name(transmissionY)
        _model_parser.infection_transmission_type(transmissionY)
        print("transmission type is set to {0}".format(transmissionY))
        infection_inputs['infection']['transmission'] = transmissionY
    _history.add_param(infection_inputs)
    return


def default(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: infection default
    :return: None
    """
    config = configparser.SafeConfigParser(allow_no_value=True)
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    name(config['INFECTION']['name'])
    variant(config['INFECTION']['variant'])
    transmission(config['INFECTION']['transmission'])
    return


def rate():
    """
    :usage: prints the R0 value
    :example: infection rate
    :return: None
    """
    c = (int(contact.countX) / len(person.population)) / int(period.endX)
    r = float(infected.chanceX) * c * float(infected.incubation_periodX)
    print("R0 =", r)
    #current_query = _model_parser.infected0()
    #try:
        #for infected_per_week, _, _, _ in run._sample(current_query):
            #total_cases = show._sum_row(infected_per_week)
            #avg = 0
            #for i in range(len(total_cases)):
            #    avg += total_cases[i]
            #avg_inf = avg/int(period.endX)
            #k = math.log(int(total_cases[int(t+1)]) - math.log(int(total_cases[t])))
            #rt = math.pow(math.e, k * infected.incubation_periodX)
    #except StopIteration:
        #_history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),time.perf_counter()))
    return

def R0():
    """
    :usage:print the R0 value
    :example: infection R0
    :return: None
    """
    rate()
    return

mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <infection> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                          time.perf_counter(), (mft - mst)))
