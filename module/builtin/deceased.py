import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <deceased>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), mst))
import csv
import configparser
import re
import _parser
import _model_parser
import period
import graph as grf
import person as prsn

models_path = 'epidemical-dsl/module/models/'
path = 'epidemical-dsl/module/data/'
deceased_population = []
countX = 0
x = 'X'
st = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <deceased>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
config = configparser.SafeConfigParser(allow_no_value=True)
try:
    mst = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <deceased>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
rateX = config['DECEASED']['rate']
deceased_inputs = {'deceased': {}}
ft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <deceased> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft,
                                                         (ft - st)))


def activate():
    """
    :usage: adds deceased to the model
    :example: deceased activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <deceased> <activate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    for i in range(int(period.startX), int(period.endX), int(period.stepX)):
        _model_parser.d(x, rateX, i)
    deceased_inputs['deceased']['activate'] = 'true'
    _history.add_param(deceased_inputs)
    print('deceased compartment is activated')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <deceased> <activate> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, (ft - st)))
    return


def deactivate():
    """
    usage: remove deceased from the model
    example: deceased deactivate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <deceased> <deactivate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                              time.perf_counter()))
    _model_parser.remove_line('deceased')

    deceased_inputs['deceased']['activate'] = 'false'
    _history.add_param(deceased_inputs)
    print('deceased compartment is deactivated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <deceased> <deactivate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))
    return


def _activate(x):
    """
    :usage: adds deceased to the model
    :example: deceased activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <deceased> <_activate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    for i in range(int(period.startX), int(period.endX), int(period.stepX)):
        _model_parser.d(x, rateX, i)
    deceased_inputs['deceased']['activate'] = 'true'
    _history.add_param(deceased_inputs)
    print('deceased compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <deceased> <_activate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, (ft - st)))
    return


def rate(rateY, deceased='this'):
    """
    :usage: assigns the chance of dying from the infection
    :param rateY: the chance of dying from the infection
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <deceased> <rate> <{}> <deceased={}>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            st, rateY, deceased))
    global rateX
    n = rateY.replace('.', '', 1).replace('%', '')
    option = deceased.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        rateX = _parser.probability(config['DECEASED']['rate'])
        deceased_inputs['deceased']['rate'] = config['DECEASED']['rate']
        _history.add_param(deceased_inputs)
        print('deceased rate is set to {}'.format(config['DECEASED']['rate']))
    if str(n).isdigit():
        rateX = _parser.probability(rateY)
        deceased_inputs['deceased']['rate'] = rateX
        _history.add_param(deceased_inputs)
        print('death rate is set to ', rateX)
    else:
        print('invalid death rate')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <deceased> <rate> <{}> <deceased={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            ft, rateY, deceased, (ft - st)))
    return


def default(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: deceased default
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <deceased> <default>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    rate(config['DECEASED']['rate'])
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <deceased> <default> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, (ft - st)))
    return


def plot(graph='pos', cases='default', save='default'):
    """
    :usage: graph (plot) deceased result data
    :example: deceased plot positive-days
    :example: deceased plot negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <deceased> <plot> <deceased={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph('deceased', 'plot', cases=cases, save=save)
        grf._neg_graph('deceased', 'plot', cases=cases, save=save)
        grf._post_graph('deceased', 'plot', cases=cases, save=save)
        grf._negt_graph('deceased', 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph('deceased', 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph('deceased', 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph('deceased', 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph('deceased', 'plot', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <deceased> <plot> <deceased={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, graph, save, (ft - st)))


def bar(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) deceased result data
    :example: deceased bar positive-weeks
    :example: deceased bar negative-months
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <deceased> <bar> <deceased={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph('deceased', 'bar', cases=cases, save=save)
        grf._neg_graph('deceased', 'bar', cases=cases, save=save)
        grf._post_graph('deceased', 'bar', cases=cases, save=save)
        grf._negt_graph('deceased', 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph('deceased', 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph('deceased', 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph('deceased', 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph('deceased', 'bar', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <deceased> <bar> <deceased={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, graph, save, (ft - st)))


def pie(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) deceased result data
    :example: deceased bar positive
    :example: deceased bar negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <deceased> <pie> <deceased={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph('deceased', 'pie', cases=cases, save=save)
        grf._neg_graph('deceased', 'pie', cases=cases, save=save)
        grf._post_graph('deceased', 'pie', cases=cases, save=save)
        grf._negt_graph('deceased', 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph('deceased', 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph('deceased', 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph('deceased', 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph('deceased', 'pie', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <deceased> <pie> <deceased={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, graph, save, (ft - st)))


def scatter(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) deceased result data
    :example: deceased bar positive-times
    :example: deceased bar negative-times
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <deceased> <scatter> <deceased={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph('deceased', 'scatter', cases=cases, save=save)
        grf._neg_graph('deceased', 'scatter', cases=cases, save=save)
        grf._post_graph('deceased', 'scatter', cases=cases, save=save)
        grf._negt_graph('deceased', 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph('deceased', 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph('deceased', 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph('deceased', 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph('deceased', 'scatter', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <deceased> <scatter> <deceased={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, graph, save, (ft - st)))


def person(personX, population='add'):
    """
    :usage: adds a deceased person to the population, at period start
    :example: deceased person p1
    :param personX: person to be added
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <deceased> <person> <{}> '.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  st,
                                                                  personX))
    global countX
    population = population.lower()
    countX += 1
    personX = _parser.name(personX)
    if population == 'add' or population == 'append':
        prsn.population.append(personX)
    deceased_population.append(personX)
    _model_parser.d0(personX, period.startX)
    deceased_inputs['deceased']['person'] = personX
    _history.add_param(deceased_inputs)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <deceased> <person> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft,
            personX, (ft - st)))
    return


def count():
    """
    :usage: prints total deceased count
    :return: None
    """
    _history._logger(
        '{}[INFO]{}: 🟩 <deceased> <count> -> count={}'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               time.perf_counter(),
                                                               countX))
    print('total deceased population count:', countX)


def group(sizeX, population='add'):
    """
    :usage: generates a population of the given size
    :example: population group 100
    :param sizeX: size of the group
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <deceased> <group> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                st,
                                                                countX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            person('d' + str(i), population=population)
        deceased_inputs['deceased']['size'] = sizeX
        _history.add_param(deceased_inputs)
    else:
        print('invalid population size')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <deceased> <group> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft,
            countX, (ft - st)))
    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: person file person.csv
    :param p_file: CSV file with a list of names, each name is a deceased person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <deceased> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                               p_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                person(_parser.name(p_name), population='add')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <deceased> <file> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft,
            p_file, (ft - st)))
    return


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: person library person.csv
    :param p_library: CSV file with a list of names, each name is a deceased person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <deceased> <library> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  time.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_file(p_library, 'deceased')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <deceased> <library> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_library, (ft - st)))
    return


def query(persons):
    """
    :usage: queries deceased population from a file for the whole period
    :example: deceased query person.csv
    :param persons: csv file with a list of people
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <deceased> <query> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                st, persons))
    global modeltext
    if not str(persons).endswith('.csv'):
        persons += '.csv'
    try:
        with open(persons) as personsfile:
            subject = csv.reader(personsfile)
            subjectsList = list(subject)
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for j in range(len(subjectsList)):
                _model_parser.query_deceased(str(subjectsList[j][0]), i)
        deceased_inputs['deceased']['query'] = persons
        _history.add_param(deceased_inputs)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <deceased> <query> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 st, persons, (ft - st)))
    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <deceased> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                         time.perf_counter(), (mft - mst)))
