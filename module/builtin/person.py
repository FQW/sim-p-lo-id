import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <person>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                          time.perf_counter()))
import carrier as ca
import deceased as de
import exposed as ex
import infected as inf
import mdi as md
import recovered as rec
import susceptible as su
import vaccinated as va
import _model_parser
import period
import csv
import _parser

population = []
person_inputs = {'person': {}}
countX = 0
path = 'epidemical-dsl/module/data/'


def count():
    """
    :usage: prints total count
    :return: None
    """
    print('total person count:', countX)
    print('total population count:', len(population))


def name(personX):
    """
    :usage: adds a person to the population
    :example: population person p1
    :param personX: person to be added
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <person> <name> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                             personX))
    global countX
    countX += 1
    personX = _parser.name(personX)
    population.append(personX)
    _model_parser.p(personX, period.startX)
    person_inputs['person']['name'] = personX
    _history.add_param(person_inputs)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <person> <name> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft,
            personX, (ft - st)))
    return


def remove(person):
    """
    :usage: remove person from population, which is used in query, and removes person from prolog model text
    :param person: person to remove
    :return: None
    """
    global query_list
    population.remove(person)
    _model_parser.remove_line(person)
    return


def group(sizeX):
    """
    :usage: generates a population of the given size
    :example: person group 100
    :param sizeX: size of the group
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <person> <group> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                              sizeX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            name('p' + str(i))
        person_inputs['person']['size'] = sizeX
        _history.add_param(person_inputs)
    else:
        print('invalid population size')
    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: person file person.csv
    :param p_file: CSV file with a list of names, each name is a person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <person> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                             p_file))
    names_list = []
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                name(_parser.name(p_name))
                names_list.append(_parser.name(p_name))
    except FileNotFoundError:
        print('file not found')
    except FileExistsError:
        print('file does not exist')
    return names_list


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: person library person.csv
    :param p_library: CSV file with a list of names, each name is a carrier person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <library> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_file(p_library, 'person')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger('{}[ERROR]{}: 🛑 FileExistsError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <person> <library> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_library, (ft - st)))
    return


def carrier(personX):
    """
    :usaga: activate compartment for specific person with the current settings and period
    :param personX: person to be activated
    :return: None
    """
    ca._activate(personX)
    return


def deceased(personX):
    """
    :usaga: activate compartment for specific person with the current settings and period
    :param personX: person to be activated
    :return: None
    """
    de._activate(personX)
    return


def exposed(personX):
    """
    :usaga: activate compartment for specific person with the current settings and period
    :param personX: person to be activated
    :return: None
    """
    ex._activate(personX)
    return


def infected(personX):
    """
    :usaga: activate compartment for specific person with the current settings and period
    :param personX: person to be activated
    :return: None
    """
    inf._activate(personX)
    return


def mdi(personX):
    """
    :usaga: activate compartment for specific person with the current settings and period
    :param personX: person to be activated
    :return: None
    """
    md._activate(personX)
    return


def recovered(personX):
    """
    :usaga: activate compartment for specific person with the current settings and period
    :param personX: person to be activated
    :return: None
    """
    rec._activate(personX)
    return


def susceptible(personX):
    """
    :usaga: activate compartment for specific person with the current settings and period
    :param personX: person to be activated
    :return: None
    """
    su._activate(personX)
    return


def vaccinated(personX):
    """
    :usaga: activate compartment for specific person with the current settings and period
    :param personX: person to be activated
    :return: None
    """
    va._activate(personX)
    return


def new():
    """
    :usage: clear population list which used for query
    :return: None
    """
    global population
    population = []
    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <person> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter(), (mft - mst)))
