import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <susceptible>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               time.perf_counter()))
import csv
import configparser
import re
import _parser
import _model_parser
import period
import graph as grf
import person as prsn

path = 'epidemical-dsl/module/data/'
models_path = 'epidemical-dsl/module/models/'
susceptible_population = []
countX = 0
x = 'X'
st = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <deceased>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
config = configparser.SafeConfigParser(allow_no_value=True)
try:
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    _history._logger(
        '{}[ERROR]{}: 🛑 default.ini not found'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
percentX = config['SUSCEPTIBLE']['percent']
susceptible_inputs = {'susceptible': {}}
ft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <susceptible> loading default.ini :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, (ft - st)))


def activate():
    """
    :usage: adds susceptible to the model
    :example: susceptible activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <susceptible> <activate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               st))
    for i in range(int(period.startX), int(period.endX), int(period.stepX)):
        _model_parser.s(x, percentX, i)
    susceptible_inputs['susceptible']['activate'] = 'true'
    _history.add_param(susceptible_inputs)
    print('susceptible compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <susceptible> <activate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))

    return


def _activate(x):
    """
    :usage: adds susceptible to the model
    :example: susceptible activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <susceptible> <_activate> <{}>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, x))
    for i in range(int(period.startX), int(period.endX), int(period.stepX)):
        _model_parser.s(x, percentX, i)
    susceptible_inputs['susceptible']['activate'] = 'true'
    _history.add_param(susceptible_inputs)
    print('susceptible compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <susceptible> <_activate> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))

    return


def deactivate():
    """
    usage: remove susceptible from the model
    example: susceptible deactivate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <susceptible> <deactivate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter()))
    _model_parser.remove_line('susceptible')
    susceptible_inputs['susceptible']['activate'] = 'false'
    _history.add_param(susceptible_inputs)
    print('susceptible compartment is deactivated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <susceptible> <deactivate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))
    return


def percent(percentY, susceptible='this'):
    """
    :usage: assigns percentage of population who are susceptible to the infection
    :example: susceptible percent 0.95
    :param percentY: percentage of population who are susceptible to the infection
    :print: confirmation message
    :return: None
    """
    _history._logger('{}[INFO]{}: 🟩 <susceptible> <activate> <{}> <susceptible={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), percentY, susceptible))
    global percentX
    n = percentX.replace('.', '', 1).replace('%', '')
    option = susceptible.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        percentX = _parser.probability(config['SUSCEPTIBLE']['percent'])
        susceptible_inputs['susceptible']['percent'] = config['SUSCEPTIBLE']['percent']
        _history.add_param(susceptible_inputs)
        print('susceptible susceptible percent is set to {}'.format(config['SUSCEPTIBLE']['percent']))
    elif str(n).isdigit():
        percentX = _parser.probability(percentY)
        susceptible_inputs['susceptible']['percent'] = percentX
        _history.add_param(susceptible_inputs)
        print('susceptible percent is set to {}'.format(percentX))
    else:
        print('susceptible percent entry is invalid')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <susceptible> <percent> <{}> <susceptible={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), percentY, susceptible, (ft - st)))

    return


def default(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: susceptible default
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <susceptible> <default>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                st))
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    percent(config['SUSCEPTIBLE']['percent'])
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <susceptible> <default> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))

    return


def plot(graph='pos', cases='default', save='default'):
    """
    :usage: graph (plot) susceptible result data
    :example: susceptible plot positive-days
    :example: susceptible plot negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    _history._logger('{}[INFO]{}: 🟩 <susceptible> <plot> <vaccinated={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.susceptible0(), 'plot', cases=cases, save=save)
        grf._neg_graph(_model_parser.susceptible0(), 'plot', cases=cases, save=save)
        grf._post_graph(_model_parser.susceptible0(), 'plot', cases=cases, save=save)
        grf._negt_graph(_model_parser.susceptible0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.susceptible0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.susceptible0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.susceptible0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.susceptible0(), 'plot', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <susceptible> <plot> <susceptible={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def bar(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) susceptible result data
    :example: susceptible bar positive-weeks.susceptible
    :example: susceptible bar negative-months.susceptible all.save
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    _history._logger('{}[INFO]{}: 🟩 <susceptible> <bar> <vaccinated={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.susceptible0(), 'bar', cases=cases, save=save)
        grf._neg_graph(_model_parser.susceptible0(), 'bar', cases=cases, save=save)
        grf._post_graph(_model_parser.susceptible0(), 'bar', cases=cases, save=save)
        grf._negt_graph(_model_parser.susceptible0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.susceptible0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.susceptible0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.susceptible0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.susceptible0(), 'bar', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <susceptible> <bar> <susceptible={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def pie(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) susceptible result data
    :example: susceptible pie positive.susceptible
    :example: susceptible pie negative.susceptible
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    _history._logger('{}[INFO]{}: 🟩 <susceptible> <pie> <vaccinated={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.susceptible0(), 'pie', cases=cases, save=save)
        grf._neg_graph(_model_parser.susceptible0(), 'pie', cases=cases, save=save)
        grf._post_graph(_model_parser.susceptible0(), 'pie', cases=cases, save=save)
        grf._negt_graph(_model_parser.susceptible0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.susceptible0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.susceptible0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.susceptible0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.susceptible0(), 'pie', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <susceptible> <pie> <susceptible={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def scatter(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) susceptible result data
    :example: susceptible scatter positive-times.susceptible
    :example: susceptible scatter none.save
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    _history._logger('{}[INFO]{}: 🟩 <susceptible> <scatter> <vaccinated={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.susceptible0(), 'scatter', cases=cases, save=save)
        grf._neg_graph(_model_parser.susceptible0(), 'scatter', cases=cases, save=save)
        grf._post_graph(_model_parser.susceptible0(), 'scatter', cases=cases, save=save)
        grf._negt_graph(_model_parser.susceptible0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.susceptible0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.susceptible0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.susceptible0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.susceptible0(), 'scatter', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <susceptible> <scatter> <susceptible={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def person(personX, population='add'):
    """
    :usage: adds a susceptible person to the population, at period start
    :example: susceptible person p1
    :param personX: person to be added
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <susceptible> <person> <{}>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
            personX))
    _history._logger(
        '{}[INFO]{}: 🟩 <susceptible> <person> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                            time.perf_counter(), personX))
    global countX
    population = population.lower()
    countX += 1
    personX = _parser.name(personX)
    if population == 'add' or population == 'append':
        prsn.population.append(personX)
    susceptible_population.append(personX)
    _model_parser.s0(personX, period.startX)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <susceptible> <person> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), personX, (ft - st)))
    return


def count():
    """
    :usage: prints total susceptible count
    :return: None
    """
    _history._logger(
        '{}[INFO]{}: 🟩 <susceptible> <count> -> count={}'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  time.perf_counter(), countX))
    print('total susceptible population count:', countX)


def group(sizeX, population='add'):
    """
    :usage: generates a population of the given size
    :example: population group 100
    :param sizeX: size of the group
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <susceptible> <group> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                   st, sizeX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            person('s' + str(i), population=population)
        susceptible_inputs['susceptible']['size'] = sizeX
        _history.add_param(susceptible_inputs)
    else:
        print('invalid population size')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <susceptible> <group> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), sizeX, (ft - st)))
    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: person file person.csv
    :param p_file: CSV file with a list of names, each name is a susceptible person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <susceptible> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  st, p_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                person(_parser.name(p_name), population='add')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger('{}[ERROR]{}: 🛑 FileExistsError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <susceptible> <file> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_file, (ft - st)))
    return


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: person library person.csv
    :param p_library: CSV file with a list of names, each name is a susceptible person
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <susceptible> <library> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
        time.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_file(p_library, 'susceptible')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <susceptible> <library> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_library, (ft - st)))
    return


def query(persons):
    """
    :usage: queries susceptible population from a file for the whole period
    :example: susceptible query person.csv
    :param persons: csv file with a list of people
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <susceptible> <query> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                   st,
                                                                   persons))
    global modeltext
    if not str(persons).endswith('.csv'):
        persons += '.csv'
    try:
        with open(persons) as personsfile:
            subject = csv.reader(personsfile)
            subjectsList = list(subject)
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            for j in range(len(subjectsList)):
                _model_parser.query_susceptible(str(subjectsList[j][0]), i)
        susceptible_inputs['susceptible']['query'] = persons
        _history.add_param(susceptible_inputs)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger('{}[ERROR]{}: 🛑 FileExistsError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  time.perf_counter()))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <susceptible> <query> <{} :{} sec.>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), persons, (ft - st)))
    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <susceptible> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                            time.perf_counter(), (mft - mst)))
