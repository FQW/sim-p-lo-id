import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <carrier>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), mst))
import csv
import configparser
import _parser
import _model_parser
import period as _period
import re
import graph as grf
import person as prsn
import infected

path = 'epidemical-dsl/module/data/'
models_path = 'epidemical-dsl/module/models/'
carrier_population = []
countX = 0
x = 'X'
st = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟩 started <carrier> loading {}.ini'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                             _parser.config_file_name))
config = configparser.SafeConfigParser(allow_no_value=True)
try:
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
chanceX = config['CARRIER']['chance']
percentX = config['CARRIER']['percent']
recursionX = config['CARRIER']['recursion']
carrier_periodX = config['CARRIER']['period']
carrier_inputs = {'carrier': {}}
ft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <carrier> loading {}.ini :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, _parser.config_file_name, (ft - st)))


def activate():
    """
    usage: adds carrier to the model
    example: carrier activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <activate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
    # _model_parser.remove_line('recovered')
    # _model_parser.remove_line('resistant')
    recursionZ = abs((float(recursionX) - float(percentX)) / (1 - float(percentX)))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.c(x, chanceX, percentX, recursionZ, infected.incubation_periodX, i)
        if int(carrier_periodX) < 0:
            _model_parser.c0_period(x, i + 1, i)
        elif int(carrier_periodX) == 0:
            _model_parser.not_c(x, i)
            _model_parser.not_c_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(carrier_periodX)))):
                if int(i) == (int(i) + int(carrier_periodX) - int(a) - 1):
                    continue
                _model_parser.c0_period(x, i + int(carrier_periodX) - a - 1, i)
            _model_parser.not_c_period(x, i + int(carrier_periodX), i)
    carrier_inputs['carrier']['activate'] = 'true'
    _history.add_param(carrier_inputs)
    print('carrier compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <carrier> <activate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))

    return


def deactivate():
    """
    usage: remove carrier from the model
    example: carrier deactivate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <deactivate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                             time.perf_counter()))
    _model_parser.remove_line('carrier')

    carrier_inputs['carrier']['activate'] = 'false'
    _history.add_param(carrier_inputs)
    print('carrier compartment is deactivated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <carrier> <deactivate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))
    return


def _activate(x):
    """
    usage: adds carrier to the model
    example: carrier activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <_activate> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), x))
    recursionZ = abs((float(recursionX) - float(percentX)) / (1 - float(percentX)))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.c(x, chanceX, percentX, recursionZ, infected.incubation_periodX, i)
        if int(carrier_periodX) < 0:
            _model_parser.c0_period(x, i + 1, i)
        elif int(carrier_periodX) == 0:
            _model_parser.not_c(x, i)
            _model_parser.not_c_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(carrier_periodX)))):
                if int(i) == (int(i) + int(carrier_periodX) - int(a) - 1):
                    continue
                _model_parser.c0_period(x, i + int(carrier_periodX) - a - 1, i)
            _model_parser.not_c_period(x, i + int(carrier_periodX), i)
    carrier_inputs['carrier']['activate'] = 'true'
    _history.add_param(carrier_inputs)
    print('carrier compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <carrier> <_activate> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))

    return


def chance(chanceY, carrier='this'):
    """
    :usage: sets the chance of getting infected every time contact occurs
    :param chanceY: chance of getting infected every time contact occurs
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <carrier> <chance> <{}> <carrier={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, chanceY, carrier))
    global chanceX
    n = chanceY.replace('.', '', 1).replace('%', '')
    option = carrier.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        chanceX = _parser.probability(config['CARRIER']['chance'])
        carrier_inputs['carrier']['chance'] = config['CARRIER']['chance']
        _history.add_param(carrier_inputs)
        print('carrier chance is set to {}'.format(config['CARRIER']['chance']))
    elif str(n).isdigit():
        chanceX = _parser.probability(chanceY)
        carrier_inputs['carrier']['chance'] = chanceX
        _history.add_param(carrier_inputs)
        print('carrier chance is set to ', chanceX)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <carrier> <chance> <{}> <carrier={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, chanceY, carrier, (ft - st)))
    return


def percent(percentage, carrier='this'):
    """
    usage: assigns percentage of the infected population who can carry the infection
    example: carrier percent 0.1
    :param percentage: percentage of the infected population who can carry the infection
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <carrier> <percent> <{}> <carrier={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), percentage, carrier))
    global percentX
    n = percentage.replace('.', '', 1).replace('%', '')
    option = carrier.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        percentX = _parser.probability(config['CARRIER']['percent'])
        carrier_inputs['carrier']['percent'] = config['CARRIER']['percent']
        _history.add_param(carrier_inputs)
        print('carrier percent is set to {}'.format(config['CARRIER']['percent']))
    elif str(n).isdigit():
        percentX = _parser.probability(percentage)
        carrier_inputs['carrier']['percent'] = percentX
        _history.add_param(carrier_inputs)
        print('carrier percent is set to ', percentX)
    else:
        print('invalid carrier percent')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <carrier> <percent> <{}> <carrier={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), percentage, carrier, (ft - st)))
    return


def period(duration, period='this'):
    """
    usage: assigns the average period length for carrying the infection
    example: carrier period 7
    :param duration: the average period length for carrying the infection
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <carrier> <percent> <{}> <period={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), duration, period))
    global carrier_periodX
    option = period.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
            print('carrier period is set to {}'.format(config['CARRIER']['period']))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        carrier_periodX = _parser.probability(config['CARRIER']['period'])
        carrier_inputs['carrier']['period'] = config['CARRIER']['period']
        _history.add_param(carrier_inputs)
    elif str(duration).isdigit():
        carrier_periodX = int(duration)
        carrier_inputs['carrier']['period'] = carrier_periodX
        _history.add_param(carrier_inputs)
        print('carrier duration is set to ', carrier_periodX, _period.base_stringX + 's')
    elif str(duration) == '-1':
        print('carrier duration is set to infinity')
        carrier_periodX = -1
    else:
        print('invalid carrier period')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <carrier> <percent> <{}> <period={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), duration, period, (ft - st)))
    return


def recursion(recursionY, carrier='this'):
    """
    usage: assigns the chance of an infected person getting back to the carrier state
    example: carrier recursion 0.5
    :param recursionY: the chance of an infected person getting back to the carrier state
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <carrier> <percent> <{}> <carrier={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), recursionY, carrier))
    global recursionX
    n = recursionY.replace('.', '', 1).replace('%', '')
    option = carrier.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))

        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        recursionX = _parser.probability(config['CARRIER']['recursion'])
        carrier_inputs['carrier']['recursion'] = config['CARRIER']['recursion']
        _history.add_param(carrier_inputs)
        print('carrier recursion is set to {}'.format(config['CARRIER']['recursion']))
    elif str(n).isdigit():
        recursionX = _parser.probability(recursionY)
        carrier_inputs['carrier']['recursion'] = recursionX
        _history.add_param(carrier_inputs)
        print('carrier recursion chance is set to ', recursionX)
    else:
        print('invalid carrier recursion chance')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <carrier> <percent> <{}> <carrier={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), recursionY, carrier, (ft - st)))
    return


def default(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: carrier default
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <default> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), x))
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    percent(config['CARRIER']['percent'])
    period(config['CARRIER']['period'])
    recursion(config['CARRIER']['recursion'])
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <carrier> <default> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))
    return


def plot(graph='pos', cases='default', save='default'):
    """
    :usage: graph (plot) carrier result data
    :example: carrier plot positive-days
    :example: carrier plot negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <carrier> <plot> <carrier={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.carrier0(), 'plot', cases=cases, save=save)
        grf._neg_graph(_model_parser.carrier0(), 'plot', cases=cases, save=save)
        grf._post_graph(_model_parser.carrier0(), 'plot', cases=cases, save=save)
        grf._negt_graph(_model_parser.carrier0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.carrier0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.carrier0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.carrier0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.carrier0(), 'plot', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <carrier> <plot> <carrier={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def bar(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) carrier result data
    :example: carrier bar positive-weeks
    :example: carrier bar negative-months
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <carrier> <bar> <carrier={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.carrier0(), 'bar', cases=cases, save=save)
        grf._neg_graph(_model_parser.carrier0(), 'bar', cases=cases, save=save)
        grf._post_graph(_model_parser.carrier0(), 'bar', cases=cases, save=save)
        grf._negt_graph(_model_parser.carrier0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.carrier0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.carrier0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.carrier0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.carrier0(), 'bar', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <carrier> <bar> <carrier={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def pie(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) carrier result data
    :example: carrier bar positive
    :example: carrier bar negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <carrier> <pie> <carrier={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.carrier0(), 'pie', cases=cases, save=save)
        grf._neg_graph(_model_parser.carrier0(), 'pie', cases=cases, save=save)
        grf._post_graph(_model_parser.carrier0(), 'pie', cases=cases, save=save)
        grf._negt_graph(_model_parser.carrier0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.carrier0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.carrier0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.carrier0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.carrier0(), 'pie', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <carrier> <pie> <carrier={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def scatter(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) carrier result data
    :example: carrier bar positive-times
    :example: carrier bar negative-times
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <carrier> <scatter> <carrier={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.carrier0(), 'scatter', cases=cases, save=save)
        grf._neg_graph(_model_parser.carrier0(), 'scatter', cases=cases, save=save)
        grf._post_graph(_model_parser.carrier0(), 'scatter', cases=cases, save=save)
        grf._negt_graph(_model_parser.carrier0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.carrier0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.carrier0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.carrier0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.carrier0(), 'scatter', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <carrier> <scatter> <carrier={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def person(personX, population='add'):
    """
    :usage: adds a carrier person to the population, at period start
    :example: carrier person p1
    :param personX: person to be added
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <person> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),

                                                                time.perf_counter(), personX))
    global countX
    population = population.lower()
    countX += 1
    personX = _parser.name(personX)
    if population == 'add' or population == 'append':
        prsn.population.append(personX)
    carrier_population.append(personX)
    _model_parser.c0(personX, _period.startX)
    carrier_inputs['carrier']['person'] = personX
    _history.add_param(carrier_inputs)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <carrier> <person> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), personX, (ft - st)))
    return


def count():
    """
    :usage: prints total carrier count
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <count>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                          time.perf_counter()))
    _history._logger(
        '{}[INFO]{}: 🟩 <vaccinated> <count> -> count={}'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), countX))
    print('total carrier population count:', countX)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <carrier> <count> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))


def group(sizeX, population='add'):
    """
    :usage: generates a population of the given size
    :example: population group 100
    :param sizeX: size of the group
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <group> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               time.perf_counter(), sizeX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            person('c' + str(i), population=population)
        carrier_inputs['carrier']['size'] = sizeX
        _history.add_param(carrier_inputs)
    else:
        print('invalid population size')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <carrier> <group> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), sizeX, (ft - st)))
    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: person file person.csv
    :param p_file: CSV file with a list of names, each name is a carrier person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                              time.perf_counter(), p_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                person(_parser.name(p_name), population='add')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <carrier> <file> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_file, (ft - st)))
    return


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: person library person.csv
    :param p_library: CSV file with a list of names, each name is a carrier person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <library> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_file(p_library, 'carrier')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <carrier> <library> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_library, (ft - st)))
    return


def query(persons):
    """
    :usage: queries carrier population from a file for the whole period
    :example: carrier query person.csv
    :param persons: csv file with a list of people
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <query> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               time.perf_counter(), persons))
    global modeltext
    if not str(persons).endswith('.csv'):
        persons += '.csv'
    try:
        with open(persons) as personsfile:
            subject = csv.reader(personsfile)
            subjectsList = list(subject)
        for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
            for j in range(len(subjectsList)):
                _model_parser.query_carrier(str(subjectsList[j][0]), i)
        carrier_inputs['carrier']['query'] = persons
        _history.add_param(carrier_inputs)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <carrier> <query> <{} :{} sec.>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), persons, (ft - st)))
    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <carrier> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                        time.perf_counter(), (mft - mst)))
