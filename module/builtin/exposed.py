import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <exposed>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))

import csv
import configparser
import re
import _parser
import _model_parser
import period as _period
import graph as grf
import person as prsn

models_path = 'epidemical-dsl/module/models/'
path = 'epidemical-dsl/module/data/'
exposed_population = []
countX = 0
x = 'X'
st = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <deceased>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
config = configparser.SafeConfigParser(allow_no_value=True)

try:
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
chanceX = config['EXPOSED']['chance']
percentX = config['EXPOSED']['percent']
exposed_periodX = config['EXPOSED']['period']
exposed_inputs = {'exposed': {}}
ft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <deceased> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft,
                                                         (ft - st)))


def activate():
    """
    :usage: adds exposed to the model
    :example: exposed activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <exposed> <activate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.e(x,chanceX, percentX, i)
        if int(exposed_periodX) < 0:
            _model_parser.e0_period(x, i + 1, i)
        elif int(exposed_periodX) == 0:
            _model_parser.not_e(x, i)
            _model_parser.not_e_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(exposed_periodX)))):
                if int(i) == (int(i) + int(exposed_periodX) - int(a) - 1):
                    continue
                _model_parser.e0_period(x, i + int(exposed_periodX) - a - 1, i)
            _model_parser.not_e_period(x, i + int(exposed_periodX), i)
    exposed_inputs['exposed']['activate'] = 'true'
    _history.add_param(exposed_inputs)
    print('exposed compartment is activated')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <exposed> <activate> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, (ft - st)))
    return


def deactivate():
    """
    usage: remove exposed from the model
    example: exposed deactivate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <exposed> <deactivate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                             time.perf_counter()))
    _model_parser.remove_line('exposed')

    exposed_inputs['exposed']['activate'] = 'false'
    _history.add_param(exposed_inputs)
    print('exposed compartment is deactivated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <exposed> <deactivate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))
    return


def _activate(x):
    """
    :usage: adds exposed to the model
    :example: exposed activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <exposed> <_activate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.e(x,chanceX, percentX, i)
        if int(exposed_periodX) < 0:
            _model_parser.e0_period(x, i + 1, i)
        elif int(exposed_periodX) == 0:
            _model_parser.not_e(x, i)
            _model_parser.not_e_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(exposed_periodX)))):
                if int(i) == (int(i) + int(exposed_periodX) - int(a) - 1):
                    continue
                _model_parser.e0_period(x, i + int(exposed_periodX) - a - 1, i)
            _model_parser.not_e_period(x, i + int(exposed_periodX), i)
    exposed_inputs['exposed']['activate'] = 'true'
    _history.add_param(exposed_inputs)
    print('exposed compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <exposed> <_activate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft,
            (ft - st)))
    return

def chance(chance, exposed='this'):
    """
    :usage: assigns chance of the susceptible population who can get exposed to the infection when there is contact
    :example: exposed chance 0.1
    :param:: chance: chance of the susceptible population who can get exposed to the infection
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <exposed> <chance> <{}> <exposed={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, chance, exposed))
    global chanceX
    n = chance.replace('.', '', 1).replace('%', '')
    option = exposed.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        chanceX = _parser.probability(config['EXPOSED']['chance'])
        exposed_inputs['exposed']['chance'] = config['EXPOSED']['chance']
        _history.add_param(exposed_inputs)
        print('exposed chance is set to {}'.format(config['EXPOSED']['chance']))
    if str(n).isdigit():
        chanceX = _parser.probability(chance)
        exposed_inputs['exposed']['chance'] = chanceX
        _history.add_param(exposed_inputs)
        print('exposed chance is set to', chanceX)
    else:
        print('invalid exposed chance')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <exposed> <chance> <{}> <exposed={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            ft, chance, exposed, (ft - st)))
    return


def percent(percentage, exposed='this'):
    """
    :usage: assigns percentage of the susceptible population who can get exposed to the infection
    :example: exposed percent 0.1
    :param:: percentage: percentage of the susceptible population who can get exposed to the infection
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <exposed> <percent> <{}> <exposed={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, percentage, exposed))
    global percentX
    n = percentage.replace('.', '', 1).replace('%', '')
    option = exposed.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        percentX = _parser.probability(config['EXPOSED']['percent'])
        exposed_inputs['exposed']['percent'] = config['EXPOSED']['percent']
        _history.add_param(exposed_inputs)
        print('exposed percent is set to {}'.format(config['EXPOSED']['percent']))
    if str(n).isdigit():
        percentX = _parser.probability(percentage)
        exposed_inputs['exposed']['percent'] = percentX
        _history.add_param(exposed_inputs)
        print('exposed percent is set to', percentX)
    else:
        print('invalid exposed percent')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <exposed> <percent> <{}> <exposed={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            ft, percentage, exposed, (ft - st)))
    return


def period(duration, period='this'):
    """
    :usage: assigns the average period length for getting exposed to the infection
    :example: exposed period 7
    :param duration: the average period length for getting exposed to the infection
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <exposed> <period> <{}> <period={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, duration, period))
    global exposed_periodX
    option = period.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        exposed_periodX = int(config['EXPOSED']['period'])
        exposed_inputs['exposed']['period'] = config['EXPOSED']['period']
        _history.add_param(exposed_inputs)
        print('exposed period is set to {}'.format(config['EXPOSED']['period']))
    elif str(duration).isdigit():
        exposed_periodX = int(duration)
        exposed_inputs['exposed']['period'] = exposed_periodX
        _history.add_param(exposed_inputs)
        print('exposed duration is set to ', exposed_periodX, _period.base_stringX + 's')
    elif str(duration) == '-1':
        exposed_periodX = -1
        print('exposed duration is set to infinity')
    else:
        print('invalid exposed period')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <exposed> <period> <{}> <period={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, duration, period, (ft - st)))
    return


def default(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: exposed default
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 <exposed> <default>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    chance(config['EXPOSED']['chance'])
    percent(config['EXPOSED']['percent'])
    period(config['EXPOSED']['period'])
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 <exposed> <default> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft,
                                                             (ft - st)))
    return


def plot(graph='pos', cases='default', save='default'):
    """
    :usage: graph (plot) exposed result data
    :example: exposed plot positive-days
    :example: exposed plot negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <exposed> <plot> <exposed={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.exposed0(), 'plot', cases=cases, save=save)
        grf._neg_graph(_model_parser.exposed0(), 'plot', cases=cases, save=save)
        grf._post_graph(_model_parser.exposed0(), 'plot', cases=cases, save=save)
        grf._negt_graph(_model_parser.exposed0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.exposed0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.exposed0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.exposed0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.exposed0(), 'plot', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <exposed> <plot> <exposed={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, graph, save, (ft - st)))


def bar(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) exposed result data
    :example: exposed bar positive-weeks
    :example: exposed bar negative-months
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <exposed> <bar> <exposed={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.exposed0(), 'bar', cases=cases, save=save)
        grf._neg_graph(_model_parser.exposed0(), 'bar', cases=cases, save=save)
        grf._post_graph(_model_parser.exposed0(), 'bar', cases=cases, save=save)
        grf._negt_graph(_model_parser.exposed0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.exposed0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.exposed0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.exposed0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.exposed0(), 'bar', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <exposed> <bar> <exposed={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, graph, save, (ft - st)))


def pie(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) exposed result data
    :example: exposed bar positive
    :example: exposed bar negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <exposed> <pie> <exposed={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.exposed0(), 'pie', cases=cases, save=save)
        grf._neg_graph(_model_parser.exposed0(), 'pie', cases=cases, save=save)
        grf._post_graph(_model_parser.exposed0(), 'pie', cases=cases, save=save)
        grf._negt_graph(_model_parser.exposed0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.exposed0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.exposed0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.exposed0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.exposed0(), 'pie', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <exposed> <pie> <exposed={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, graph, save, (ft - st)))


def scatter(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) exposed result data
    :example: exposed bar positive-times
    :example: exposed bar negative-times
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <exposed> <scatter> <exposed={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.exposed0(), 'scatter', cases=cases, save=save)
        grf._neg_graph(_model_parser.exposed0(), 'scatter', cases=cases, save=save)
        grf._post_graph(_model_parser.exposed0(), 'scatter', cases=cases, save=save)
        grf._negt_graph(_model_parser.exposed0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.exposed0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.exposed0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.exposed0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.exposed0(), 'scatter', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <exposed> <scatter> <exposed={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, graph, save, (ft - st)))


def person(personX, population='add'):
    """
    :usage: adds a exposed person to the population, at period start
    :example: exposed person p1
    :param personX: person to be added
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <exposed> <person> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                st, personX))
    global countX
    population = population.lower()
    countX += 1
    personX = _parser.name(personX)
    if population == 'add' or population == 'append':
        prsn.population.append(personX)
    exposed_population.append(personX)
    _model_parser.e0(personX, _period.startX)
    exposed_inputs['exposed']['person'] = personX
    _history.add_param(exposed_inputs)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <exposed> <person> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, personX, (ft - st)))
    return


def count():
    """
    :usage: prints total exposed count
    :return: None
    """
    _history._logger('{}[INFO]{}: 🟩 started <exposed> <count> -> count={}'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), countX))
    print('total exposed population count:', countX)


def group(sizeX, population='add'):
    """
    :usage: generates a population of the given size
    :example: group size 100
    :param sizeX: size of the group
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <exposed> <group> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                               sizeX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            person('e' + str(i), population=population)
        exposed_inputs['exposed']['size'] = sizeX
        _history.add_param(exposed_inputs)
    else:
        print('invalid population size')
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <exposed> <group> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, sizeX, (ft - st)))
    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: person file person.csv
    :param p_file: CSV file with a list of names, each name is an exposed person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <exposed> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                              p_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                person(_parser.name(p_name), population='add')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <exposed> <file> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, p_file, (ft - st)))
    return


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: person library person.csv
    :param p_library: CSV file with a list of names, each name is a exposed person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <exposed> <library> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_file(p_library, 'exposed')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <exposed> <library> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_library, (ft - st)))
    return


def query(persons):
    """
    :usage: queries exposed population from a file for the whole period
    :example: exposed query person.csv
    :param persons: csv file with a list of people
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <exposed> <query> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                               persons))
    global modeltext
    if not str(persons).endswith('.csv'):
        persons += '.csv'
    try:
        with open(persons) as personsfile:
            subject = csv.reader(personsfile)
            subjectsList = list(subject)
        for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
            for j in range(len(subjectsList)):
                _model_parser.query_exposed(str(subjectsList[j][0]), i)
        exposed_inputs['exposed']['query'] = persons
        _history.add_param(exposed_inputs)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <exposed> <query> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, persons, (ft - st)))
    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <exposed> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                        time.perf_counter(), (mft - mst)))
