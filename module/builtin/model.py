import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <model>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                         time.perf_counter()))
import configparser
import sys
import re
import _model_parser as mp
import _parser
import carrier, deceased, exposed, infected, infection, mdi, period, recovered, susceptible, vaccinated, vaccine, \
    resistant, person
import csv as csv_module
import pandas as pd
formatX = 'dict'
propagate_evidenceX = False
distributionsX = None
progressX = True
x = 'X'
data_path = 'epidemical-dsl/module/data/'
models_path = 'epidemical-dsl/module/models/'
c_t = time.localtime()
current_time = time.strftime("%y-%m-%d-%H-%M-%S", c_t)
_config = configparser.SafeConfigParser(allow_no_value=True)

try:
    _config.read_file(open(models_path + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    _config.read_file(open('epidemical-dsl/module/models/default.ini'))
    _history._logger(
        '{}[ERROR]{}: 🛑 <model> FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
samples_quantityX = _config['MODEL']['samples']
model_inputs = {'model': {}}


def samples(s, model='this'):
    """
    :usage: sets the number of samples
    :example: model samples 5
    :param s: the number of samples
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <model> <samples> <{}> <model={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, s, model))
    option = model.lower()
    if option == 'default':
        _config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            _config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            _config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 <model> <samples> FileNotFoundError'.format(
                    time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                    time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        samples(_config['MODEL']['samples'])
    elif str(s).isdigit():
        global samples_quantityX
        samples_quantityX = s
        print("number of samples is set to ", str(s))
        model_inputs['model']['samples'] = samples_quantityX
        _history.add_param(model_inputs)
    else:
        print('invalid number of samples')
    return


def exit():
    """
    :usage: exits the program
    :example: model exit
    :return:
    """
    sys.exit("exit successfully")


def new():
    """
    :usage: resets the model
    :example: model new
    :return: None
    """
    mp.new()
    print('new model is set')
    print('you can check your model with (show model)')
    return


def config(name):
    """
    :usage: set the file_name.ini file
    :example: model config sir.ini
    :param name: file name
    :print: confirmation message
    :return: None
    """
    _parser.config_name(name)
    return


def defaults(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: vaccinated defaults
    :return: None
    """
    file = file.lower().replace('.ini', '')
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <model> <defaults>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    _config = configparser.SafeConfigParser(allow_no_value=True)
    if file == 'default':
        print('setting the defaults with {}.ini'.format(_parser.config_file_name))
    else:
        print('setting the defaults with {}.ini'.format(file))
    try:
        if file == 'default':
            _config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            _config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        _config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger('{}[ERROR]{}: 🛑 <model> <defaults> FileNotFoundError'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter()))
        print('{}.ini file is not found while setting the defaults, it will be replaced with default.ini'.format(
            _parser.config_file_name))
    print('\n')
    samples(_config['MODEL']['samples'])
    print('\n')
    carrier.default(file=file)
    print('\n')

    deceased.default(file=file)
    print('\n')

    exposed.default(file=file)
    print('\n')

    infected.default(file=file)
    print('\n')

    infection.default(file=file)
    print('\n')

    mdi.default(file=file)
    print('\n')

    period.default(file=file)
    print('\n')

    recovered.default(file=file)
    print('\n')

    susceptible.default(file=file)
    print('\n')

    vaccinated.default(file=file)
    print('\n')

    vaccine.default(file=file)
    print('\n')

    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <model> <defaults> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, (ft - st)))
    return


def format(formatY):
    global formatX
    if formatY == 'str' or formatY == 'dict':
        formatX = formatY
        print('format is set to ', formatY)
    else:
        print(formatY +' is not supported')

def distributions(distributionsY):
    """
    :param distributionsY:
            "normal"
            "gaussian"
            "poisson"
            "exponential"
            "beta"
            "gamma"
            "uniform"
            "triangular"
            "vonmises"
            "weibull"
            "in_range"
    :return:
    """
    global distributionsX
    if re.search('normal|gaussian|poisson|exponential|beta|gamma|uniform|triangular|vonmises|weibull|in_range', distributionsY):
        distributionsX = distributionsY
        print('distributions is set to ', distributionsY)
    else:
        distributionsX = None
        print('distributions is set to None')


def dist(distributionsY):
    distributions(distributionsY)
    return
def propagate_evidence(propagate_evidenceY):
    global propagate_evidenceX
    if re.search('true', propagate_evidenceY, re.IGNORECASE):
        propagate_evidenceX = True
        print('propagate_evidence is set to True')
    else:
        propagate_evidenceX = False
        print('propagate_evidence is set to False')

def progress(progressY):
    global progressX
    if re.search('true', progressY, re.IGNORECASE):
        progressX = True
        print('progress is set to True')
    else:
        progressX = False
        print('progress is set to False')

def values():
    """
    :usage: show saved values
    :print: dictionary containing all user entered values
    :return: None
    """
    _history.all_param()
    return


def save():
    """
    :usage: save problog model text to a file in /docs
    :exmple: model save
    :return:None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <model> <save>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    file = open('epidemical-dsl/module/docs/' + current_time + "-model.txt", "w")
    file.write(mp.modeltext)
    file.close()
    print(current_time + "-model.txt is saved in epidemical-dsl/module/docs")

def text():
    save()
    return

def _save():
    """
    :usage: save problog model text to a file in /docs if autosave is activate
    :return:None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <model> <_save>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    if mp.auto_S:
        file = open(mp.file_name, "w")
        file.write(mp.modeltext)
        file.close()
        _history._logger(
            '{}[INFO]{}: 🟢 {} file is saved'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                     time.perf_counter(), mp.file_name))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <model> <_save> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  st, (ft - st)))


def autosave():
    """
    :usage: toggle autosave to save problog model text to a file in /docs
    :exmple: model autosave
    :return:None
    """
    mp.autosave()
    return


def problog(motxt):
    """
    :usage: load problog model from file in the /docs directory
    :example: model problog problogmodel.txt
    :param motxt: problog file name
    :return: None
    """
    mp.file(motxt)
    return


def csv(csv_fileX):
    """
    :usage: add data from csv file as problog code
    :param csv_fileX: persons or contact csv file
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <model> <csv> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                           csv_fileX))
    if not str(csv_fileX).endswith('.csv'):
        csv_fileX += '.csv'
    csv_file = open(data_path + csv_fileX, 'r')
    reader = csv_module.reader(csv_file, delimiter=',')
    num_col = len(next(reader))
    csv_file.seek(0)
    if num_col == 1:
        for line in reader:
            mp.p(line[0], period.startX)
    elif num_col == 2:
        print('num of col 2')
        for line in reader:
            mp.contact_(line[0], line[1], period.startX)
    elif num_col == 3:
        for line in reader:
            mp.contact_(line[0], line[1], line[3])
    else:
        print('invalid file format, persons file must have one column and contact file must have three')
    return


def file(csv_fileX):
    """
    :usage: covert persons or contacts csv file to problog txt file
    :param csv_fileX: persons or contacts csv file, comma separated
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <model> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                            csv_fileX))
    if not str(csv_fileX).endswith('.csv'):
        csv_fileX += '.csv'
    new_file_name = data_path + 'problog-' + str(csv_fileX).replace('.csv', '.txt')
    csv_file = open(data_path + csv_fileX, 'r')
    reader = csv_module.reader(csv_file, delimiter=',')
    num_col = len(next(reader))
    csv_file.seek(0)
    if num_col == 1:
        persons_file = open(new_file_name, "w")
        for line in reader:
            persons_file.write(mp.person(line[0], period.startX) + '.\n')
        persons_file.close()
        print(new_file_name + ' saved')
    elif num_col == 2:
        contacts_file = open(new_file_name, "w")
        for line in reader:
            contacts_file.write(mp.contact(line[0], line[1], period.startX) + '.\n')
        contacts_file.close()
        print(new_file_name + ' saved')
    elif num_col == 3:
        contacts_file = open(new_file_name, "w")
        for line in reader:
            contacts_file.write(mp.contact(line[0], line[1], line[3]) + '.\n')
        contacts_file.close()
        print(new_file_name + ' saved')
    else:
        print('invalid file format, persons file must have one column and contact file must have three')
    return


def probability(p, x, y):
    """
    :usage: adds a pure problog fact rule with a probability
    :example: model probability 0.05 susceptible(X) infected(X)
    :param p: probability
    :param x: predicate
    :param y: clause
    :return: None
    """
    mp.problog_probability(p, x, y)
    return


def rule(x, y):
    """
    :usage: adds pure problog fact
    :example: model rule immune(X) vaccinated(X)
    :param x: predicate
    :param y: clause
    :return: None
    """
    mp.rule(x, y)
    return


def add(text):
    """
    :usage: adds pure problog model text
    :param text: text to add
    :return: None
    """
    mp.add_text(text)
    return


def line(line):
    """
    :usage: adds pure problog model line without spaces
    :example: add line
    :param line: line to add
    :return: None
    """
    mp.model_line(line)
    return


def remove(something):
    """
    :usage: remove something from model
    :param something: something to remove
    :return: None
    """
    count = mp.remove_line(something)
    print('{} lines removed'.format(count))
    return


def replace(old, new):
    """
    :usage: replace something in the model text
    :param old: old phrase
    :param new: new phrase
    :return: None
    """
    mp.replace_word(old, new)
    return


def groups():
    """
    :usage: prints the model groups
    :return: None
    """
    dic = {'person': person.population, 'susceptible': susceptible.susceptible_population,
           'exposed': exposed.exposed_population, 'infected': infected.infected_population,
           'carrier': carrier.carrier_population, 'recovered': recovered.recovered_population,
           'resistant': resistant.resistant_population, 'mdi': mdi.mdi_population,
           'vaccinated': vaccinated.vaccinated_population, 'deceased': deceased.deceased_population}
    df = pd.DataFrame.from_dict(dic, orient='index')
    df = df.transpose()
    df.fillna('', inplace=True)
    print(df.to_string(index=False))


def db(file):
    mp.load_sql(file)
    return


def si():
    for i in range(int(period.startX), int(period.endX), int(period.stepX)):
        mp.si_model(susceptible.percentX, infected.percentX, infected.chanceX, i)
        if int(infected.incubation_periodX) < 0:
            mp.i0_period(x, i + 1, i)
        elif int(infected.incubation_periodX) == 0:
            mp.not_i(x, i)
            mp.not_i_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(infected.incubation_periodX)))):
                if int(i) == (int(i) + int(infected.incubation_periodX) - int(a) - 1):
                    continue
                mp.i0_period(x, i + int(infected.incubation_periodX) - a - 1, i)
            mp.not_i_period(x, i + int(infected.incubation_periodX), i)
    return


def sir():
    for i in range(int(period.startX), int(period.endX), int(period.stepX)):
        mp.sir_model(susceptible.percentX, infected.percentX, infected.chanceX, recovered.chanceX, i)
        if int(infected.incubation_periodX) < 0:
            mp.i0_period(x, i + 1, i)
        elif int(infected.incubation_periodX) == 0:
            mp.not_i(x, i)
            mp.not_i_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(infected.incubation_periodX)))):
                if int(i) == (int(i) + int(infected.incubation_periodX) - int(a) - 1):
                    continue
                mp.i0_period(x, i + int(infected.incubation_periodX) - a - 1, i)
            mp.not_i_period(x, i + int(infected.incubation_periodX), i)
        if int(recovered.resistance_periodX) < 0:
            mp.r0_period(x, i + 1, i)
        elif int(recovered.resistance_periodX) == 0:
            mp.not_rs(x, i)
            mp.not_rs_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(recovered.resistance_periodX)))):
                if int(i) == (int(i) + int(recovered.resistance_periodX) - int(a) - 1):
                    continue
                mp.r0_period(x, i + int(recovered.resistance_periodX) - a - 1, i)
            mp.not_rs_period(x, i + int(recovered.resistance_periodX), i)
    return


def sirc():
    for i in range(int(period.startX), int(period.endX), int(period.stepX)):
        mp.sirc_model(susceptible.percentX, infected.percentX, infected.chanceX, recovered.chanceX, carrier.percentX,
                      carrier.chanceX, carrier.recursionX, i)
        if int(infected.incubation_periodX) < 0:
            mp.i0_period(x, i + 1, i)
        elif int(infected.incubation_periodX) == 0:
            mp.not_i(x, i)
            mp.not_i_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(infected.incubation_periodX)))):
                if int(i) == (int(i) + int(infected.incubation_periodX) - int(a) - 1):
                    continue
                mp.i0_period(x, i + int(infected.incubation_periodX) - a - 1, i)
            mp.not_i_period(x, i + int(infected.incubation_periodX), i)
        if int(recovered.resistance_periodX) < 0:
            mp.r0_period(x, i + 1, i)
        elif int(recovered.resistance_periodX) == 0:
            mp.not_rs(x, i)
            mp.not_rs_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(recovered.resistance_periodX)))):
                if int(i) == (int(i) + int(recovered.resistance_periodX) - int(a) - 1):
                    continue
                mp.r0_period(x, i + int(recovered.resistance_periodX) - a - 1, i)
            mp.not_rs_period(x, i + int(recovered.resistance_periodX), i)
        if int(carrier.carrier_periodX) < 0:
            mp.i_c0_period(x, i + 1, i)
        elif int(carrier.carrier_periodX) == 0:
            mp.not_i_c(x, i)
            mp.not_i_c_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(carrier.carrier_periodX)))):
                if int(i) == (int(i) + int(carrier.carrier_periodX) - int(a) - 1):
                    continue
                mp.i_c0_period(x, i + int(carrier.carrier_periodX) - a - 1, i)
            mp.not_i_c_period(x, i + int(carrier.carrier_periodX), i)
    return
