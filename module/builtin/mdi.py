import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <mdi>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
import csv
import configparser
import re
import _parser
import _model_parser
import period as _period
import graph as grf
import person as prsn

path = 'epidemical-dsl/module/data/'
models_path = 'epidemical-dsl/module/models/'
mdi_population = []
countX = 0
x = 'X'
st = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <deceased>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
config = configparser.SafeConfigParser(allow_no_value=True)
try:
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
percentX = config['MATERNALLY_DERIVED_IMMUNITY']['percent']
maternally_derived_immune_periodX = config['MATERNALLY_DERIVED_IMMUNITY']['period']
ft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <mdi> loading default.ini :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, (ft - st)))

mdi_inputs = {'mdi': {}}


def activate():
    """
    :usage: adds maternally derived immunity to the model
    :example: mdi activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <mdi> <activate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.m(x, percentX, i)
        if int(maternally_derived_immune_periodX) < 0:
            _model_parser.m0_period(x, i + 1, i)
        elif int(maternally_derived_immune_periodX) == 0:
            _model_parser.not_m(x, i)
            _model_parser.not_m_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(maternally_derived_immune_periodX)))):
                if int(i) == (int(i) + int(maternally_derived_immune_periodX) - int(a) - 1):
                    continue
                _model_parser.m0_period(x, i + int(maternally_derived_immune_periodX) - a - 1, i)
            _model_parser.not_m_period(x, i + int(maternally_derived_immune_periodX), i)
    mdi_inputs['mdi']['activate'] = 'true'
    _history.add_param(mdi_inputs)
    print('mdi compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <mdi> <activate> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), (ft - st)))
    return


def deactivate():
    """
    usage: remove mdi from the model
    example: mdi deactivate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <mdi> <deactivate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                         time.perf_counter()))
    _model_parser.remove_line('mdi')

    mdi_inputs['mdi']['activate'] = 'false'
    _history.add_param(mdi_inputs)
    print('mdi compartment is deactivated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <mdi> <deactivate> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                   time.perf_counter(), (ft - st)))
    return


def _activate(x):
    """
    :usage: adds maternally derived immunity to the model
    :example: mdi activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <mdi> <_activate> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                             x))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.m(x, percentX, i)
        if int(maternally_derived_immune_periodX) < 0:
            _model_parser.m0_period(x, i + 1, i)
        elif int(maternally_derived_immune_periodX) == 0:
            _model_parser.not_m(x, i)
            _model_parser.not_m_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(maternally_derived_immune_periodX)))):
                if int(i) == (int(i) + int(maternally_derived_immune_periodX) - int(a) - 1):
                    continue
                _model_parser.m0_period(x, i + int(maternally_derived_immune_periodX) - a - 1, i)
            _model_parser.not_m_period(x, i + int(maternally_derived_immune_periodX), i)
    mdi_inputs['mdi']['activate'] = 'true'
    _history.add_param(mdi_inputs)
    print('mdi compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <mdi> <_activate> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))

    return


def percent(percentage, mdi='this'):
    """
    :usage: assigns percentage of population with maternally derived immunity
    :example: mdi percent 0.1
    :param percentage: percentage of population with maternally derived immunity
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <mdi> <percent> <{}> <mdi={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, percentage, mdi))
    global percentX
    n = percentage.replace('.', '', 1).replace('%', '')
    option = mdi.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        percentX = _parser.probability(config['MATERNALLY_DERIVED_IMMUNITY']['percent'])
        mdi_inputs['mdi']['percent'] = config['MATERNALLY_DERIVED_IMMUNITY']['percent']
        _history.add_param(mdi_inputs)
        print('mdi mdi percent is set to {}'.format(config['MDI']['percent']))
    elif str(n).isdigit():
        percentX = _parser.probability(percentage)
        mdi_inputs['mdi']['percent'] = percentX
        _history.add_param(mdi_inputs)
        print('mdi percent is set to ', percentX)
    else:
        print('invalid mdi percent')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <mdi> <percent> <{}> <mdi={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), percentage, mdi, (ft - st)))

    return


def period(duration, period='this'):
    """
    :usage: assigns the average period length for getting maternally derived immunity
    :example: mdi period 24
    :param duration: the average period length for getting maternally derived immunity
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <mdi> <period> <{}> <period={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, duration, period))
    global maternally_derived_immune_periodX
    option = period.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        maternally_derived_immune_periodX = _parser.probability(config['MATERNALLY_DERIVED_IMMUNITY']['percent'])
        mdi_inputs['mdi']['percent'] = config['MATERNALLY_DERIVED_IMMUNITY']['percent']
        _history.add_param(mdi_inputs)
        print('mdi mdi percent is set to {}'.format(config['MDI']['percent']))
    elif str(duration).isdigit():
        maternally_derived_immune_periodX = int(duration)
        mdi_inputs['mdi']['period'] = maternally_derived_immune_periodX
        _history.add_param(mdi_inputs)
        print('mdi duration is set to ', maternally_derived_immune_periodX, _period.base_stringX + 's')
    elif str(duration) == '-1':
        maternally_derived_immune_periodX = -1
        print('mdi duration is set to infinity')
    else:
        print('invalid mdi period')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <mdi> <percent> <{}> <period={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), duration, period, (ft - st)))

    return


def default(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: mdi default
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <mdi> <default>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    percent(config['MATERNALLY_DERIVED_IMMUNITY']['percent'])
    period(config['MATERNALLY_DERIVED_IMMUNITY']['period'])
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <mdi> <default> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))

    return


def plot(graph='pos', cases='default', save='default'):
    """
    :usage: graph (plot) mdi result data
    :example: mdi plot positive-days
    :example: mdi plot negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 <mdi> <plot> <mdi={}> <save={}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.mdi0(), 'plot', cases=cases, save=save)
        grf._neg_graph(_model_parser.mdi0(), 'plot', cases=cases, save=save)
        grf._post_graph(_model_parser.mdi0(), 'plot', cases=cases, save=save)
        grf._negt_graph(_model_parser.mdi0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.mdi0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.mdi0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.mdi0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.mdi0(), 'plot', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <mdi> <plot> <mdi={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def bar(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) mdi result data
    :example: mdi bar positive-weeks
    :example: mdi bar negative-months
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 <mdi> <bar> <mdi={}> <save={}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                               graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.mdi0(), 'bar', cases=cases, save=save)
        grf._neg_graph(_model_parser.mdi0(), 'bar', cases=cases, save=save)
        grf._post_graph(_model_parser.mdi0(), 'bar', cases=cases, save=save)
        grf._negt_graph(_model_parser.mdi0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.mdi0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.mdi0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.mdi0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.mdi0(), 'bar', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <mdi> <bar> <mdi={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def pie(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) mdi result data
    :example: mdi pie positive
    :example: mdi pie negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 <mdi> <pie> <mdi={}> <save={}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                               graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.mdi0(), 'pie', cases=cases, save=save)
        grf._neg_graph(_model_parser.mdi0(), 'pie', cases=cases, save=save)
        grf._post_graph(_model_parser.mdi0(), 'pie', cases=cases, save=save)
        grf._negt_graph(_model_parser.mdi0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.mdi0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.mdi0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.mdi0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.mdi0(), 'pie', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <mdi> <pie> <mdi={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def scatter(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) mdi result data
    :example: mdi bar positive-times
    :example: mdi bar negative-times
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 <mdi> <scatter> <mdi={}> <save={}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                   st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.mdi0(), 'scatter', cases=cases, save=save)
        grf._neg_graph(_model_parser.mdi0(), 'scatter', cases=cases, save=save)
        grf._post_graph(_model_parser.mdi0(), 'scatter', cases=cases, save=save)
        grf._negt_graph(_model_parser.mdi0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.mdi0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.mdi0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.mdi0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.mdi0(), 'scatter', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <mdi> <scatter> <mdi={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def person(personX, population='add'):
    """
    :usage: adds a mdi person to the population, at period start
    :example: mdi person p1
    :param personX: person to be added
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <mdi> <person> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                            personX))
    global countX
    population = population.lower()
    countX += 1
    personX = _parser.name(personX)
    mdi_population.append(personX)
    if population == 'add' or population == 'append':
        prsn.population.append(personX)
    _model_parser.m0(personX, _period.startX)
    mdi_inputs['mdi']['person'] = personX
    _history.add_param(mdi_inputs)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <mdi> <person> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), personX, (ft - st)))
    return


def count():
    """
    :usage: prints total mdi count
    :return: None
    """
    _history._logger(
        '{}[INFO]{}: 🟩 <vaccinated> <count> -> count={}'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), countX))
    print('total mdi population count:', countX)


def group(sizeX, population='add'):
    """
    :usage: generates a population of the given size
    :example: population group 100
    :param sizeX: size of the group
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <mdi> <group> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                           sizeX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            person('m' + str(i), population=population)
        mdi_inputs['mdi']['size'] = sizeX
        _history.add_param(mdi_inputs)
    else:
        print('invalid population size')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <mdi> <group> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), sizeX, (ft - st)))
    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: person file person.csv
    :param p_file: CSV file with a list of names, each name is a maternally derived immune person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <mdi> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                          p_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                person(_parser.name(p_name), population='add')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <mdi> <file> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_file, (ft - st)))

    return


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: person library person.csv
    :param p_library: CSV file with a list of names, each name is a mdi person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <mdi> <library> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                             time.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_file(p_library, 'mdi')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <mdi> <library> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_library, (ft - st)))
    return


def query(persons):
    """
    :usage: queries mdi population from a file for the whole period
    :example: mdi query person.csv
    :param persons: csv file with a list of people
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <mdi> <query> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                           persons))
    global modeltext
    if not str(persons).endswith('.csv'):
        persons += '.csv'
    try:
        with open(persons) as personsfile:
            subject = csv.reader(personsfile)
            subjectsList = list(subject)
        for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
            for j in range(len(subjectsList)):
                _model_parser.query_mdi(str(subjectsList[j][0]), i)
        mdi_inputs['mdi']['query'] = persons
        _history.add_param(mdi_inputs)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <mdi> <query> <{} :{} sec.>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), persons, (ft - st)))
    return


mft = time.perf_counter()
_history._logger('{}[INFO]{}: 🟨 finished <mdi> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), (mft - mst)))
