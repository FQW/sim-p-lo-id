user_inputsX = []
param_inputsX = {}
write_to_file = False
write_to_terminal = False
descending = False
import pandas as pd

mpath = 'epidemical-dsl/module/'
logged_txt = ''


def _logger(message):
    global logged_txt
    logged_txt += message
    if write_to_terminal:
        print(message)
    if write_to_file:
        try:
            if descending:
                log_file = open(mpath + 'log', 'r')
                temp = log_file.read()
                log_file.close()
                log_file = open(mpath + 'log', 'a')
                log_file.write(message + '\n')
                log_file.write(temp + '\n')
                log_file.close()
            if not descending:
                log_file = open(mpath + 'log', 'a')
                log_file.write(message + '\n')
                log_file.close()
        except FileNotFoundError:
            print('log file not found')
        except FileExistsError:
            print('log file does not exist')
    return


def log(enable='terminal', disable='file', order='default', clear='none'):
    """
    :usage: debugging
    :print: if toggled (+confirmation), all problog sample results
    :return: None
    """
    global write_to_file
    global write_to_terminal
    global descending
    enable = enable.lower()
    disable = disable.lower()
    if enable == 'all' or enable == 'file' or enable == 'log' or enable == 'save' or enable == 'default':
        write_to_file = True
        print('writing to log file is enabled')
        try:
            if descending:
                log_file = open(mpath + 'log', 'r')
                temp = log_file.read()
                log_file.close()
                log_file = open(mpath + 'log', 'w')
                log_file.write(logged_txt + '\n')
                log_file.write(temp + '\n')
                log_file.close()
            if not descending:
                log_file = open(mpath + 'log', 'w')
                log_file.write(logged_txt + '\n')
                log_file.close()
        except FileNotFoundError:
            print('log file not found')
        except FileExistsError:
            print('log file does not exist')
    if enable == 'all' or enable == 'terminal' or enable == 'console':
        write_to_terminal = True
        print('writing to terminal is enabled')
    if disable == 'all' or disable == 'file' or enable == 'none':
        write_to_file = False
        print('writing to log file is disabled')
    if disable == 'all' or disable == 'terminal' or disable == 'console' or disable == 'default' or enable == 'none':
        write_to_terminal = False
        print('writing to terminal is disabled')
    if order == 'filo' or order == 'descending':
        descending = True
        print('writing to log file if enabled will be in descending order (newest first)')
    if order == 'default' or order == 'lifo' or order == 'ascending':
        descending = False
        print('writing to log file if enabled will be in ascending order (newest last)')
    if clear == 'all' or clear == 'true':
        log_file = open(mpath + 'log', 'w')
        log_file.write('')
        log_file.close()


def last_input():
    try:
        x = user_inputsX[-2]
        print('>> ', x)
    except IndexError:
        print('no inputs')
        return
    return x


def all_inputs():
    for index, value in enumerate(user_inputsX, 0):
        print("{}. {}".format(index, value))
    return user_inputsX


def clear_inputs():
    global user_inputsX
    user_inputsX = []
    print('history cleared')
    return


def all_param():
    df = pd.DataFrame.from_dict(param_inputsX)
    df = df.transpose()
    df.fillna('', inplace=True)
    print(df.to_string())
    return param_inputsX


def add_param(inp):
    global param_inputsX
    param_inputsX.update(inp)
    return


def param_index(entry):
    print(param_inputsX[entry[0]][entry[1]])
