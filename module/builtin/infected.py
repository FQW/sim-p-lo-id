import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <infected>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                            time.perf_counter()))

import csv
import configparser
import re
import _parser
import _model_parser
import period as _period
import graph as grf
import person as prsn

models_path = 'epidemical-dsl/module/models/'
path = 'epidemical-dsl/module/data/'
infected_population = []
countX = 0
x = 'X'
st = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <deceased>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
config = configparser.SafeConfigParser(allow_no_value=True)
try:
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
chanceX = config['INFECTED']['chance']
percentX = config['INFECTED']['percent']
incubation_periodX = config['INFECTION']['period']
infected_inputs = {'infected': {}}
ft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <infected> loading default.ini :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, (ft - st)))


def activate():
    """
    :usage: adds infected to the model
    :example: infected activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infected> <activate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    chanceZ = abs((float(chanceX) - float(percentX)) / (1 - float(percentX)))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.i(x, chanceZ, percentX, i)
        if int(incubation_periodX) < 0:
            _model_parser.i0_period(x, i + 1, i)
        elif int(incubation_periodX) == 0:
            _model_parser.not_i(x, i)
            _model_parser.not_i_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(incubation_periodX)))):
                if int(i) == (int(i) + int(incubation_periodX) - int(a) - 1):
                    continue
                _model_parser.i0_period(x, i + int(incubation_periodX) - a - 1, i)
            _model_parser.not_i_period(x, i + int(incubation_periodX), i)
    infected_inputs['infected']['activate'] = 'true'
    _history.add_param(infected_inputs)
    print('infected compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infected> <activate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))

    return


def deactivate():
    """
    usage: remove infected from the model
    example: infected deactivate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infected> <deactivate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                              time.perf_counter()))
    _model_parser.remove_line('infected')

    infected_inputs['infected']['activate'] = 'false'
    _history.add_param(infected_inputs)
    print('infected compartment is deactivated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infected> <deactivate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))
    return


def _activate(x):
    """
    :usage: adds infected to the model
    :example: infected activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infected> <_activate> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  st, x))
    chanceZ = abs((float(chanceX) - float(percentX)) / (1 - float(percentX)))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.i(x, chanceZ, percentX, i)
        if int(incubation_periodX) < 0:
            _model_parser.i0_period(x, i + 1, i)
        elif int(incubation_periodX) == 0:
            _model_parser.not_i(x, i)
            _model_parser.not_i_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(incubation_periodX)))):
                if int(i) == (int(i) + int(incubation_periodX) - int(a) - 1):
                    continue
                _model_parser.i0_period(x, i + int(incubation_periodX) - a - 1, i)
            _model_parser.not_i_period(x, i + int(incubation_periodX), i)
    infected_inputs['infected']['activate'] = 'true'
    _history.add_param(infected_inputs)
    print('infected compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infected> <_activate> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))

    return


def chance(chanceY, infected='this'):
    """
    :usage: sets the chance of getting infected every time contact occurs
    :param chanceY: chance of getting infected every time contact occurs
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infected> <chance> <{}> <infected={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, chanceY, infected))
    global chanceX
    n = chanceY.replace('.', '', 1).replace('%', '')
    option = infected.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        chanceX = _parser.probability(config['INFECTED']['chance'])
        infected_inputs['infected']['chance'] = config['INFECTED']['chance']
        _history.add_param(infected_inputs)
        print('infected chance is set to {}'.format(config['INFECTED']['chance']))
    elif str(n).isdigit():
        chanceX = _parser.probability(chanceY)
        infected_inputs['infected']['chance'] = chanceX
        _history.add_param(infected_inputs)
        print('infected chance is set to ', chanceX)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infected> <chance> <{}> <infected={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, chanceY, infected, (ft - st)))

    return


def percent(percentage, infected='this'):
    """
    :usage: assigns percentage of the susceptible population who get infected without contact
    :example: infected percent 0.1
    :param: percentage: percentage of the susceptible population who get infected without contact
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infected> <percent> <{}> <infected={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, percentage, infected))
    global percentX
    n = percentage.replace('.', '', 1).replace('%', '')
    option = infected.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        percentX = _parser.probability(config['INFECTED']['percent'])
        infected_inputs['infected']['percent'] = config['INFECTED']['percent']
        _history.add_param(infected_inputs)
        print('infected infected percent is set to {}'.format(config['INFECTED']['percent']))
    elif str(n).isdigit():
        percentX = _parser.probability(percentage)
        infected_inputs['infected']['percent'] = percentX
        _history.add_param(infected_inputs)
        print('infected percent is set to ', percentX)
    else:
        print('invalid infected percent')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <infected> <percent> <{}> <infected={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), percentage, infected, (ft - st)))

    return


def period(incubation, period='this'):
    """
    :usage: assigns the average period length for getting infected
    :example: infected period 7
    :param: incubation: the average period length for getting infected
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infected> <period> <{}> <period={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, incubation, period))
    global incubation_periodX
    option = period.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        incubation_periodX = _parser.probability(config['INFECTED']['period'])
        infected_inputs['infected']['period'] = config['INFECTED']['period']
        _history.add_param(infected_inputs)
        print('infected period is set to {}'.format(config['INFECTED']['period']))
    elif str(incubation).isdigit():
        incubation_periodX = int(incubation)
        infected_inputs['infected']['period'] = incubation_periodX
        _history.add_param(infected_inputs)
        print('infection duration is set to ', incubation_periodX, _period.base_stringX + 's')
    elif str(incubation) == '-1':
        print('infection duration is set to infinity')
        incubation_periodX = -1
    else:
        print('invalid infected period')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <infected> <percent> <{}> <period={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), incubation, period, (ft - st)))

    return


def default(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: infected default
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infected> <default>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    chance(config['INFECTED']['chance'])
    percent(config['INFECTED']['percent'])
    period(config['INFECTION']['period'])
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infected> <default> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))

    return


def plot(graph='pos', cases='default', save='default'):
    """
    :usage: graph (plot) infected result data
    :example: infected plot positive-days
    :example: infected plot negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infected> <plot> <infected={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.infected0(), 'plot', cases=cases, save=save)
        grf._neg_graph(_model_parser.infected0(), 'plot', cases=cases, save=save)
        grf._post_graph(_model_parser.infected0(), 'plot', cases=cases, save=save)
        grf._negt_graph(_model_parser.infected0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.infected0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.infected0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.infected0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.infected0(), 'plot', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <infected> <plot> <infected={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def bar(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) infected result data
    :example: infected bar positive-weeks
    :example: infected bar negative-months
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infected> <bar> <infected={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.infected0(), 'bar', cases=cases, save=save)
        grf._neg_graph(_model_parser.infected0(), 'bar', cases=cases, save=save)
        grf._post_graph(_model_parser.infected0(), 'bar', cases=cases, save=save)
        grf._negt_graph(_model_parser.infected0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.infected0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.infected0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.infected0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.infected0(), 'bar', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <infected> <bar> <infected={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def pie(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) infected result data
    :example: infected pie positive
    :example: infected pie negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infected> <pie> <infected={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.infected0(), 'pie', cases=cases, save=save)
        grf._neg_graph(_model_parser.infected0(), 'pie', cases=cases, save=save)
        grf._post_graph(_model_parser.infected0(), 'pie', cases=cases, save=save)
        grf._negt_graph(_model_parser.infected0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.infected0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.infected0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.infected0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.infected0(), 'pie', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <infected> <pie> <infected={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def scatter(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) infected result data
    :example: infected scatter positive-times
    :example: infected scatter negative-times
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infected> <scatter> <infected={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.infected0(), 'scatter', cases=cases, save=save)
        grf._neg_graph(_model_parser.infected0(), 'scatter', cases=cases, save=save)
        grf._post_graph(_model_parser.infected0(), 'scatter', cases=cases, save=save)
        grf._negt_graph(_model_parser.infected0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.infected0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.infected0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.infected0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.infected0(), 'scatter', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <infected> <scatter> <infected={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def person(personX, population='add'):
    """
    :usage: adds an infected person to the population, at period start
    :example: infected person p1
    :param personX: person to be added
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infected> <person> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 st, personX))
    global countX
    population = population.lower()
    countX += 1
    personX = _parser.name(personX)
    if population == 'add' or population == 'append':
        prsn.population.append(personX)
    infected_population.append(personX)
    _model_parser.i0(personX, _period.startX)
    infected_inputs['infected']['person'] = personX
    _history.add_param(infected_inputs)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infected> <person> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), personX, (ft - st)))

    return


def count():
    """
    :usage: prints total infected count
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 <vaccinated> <count> -> count={}'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 st, countX))
    print('total infected population count:', countX)


def group(sizeX, population='add'):
    """
    :usage: generates a population of the given size
    :example: population group 100
    :param sizeX: size of the group
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infected> <group> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                st, sizeX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            person('i' + str(i), population=population)
        infected_inputs['infected']['size'] = sizeX
        _history.add_param(infected_inputs)
    else:
        print('invalid population size')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infected> <group> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), sizeX, (ft - st)))

    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: person file person.csv
    :param p_file: CSV file with a list of names, each name is an infected person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infected> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                               p_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                person(_parser.name(p_name), population='add')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infected> <file> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_file, (ft - st)))
    return


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: person library person.csv
    :param p_library: CSV file with a list of names, each name is a infected person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infected> <library> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  time.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_file(p_library, 'infected')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infected> <library> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_library, (ft - st)))
    return


def query(persons):
    """
    :usage: queries infected population from a file for the whole period
    :example: infected query person.csv
    :param persons: csv file with a list of people
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infected> <query> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                st, persons))
    global modeltext
    if not str(persons).endswith('.csv'):
        persons += '.csv'
    try:
        with open(persons) as personsfile:
            subject = csv.reader(personsfile)
            subjectsList = list(subject)
        for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
            for j in range(len(subjectsList)):
                _model_parser.query_infected(str(subjectsList[j][0]), i)
        infected_inputs['infected']['query'] = persons
        _history.add_param(infected_inputs)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infected> <query> <{} :{} sec.>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), persons, (ft - st)))

    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <infected> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                         time.perf_counter(), (mft - mst)))
