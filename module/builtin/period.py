import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <period>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                          time.perf_counter()))
import configparser
import _parser

models_path = 'epidemical-dsl/module/models/'
config = configparser.SafeConfigParser(allow_no_value=True)
try:
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
startX = config['PERIOD']['start']
endX = config['PERIOD']['end']
stepX = config['PERIOD']['step']
baseX = config['PERIOD']['base']
base_stringX = 'day'
period_inputs = {'period': {}}


def length(t):
    """
    :usage: sets the period length
    :example period length 52
    :param t: the period length
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <period> <length> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                               t))
    if str(t).isdigit():
        start(1)
        end(t)
        step(1)
        print("start:{0}, step:{2}, end:{1}".format(startX, endX, stepX))
        period_inputs['period']['length'] = t
        _history.add_param(period_inputs)
    else:
        print('invalid period length')
    return


def start(startY, period='this'):
    """
    :usage: sets the period start
    :example period start 1
    :param startY: the period start
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <period> <start> <{}> <period={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, startY, period))
    global startX
    option = period.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        startX = _parser.probability(config['PERIOD']['start'])
        period_inputs['period']['start'] = config['PERIOD']['start']
        _history.add_param(period_inputs)
        print('period start is set to {}'.format(config['PERIOD']['start']))
    elif str(startY).isdigit():
        if int(startY) >= int(endX):
            print("the start must be before the end")
        else:
            startX = int(startY)
            period_inputs['period']['start'] = startX
            _history.add_param(period_inputs)
            print("period start at ", startX)
    else:
        print('invalid period start')
    return


def end(endY, period='this'):
    """
    :usage: sets the period end
    :example period end 52
    :param endY: the period end
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <period> <end> <{}> <period={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, endY, period))
    global endX
    option = period.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        endX = _parser.probability(config['PERIOD']['end'])
        period_inputs['period']['end'] = config['PERIOD']['end']
        _history.add_param(period_inputs)
        print('period end is set to {}'.format(config['PERIOD']['end']))
    if str(endY).isdigit():
        if int(endY) <= int(startX):
            print("the end must be after the start")
        else:
            endX = int(endY)
            period_inputs['period']['end'] = endX
            _history.add_param(period_inputs)
            print("period end at ", endX)
    else:
        print('invalid period end')
    return


def step(stepY, period='this'):
    """
    :usage: sets the period step
    :example period step 1
    :param stepY: the period step
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <period> <step> <{}> <period={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, stepY, period))
    global stepX
    option = period.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        stepX = _parser.probability(config['PERIOD']['step'])
        period_inputs['period']['step'] = config['PERIOD']['step']
        _history.add_param(period_inputs)
        print('period step is set to {}'.format(config['PERIOD']['step']))
    if str(stepY).isdigit():
        if int(stepY) <= int(endX) or int(stepY) >= int(startX):
            stepX = int(stepY)
            period_inputs['period']['step'] = stepX
            _history.add_param(period_inputs)
            print("period step is set to ", stepX)
        else:
            print('step must be between the start and end')
    else:
        print('invalid period step')
    return


def base(baseY, period='this'):
    """
    :usage: sets the period base, used for infection, resistance, carrier period and so on...
    :example: period base 7
              infected period 2
              infected activate
    :param baseY: the period base
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <period> <percent> <{}> <period={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, baseY, period))
    global baseX
    option = period.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        baseX = _parser.probability(config['PERIOD']['base'])
        period_inputs['period']['base'] = config['PERIOD']['base']
        _history.add_param(period_inputs)
        print('period base is set to {}'.format(config['PERIOD']['base']))
    if str(baseY).isdigit():
        if int(baseY) <= int(endX) or int(baseY) >= int(startX):
            baseX = int(baseY)
            period_inputs['period']['base'] = baseX
            _history.add_param(period_inputs)
            print("period base is set to ", baseX)
        else:
            print("period base must be within the period length")
    else:
        print('invalid period base')
    return


def day():
    """
    :usage: sets the period base to 1, which means a person staying in a certain condition is calculated on a daily basis
    :example: period day
    :return: None
    """
    global base_stringX
    global baseX
    base_stringX = 'day'
    baseX = int(1)
    return


def week():
    """
    :usage: sets the period base to 1, which means a person staying in a certain condition is calculated on a weekly basis
    :example: period week
    :return: None
    """
    global base_stringX
    global baseX
    base_stringX = 'week'
    baseX = int(7)
    return


def month():
    """
    :usage: sets the period base to 30, which means a person staying in a certain condition is calculated on a monthly basis
    :example: period month
    :return: None
    """
    global base_stringX
    global baseX
    base_stringX = 'month'
    baseX = int(30)
    return


def season():
    """
    :usage: sets the period base to 120, which means a person staying in a certain condition is calculated on a quarter-yearly (season) basis
    :example: period season
    :return: None
    """
    global base_stringX
    global baseX
    base_stringX = 'season'
    baseX = int(120)
    return


def year():
    """
    :usage: sets the period base to 365, which means a person staying in a certain condition is calculated on a yearly basis
    :example: period year
    :return: None
    """
    global baseX
    global base_stringX
    base_stringX = 'year'
    baseX = int(365)
    return


def _base_string():
    """
    :return: the base string, such that if it is 1, then it returns day, if 7 then week and so on...
    """
    if baseX == 1:
        return 'day'
    elif baseX == 7:
        return 'week'
    elif baseX == 30:
        return 'month'
    elif baseX == 120:
        return 'season'
    elif baseX == 365:
        return 'year'


def default(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: period default
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <period> <default> '.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    start(config['PERIOD']['start'])
    step(config['PERIOD']['step'])
    end(config['PERIOD']['end'])
    base(config['PERIOD']['base'])
    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <period> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter(), (mft - mst)))
