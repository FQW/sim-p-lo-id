import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <vaccinated>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                              time.perf_counter()))
import csv
import configparser
import _parser
import _model_parser
import period as _period
import re
import graph as grf
import person as prsn

path = 'epidemical-dsl/module/data/'
models_path = 'epidemical-dsl/module/models/'
vaccinated_population = []
countX = 0
x = 'X'
st = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <deceased>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
config = configparser.SafeConfigParser(allow_no_value=True)
try:
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
percentX = config['VACCINATED']['percent']
rateX = config['VACCINATED']['rate']
vaccination_periodX = config['VACCINATED']['period']
vaccinated_inputs = {'vaccinated': {}}
ft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <vaccinated> loading default.ini :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, (ft - st)))


def activate():
    """
    :usage: adds vaccinated to the model
    :example: vaccinated activate
    :return: None
        """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccinated> <_activate> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, x))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.v(x, percentX, rateX, i)
        if int(vaccination_periodX) < 0:
            _model_parser.v0_period(x, i + 1, i)
        elif int(vaccination_periodX) == 0:
            _model_parser.not_v(x, i)
            _model_parser.not_v_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(vaccination_periodX)))):
                if not int(i) == (int(i) + int(vaccination_periodX) - int(a) - 1):
                    _model_parser.v0_period(x, i + int(vaccination_periodX) - a - 1, i)
            _model_parser.not_v_period(x, i + int(vaccination_periodX), i)
    vaccinated_inputs['vaccinated']['activate'] = 'true'
    _history.add_param(vaccinated_inputs)
    print('vaccinated compartment is activated')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <vaccinated> <activate> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), (ft - st)))
    return


def deactivate():
    """
    usage: remove vaccinated from the model
    example: vaccinated deactivate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <vaccinated> <deactivate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    _model_parser.remove_line('vaccinated')

    vaccinated_inputs['vaccinated']['activate'] = 'false'
    _history.add_param(vaccinated_inputs)
    print('vaccinated compartment is deactivated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <vaccinated> <deactivate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))
    return


def _activate(x):
    """
    :usage: adds vaccinated person to the model
    :example: vaccinated activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <vaccinated> <_activate> <{}>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, x))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.v(x, percentX, rateX, i)
        if int(vaccination_periodX) < 0:
            _model_parser.v0_period(x, i + 1, i)
        elif int(vaccination_periodX) == 0:
            _model_parser.not_v(x, i)
            _model_parser.not_v_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(vaccination_periodX)))):
                if int(i) == (int(i) + int(vaccination_periodX) - int(a) - 1):
                    continue
                _model_parser.v0_period(x, i + int(vaccination_periodX) - a - 1, i)
            _model_parser.not_v_period(x, i + int(vaccination_periodX), i)
    vaccinated_inputs['vaccinated']['activate'] = 'true'
    _history.add_param(vaccinated_inputs)
    print('vaccinated compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <vaccinated> <_activate> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))
    return


def percent(percentY, vaccinated='this'):
    """
    :usage: assigns percentage of vaccinated who still get the infection without contact
    :example: vaccinated percent 0.35
    :param percentY: percentage of vaccinated who still get the infection without contact
    :print: confirmation message
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccinated> <percent> <{}> <vaccinated={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, percentY, vaccinated))
    global percentX
    n = percentY.replace('.', '', 1).replace('%', '')
    option = vaccinated.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        percentX = _parser.probability(config['VACCINATED']['percent'])
        vaccinated_inputs['vaccinated']['percent'] = config['VACCINATED']['percent']
        _history.add_param(vaccinated_inputs)
        print('vaccinated percent is set to {}'.format(config['VACCINATED']['percent']))
    elif str(n).isdigit():
        percentX = _parser.probability(percentY)
        vaccinated_inputs['vaccinated']['percent'] = percentX
        _history.add_param(vaccinated_inputs)
        print('vaccinated percent is set to {}'.format(percentX))
    else:
        print('vaccinated percent entry is invalid')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <vaccinated> <percent> <{}> <vaccinated={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), percentY, vaccinated, (ft - st)))

    return


def rate(rateY, vaccinated='this'):
    """
    :usage: sets the rate of vaccination, if not set, the value is taken from the default.ini file
    :example: vaccinated rate 0.65
    :param rateY: the rate of vaccination
    :print: confirmation message
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccinated> <rate> <{}> <vaccinated={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, rateY, vaccinated))
    global rateX
    n = rateY.replace('.', '', 1).replace('%', '')
    option = vaccinated.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        rateX = _parser.probability(config['VACCINATED']['rate'])
        vaccinated_inputs['vaccinated']['rate'] = config['VACCINATED']['rate']
        _history.add_param(vaccinated_inputs)
        print('vaccinated vaccinated percent is set to {}'.format(config['VACCINATED']['rate']))
    elif str(n).isdigit():
        rateX = _parser.probability(rateY)
        vaccinated_inputs['vaccinated']['rate'] = rateX
        _history.add_param(vaccinated_inputs)
        print('vaccination rate is set to {}'.format(rateX))
    else:
        print('vaccinated rate entry is invalid')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <vaccinated> <rate> <{}> <vaccinated={}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            ft, rateY, vaccinated, (ft - st)))
    return


def period(vaccination_periodY, period='this'):
    """
    :usage: sets the vaccination duration period, if not set, the value is taken from the default.ini file
    :example: vaccine period 12
    :param vaccination_periodY: the vaccination duration period
    :print: confirmation message
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccinated> <period> <{}> <period={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, vaccination_periodY, period))
    global vaccination_periodX
    option = period.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        vaccination_periodX = _parser.probability(config['VACCINATED']['period'])
        vaccinated_inputs['vaccinated']['period'] = config['VACCINATED']['period']
        _history.add_param(vaccinated_inputs)
        print('vaccinated vaccinated percent is set to {}'.format(config['VACCINATED']['period']))
    elif str(vaccination_periodY).isdigit():
        vaccination_periodX = vaccination_periodY
        vaccinated_inputs['vaccinated']['period'] = vaccination_periodX
        _history.add_param(vaccinated_inputs)
        print('vaccination duration is set to ', vaccination_periodX, _period.base_stringX + 's')
    elif str(vaccination_periodY) == '-1':
        vaccination_periodX = -1
        print('vaccination duration is set to infinity')
    else:
        print('vaccinated period entry is invalid')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <vaccinated> <percent> <{}> <period={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), vaccination_periodY, period,
        (ft - st)))

    return


def default(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: vaccinated default
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <vaccinated> <default>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               st))
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    percent(config['VACCINATED']['percent'])
    rate(config['VACCINATED']['rate'])
    period(config['VACCINATED']['period'])
    _history._logger(
        '{}[INFO]{}: 🟨 finished <vaccinated> <default> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))
    return


def plot(graph='pos', cases='default', save='default'):
    """
    :usage: graph (plot) vaccinated result data
    :example: vaccinated plot positive-days
    :example: vaccinated plot negative
    :param graph: pos, neg, and/or day, week, month, year, quarter, season, time
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccinated> <plot> <vaccinated={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.vaccinated0(), 'plot', cases=cases, save=save)
        grf._neg_graph(_model_parser.vaccinated0(), 'plot', cases=cases, save=save)
        grf._post_graph(_model_parser.vaccinated0(), 'plot', cases=cases, save=save)
        grf._negt_graph(_model_parser.vaccinated0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.vaccinated0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.vaccinated0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.vaccinated0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.vaccinated0(), 'plot', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <vaccinated> <plot> <vaccinated={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def bar(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) vaccinated result data
    :example: vaccinated bar positive-weeks
    :example: vaccinated bar negative-months
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccinated> <bar> <vaccinated={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.vaccinated0(), 'bar', cases=cases, save=save)
        grf._neg_graph(_model_parser.vaccinated0(), 'bar', cases=cases, save=save)
        grf._post_graph(_model_parser.vaccinated0(), 'bar', cases=cases, save=save)
        grf._negt_graph(_model_parser.vaccinated0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.vaccinated0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.vaccinated0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.vaccinated0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.vaccinated0(), 'bar', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <vaccinated> <bar> <vaccinated={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def pie(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) vaccinated result data
    :example: vaccinated bar positive
    :example: vaccinated bar negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccinated> <pie> <vaccinated={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.vaccinated0(), 'pie', cases=cases, save=save)
        grf._neg_graph(_model_parser.vaccinated0(), 'pie', cases=cases, save=save)
        grf._post_graph(_model_parser.vaccinated0(), 'pie', cases=cases, save=save)
        grf._negt_graph(_model_parser.vaccinated0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.vaccinated0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.vaccinated0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.vaccinated0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.vaccinated0(), 'pie', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <vaccinated> <pie> <vaccinated={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def scatter(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) vaccinated result data
    :example: vaccinated bar positive-times
    :example: vaccinated bar negative-times
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccinated> <scatter> <vaccinated={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.vaccinated0(), 'scatter', cases=cases, save=save)
        grf._neg_graph(_model_parser.vaccinated0(), 'scatter', cases=cases, save=save)
        grf._post_graph(_model_parser.vaccinated0(), 'scatter', cases=cases, save=save)
        grf._negt_graph(_model_parser.vaccinated0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.vaccinated0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.vaccinated0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.vaccinated0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.vaccinated0(), 'scatter', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <vaccinated> <scatter> <vaccinated={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def person(personX, population='add'):
    """
    :usage: adds a vaccinated person to the population, at period start
    :example: vaccinated person p1
    :param personX: person to be added
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <vaccinated> <person> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                   st, personX))
    global countX
    population = population.lower()
    countX += 1
    personX = _parser.name(personX)
    if population == 'add' or population == 'append':
        prsn.population.append(personX)
    vaccinated_population.append(personX)
    _model_parser.v0(personX, _period.startX)
    vaccinated_inputs['vaccinated']['person'] = personX
    _history.add_param(vaccinated_inputs)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <vaccinated> <person> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), personX, (ft - st)))
    return


def count():
    """
    :usage: prints total vaccinated count
    :return: None
    """
    _history._logger(
        '{}[INFO]{}: 🟩 <vaccinated> <count> -> count={}'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), countX))
    print('total vaccinated population count:', countX)


def group(sizeX, population='add'):
    """
    :usage: generates a population of the given size
    :example: population group 100
    :param sizeX: size of the group
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <vaccinated> <group> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  st, sizeX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            person('v' + str(i), population=population)
        vaccinated_inputs['vaccinated']['size'] = sizeX
        _history.add_param(vaccinated_inputs)
    else:
        print('invalid population size')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <vaccinated> <group> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), sizeX, (ft - st)))

    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: person file person.csv
    :param p_file: CSV file with a list of names, each name is a vaccinated person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <vaccinated> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 st, p_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                person(_parser.name(p_name), population='add')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger('{}[ERROR]{}: 🛑 FileExistsError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <vaccinated> <file> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_file, (ft - st)))
    return


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: person library person.csv
    :param p_library: CSV file with a list of names, each name is a vaccinated person
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccinated> <library> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
        time.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_vfile(p_library)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <vaccinated> <library> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_library, (ft - st)))
    return


def query(persons):
    """
    :usage: queries vaccinated population from a file for the whole period
    :example: vaccinated query person.csv
    :param persons: csv file with a list of people
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <vaccinated> <query> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  st, persons))
    global modeltext
    if not str(persons).endswith('.csv'):
        persons += '.csv'
    try:
        with open(persons) as personsfile:
            subject = csv.reader(personsfile)
            subjectsList = list(subject)
        for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
            for j in range(len(subjectsList)):
                _model_parser.query_vaccinated(str(subjectsList[j][0]), i)
        vaccinated_inputs['vaccinated']['query'] = persons
        _history.add_param(vaccinated_inputs)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <vaccinated> <query> <{} :{} sec.>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), persons, (ft - st)))
    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <vaccinated> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter(), (mft - mst)))
