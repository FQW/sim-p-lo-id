import configparser
import time
import infection as infektion
import vaccine as vaxine
import _parser

c_t = time.localtime()
current_time = time.strftime("%y-%m-%d-%H-%M-%S", c_t)
modeltext = ""
path = 'epidemical-dsl/module/data/'
dpath = 'epidemical-dsl/module/docs/'
auto_S = False
file_name = dpath + current_time + "-model-autosave.txt"
infection_nameX = infektion.infection_nameX + '_'
infection_variantX = infektion.infection_variantX + '_'
infection_transmissionX = infektion.infection_transmissionX + '_'
vaccine_nameX = vaxine.vaccine_nameX + '_'
vaccine_numberX = vaxine.vaccine_numberX
vaccine_efficiencyX = vaxine.vaccine_efficiencyX
vaccinated_chanceX = 1 - float(vaccine_efficiencyX)
x = 'X'
y = 'Y'


def inf_var_name():
    return infection_nameX + infection_variantX


def infection_name(infectionY):
    """
    :usage: sets the name of the infection
    :param infectionY: the name of the infection, if '.' then no name is set
    :return: None
    """
    global infection_nameX
    infection_nameX = _parser.name(infectionY) + '_'
    return


def infection_variant(variantY):
    """
    :usage: sets the name of the infection variant
    :param variantY: the name of the infection variant, if '.' then no name is set
    :return: None
    """
    global infection_variantX
    infection_variantX = _parser.name(variantY) + '_'
    return


def infection_transmission_type(transmissionY):
    """
    :usage: sets the transmission_type of the infection
    :param transmissionY: the transmission_type of the infection, if '.' then no type is set
    :return: None
    """
    global infection_transmissionX
    infection_transmissionX = _parser.name(transmissionY) + '_'


def vaccine_name(nameY):
    global vaccine_nameX
    vaccine_nameX = _parser.name(nameY) + '_'
    return


def vaccine_number(numberY):
    global vaccine_numberX
    vaccine_numberX = numberY
    return


def vaccine_efficiency(efficiencyY):
    global vaccine_efficiencyX
    global vaccinated_chanceX
    vaccine_efficiencyX = efficiencyY
    vaccinated_chanceX = 1 - float(efficiencyY)
    return


def autosave():
    """
    :usage: autosave problog model text to a file in /docs
    :return:None
    """
    global auto_S
    if auto_S:
        auto_S = False
        print('autosave deactivated')
    elif not auto_S:
        auto_S = True
        print('autosave activated')


def remove_line(phrase):
    global modeltext
    new_txt = ''
    count = 0
    # get all lines
    for line in modeltext.split('\n'):
        count += 1
        # if not found
        if line.find(phrase) == -1:
            count -= 1
            # add only the lines without the phrase
            new_txt += line + '\n'
    modeltext = new_txt
    return count

def remove_phrases(phrase1, phrase2):
    global modeltext
    new_txt = ''
    count = 0
    # get all lines
    for line in modeltext.split('\n'):
        count += 1
        # if not found
        if line.find(phrase1) and line.find(phrase2) == -1:
            count -= 1
            # add only the lines without the phrase
            new_txt += line + '\n'
    modeltext = new_txt
    return count


def replace_word(old, new):
    """
    :usage: replace text in the model text
    :param old: old text
    :param new: new text
    :return:
    """
    global modeltext
    modeltext = modeltext.replace(old, new)
    print('all {} are replaced by {}'.format(old, new))


def new():
    """resets the model
        Example: model new"""
    global modeltext
    modeltext = ""
    return


def add_text(text):
    """adds pure problog model text
        Example: model model_text "0.1::vulnerable(X) :- person(X)." """
    global modeltext
    modeltext += text
    return


def model_line(line):
    """adds pure problog model text by line
        Example: model problog_code "0.1::vulnerable(X) :- person(X)." """
    global modeltext
    modeltext += line + "\n"
    return


def file(motxt):
    """
    :usage: load problog model from file
    :param motxt: problog file name
    :return: None
    """
    global modeltext
    try:
        with open(dpath + motxt, 'r') as problog_model:
            modeltext += problog_model.read()
            problog_model.close()
        print('problog model loaded successfully')
    except FileNotFoundError:
        print('file not found')
    except FileExistsError:
        print('file does not exist')
    return


def when():
    return ":-"


def negate():
    return "\+"


def __():
    return "::"


def also():
    return ', '


def end():
    return ".\n"


def t():
    return "true"


def f():
    return "false"


def person(person, i):
    return "person(" + person + "," + str(i) + ")"


def infant(infant, i):
    return "infant(" + infant + "," + str(i) + ")"


def evidence(state, bool):
    return "evidence(" + str(state) + "," + str(bool) + ")"


def csv_load(file, title):
    return "csv_load('" + str(file) + "','" + str(title) + "')"


def db_load(file):
    return "sqlite_load('" + str(file) + "')"


def lib_db():
    return 'library(db)'


def use_module(mo):
    return ":- use_module({}).\n".format(mo)


def load_vfile(file_name):
    """loads population list and their contact data from CSV files.
        Example model contact population.csv contacts.csv"""
    global modeltext
    modeltext += use_module(lib_db())
    modeltext += when() + ' ' + csv_load(path + file_name, 'vaccinated' + vaccine_numberX) + end()
    return


def contacts_data(p_file, c_file):
    """loads population list and their contact data from CSV files.
        Example model contact population.csv contacts.csv"""
    global modeltext
    modeltext += use_module(lib_db())
    modeltext += when() + ' ' + csv_load(path + p_file, 'person') + end()
    modeltext += when() + ' ' + csv_load(path + c_file, infection_transmissionX + 'contact') + end()
    return


def contact(personX, personY, i):
    return infection_transmissionX + "contact(" + str(personX) + "," + str(personY) + "," + str(i) + ")"


def deceased(person, i):
    return infection_nameX + infection_variantX + "deceased(" + str(person) + "," + str(i) + ")"


def infected(person, i):
    return infection_nameX + infection_variantX + "infected(" + str(person) + "," + str(i) + ")"


def susceptible(person, i):
    return infection_nameX + infection_variantX + "susceptible(" + str(person) + "," + str(i) + ")"


def resistant(person, i):
    return infection_nameX + infection_variantX + "resistant(" + str(person) + "," + str(i) + ")"


def recovered(person, i):
    return infection_nameX + infection_variantX + "recovered(" + str(person) + "," + str(i) + ")"


def vaccinated(person, i, n):
    return infection_nameX + infection_variantX + vaccine_nameX + "vaccinated" + str(n) + "(" + str(person) + "," + str(
        i) + ")"


def mdi(person, i):
    return infection_nameX + infection_variantX + "mdi(" + str(person) + "," + str(i) + ")"


def exposed(person, i):
    return infection_nameX + infection_variantX + "exposed(" + str(person) + "," + str(i) + ")"


def carrier(person, i):
    return infection_nameX + infection_variantX + "carrier(" + str(person) + "," + str(i) + ")"


def infected_from_carrier(person, i):
    return infection_nameX + infection_variantX + "infected_from_carrier(" + str(person) + "," + str(i) + ")"


def deceased0():
    return infection_nameX + infection_variantX + "deceased"


def infected0():
    return infection_nameX + infection_variantX + "infected"


def susceptible0():
    return infection_nameX + infection_variantX + "susceptible"


def resistant0():
    return infection_nameX + infection_variantX + "resistant"


def recovered0():
    return infection_nameX + infection_variantX + "recovered"


def vaccinated0():
    return infection_nameX + infection_variantX + vaccine_nameX + "vaccinated" + vaccine_numberX


def mdi0():
    return infection_nameX + infection_variantX + "mdi"


def exposed0():
    return infection_nameX + infection_variantX + "exposed"


def carrier0():
    return infection_nameX + infection_variantX + "carrier"


def infected_from_carrier0():
    return infection_nameX + infection_variantX + "infected_from_carrier"


def not_deceased0():
    return negate() + infection_nameX + infection_variantX + "deceased"


def not_infected0():
    return negate() + infection_nameX + infection_variantX + "infected"


def not_susceptible0():
    return negate() + infection_nameX + infection_variantX + "susceptible"


def not_resistant0():
    return negate() + infection_nameX + infection_variantX + "resistant"


def not_recovered0():
    return negate() + infection_nameX + infection_variantX + "recovered"


def not_vaccinated0():
    return negate() + infection_nameX + infection_variantX + vaccine_nameX + "vaccinated" + vaccine_numberX


def not_mdi0():
    return negate() + infection_nameX + infection_variantX + "mdi"


def not_exposed0():
    return negate() + infection_nameX + infection_variantX + "exposed"


def not_carrier0():
    return negate() + infection_nameX + infection_variantX + "carrier"


def not_infected_from_carrier0():
    return negate() + infection_nameX + infection_variantX + "infected_from_carrier"


def query(state):
    return "query(" + str(state) + ")"


def p(personX, i):
    global modeltext
    modeltext += person(personX, i) + end()


def nf(infantX, i):
    global modeltext
    modeltext += infant(infantX, i) + end()


def c0(personX, i):
    global modeltext
    modeltext += carrier(personX, i) + end()


def d0(personX, i):
    global modeltext
    modeltext += deceased(personX, i) + end()


def e0(personX, i):
    global modeltext
    modeltext += exposed(personX, i) + end()


def i0(personX, i):
    global modeltext
    modeltext += infected(personX, i) + end()


def m0(personX, i):
    global modeltext
    modeltext += mdi(personX, i) + end()


def rc0(personX, i):
    global modeltext
    modeltext += recovered(personX, i) + end()


def rs0(personX, i):
    global modeltext
    modeltext += resistant(personX, i) + end()


def s0(personX, i):
    global modeltext
    modeltext += susceptible(personX, i) + end()


def v0(personX, i):
    global modeltext
    modeltext += vaccinated(personX, i, vaccine_numberX) + end()


def not_person(personX, i):
    return negate() + person(personX, i)


def not_infant(infantX, i):
    return negate() + infant(infantX, i)


def not_deceased(person, i):
    return negate() + deceased(person, i)


def not_contact(personX, personY, i):
    return negate() + contact(personX, personY, i)


def not_infected(person, i):
    return negate() + infected(person, i)


def not_susceptible(person, i):
    return negate() + susceptible(person, i)


def not_resistant(person, i):
    return negate() + resistant(person, i)


def not_recovered(person, i):
    return negate() + recovered(person, i)


def not_vaccinated(person, i, n):
    return negate() + vaccinated(person, i, n)


def not_mdi(person, i):
    return negate() + mdi(person, i)


def not_exposed(person, i):
    return negate() + exposed(person, i)


def not_carrier(person, i):
    return negate() + carrier(person, i)


def not_infected_from_carrier(person, i):
    return negate() + infected_from_carrier(person, i)


def load_file(file_name, predicate):
    """loads population list and their contact data from CSV files.
        Example model contact population.csv contacts.csv"""
    global modeltext
    modeltext += use_module(lib_db())
    modeltext += when() + ' ' + csv_load(path + file_name, predicate) + end()
    return


def load_sql(file_name):
    """loads data from database."""
    global modeltext
    modeltext += use_module(lib_db())
    modeltext += when() + ' ' + db_load(path + file_name) + end()
    return


def contact_(x, y, t):
    global modeltext
    modeltext += contact(x, y, t) + end()
    return


def s_test(p, t, b):
    """sets a confirmed test result
        Example: model test 3 personX true"""
    global modeltext
    modeltext += evidence(susceptible(p, t), b) + end()
    return


def s_positive(p, t):
    global modeltext
    modeltext += susceptible(p, t) + end()
    return


def s_negative(p, t):
    global modeltext
    modeltext += not_susceptible(p, t) + end()
    return


def i_test(p, t, b):
    """sets a confirmed test result
        Example: model test 3 personX true"""
    global modeltext
    modeltext += evidence(infected(p, t), b) + end()
    return


def i_positive(p, t):
    global modeltext
    modeltext += infected(p, t) + end()
    return


def i_negative(p, t):
    global modeltext
    modeltext += not_infected(p, t) + end()
    return


def r_test(p, t, b):
    """sets a confirmed test result
        Example: model test 3 personX true"""
    global modeltext
    modeltext += evidence(resistant(p, t), b) + end()
    return


def r_positive(p, t):
    global modeltext
    modeltext += resistant(p, t) + end()
    return


def r_negative(p, t):
    global modeltext
    modeltext += not_resistant(p, t) + end()
    return


def d_test(p, t, b):
    """sets a confirmed test result
        Example: model test 3 personX true"""
    global modeltext
    modeltext += evidence(deceased(p, t), b) + end()
    return


def d_positive(p, t):
    global modeltext
    modeltext += deceased(p, t) + end()
    return


def d_negative(p, t):
    global modeltext
    modeltext += not_deceased(p, t) + end()
    return


def v_test(p, t, n, b):
    """sets a confirmed test result
        Example: model test 3 personX true"""
    global modeltext
    modeltext += evidence(vaccinated(p, t, n), b) + end()
    return


def v_positive(p, t, n):
    global modeltext
    modeltext += vaccinated(p, t, n) + end()
    return


def v_negative(p, t, n):
    global modeltext
    modeltext += not_vaccinated(p, t, n) + end()
    return


def m_test(p, t, b):
    """sets a confirmed test result
        Example: model test 3 personX true"""
    global modeltext
    modeltext += evidence(mdi(p, t), b) + end()
    return


def m_positive(p, t):
    global modeltext
    modeltext += mdi(p, t) + end()
    return


def m_negative(p, t):
    global modeltext
    modeltext += not_mdi(p, t) + end()
    return


def e_test(p, t, b):
    """sets a confirmed test result
        Example: model test 3 personX true"""
    global modeltext
    modeltext += evidence(exposed(p, t), b) + end()
    return


def e_positive(p, t):
    global modeltext
    modeltext += exposed(p, t) + end()
    return


def e_negative(p, t):
    global modeltext
    modeltext += not_exposed(p, t) + end()
    return


def c_test(p, t, b):
    """sets a confirmed test result
        Example: model test 3 personX true"""
    global modeltext
    modeltext += evidence(carrier(p, t), b) + end()
    return


def c_i_test(p, t, b):
    """sets a confirmed test result
        Example: model test 3 personX true"""
    global modeltext
    modeltext += evidence(infected_from_carrier(p, t), b) + end()
    return


def c_positive(p, t):
    global modeltext
    return


def c_negative(p, t):
    global modeltext
    modeltext += not_carrier(p, t) + end()
    return


def c_i_negative(p, t):
    global modeltext
    modeltext += not_infected_from_carrier(p, t) + end()
    return


def query_person(p, i):
    global modeltext
    modeltext += query(person(p, i)) + end()
    return


def query_susceptible(p, i):
    global modeltext
    modeltext += query(susceptible(p, i)) + end()
    return


def query_infected(p, i):
    global modeltext
    modeltext += query(infected(p, i)) + end()
    return


def query_recovered(p, i):
    global modeltext
    modeltext += query(recovered(p, i)) + end()
    return


def query_resistant(p, i):
    global modeltext
    modeltext += query(resistant(p, i)) + end()
    return


def query_deceased(p, i):
    global modeltext
    modeltext += query(deceased(p, i)) + end()
    return


def query_vaccinated(p, i):
    global modeltext
    modeltext += query(vaccinated(p, i, vaccine_numberX)) + end()
    return


def query_mdi(p, i):
    global modeltext
    modeltext += query(mdi(p, i)) + end()
    return


def query_exposed(p, i):
    global modeltext
    modeltext += query(exposed(p, i)) + end()
    return


def query_carrier(p, i):
    global modeltext
    modeltext += query(carrier(p, i)) + end()
    return


def query_infected_from_carrier(p, i):
    global modeltext
    modeltext += query(infected_from_carrier(p, i)) + end()
    return


def query_not_person(p, i):
    global modeltext
    modeltext += query(not_person(p, i)) + end()
    return


def query_not_susceptible(p, i):
    global modeltext
    modeltext += query(not_susceptible(p, i)) + end()
    return


def query_not_infected(p, i):
    global modeltext
    modeltext += query(not_infected(p, i)) + end()
    return


def query_not_recovered(p, i):
    global modeltext
    modeltext += query(not_recovered(p, i)) + end()
    return


def query_not_resistant(p, i):
    global modeltext
    modeltext += query(not_resistant(p, i)) + end()
    return


def query_not_deceased(p, i):
    global modeltext
    modeltext += query(not_deceased(p, i)) + end()
    return


def query_not_vaccinated(p, i):
    global modeltext
    modeltext += query(not_vaccinated(p, i, vaccine_numberX)) + end()
    return


def query_not_mdi(p, i):
    global modeltext
    modeltext += query(not_mdi(p, i)) + end()
    return


def query_not_exposed(p, i):
    global modeltext
    modeltext += query(not_exposed(p, i)) + end()
    return


def query_not_carrier(p, i):
    global modeltext
    modeltext += query(not_carrier(p, i)) + end()
    return


def query_not_infected_from_carrier(p, i):
    global modeltext
    modeltext += query(not_infected_from_carrier(p, i)) + end()
    return


def problog_probability(p, x, y):
    """adds a pure problog fact rule with a probability
        Example: model probability 0.05 vulnerable(X) vaccinated(X)"""
    global modeltext
    modeltext += p + __() + x + when() + y + end()
    return


def rule(x, y):
    """adds pure problog fact, immune(X) :- vaccinated(X)
        Example: model p_fact immune(X) vaccinated3(X)"""
    global modeltext
    modeltext += x + when() + y + end()
    return


def not_p_period(x, i, a):
    global modeltext
    modeltext += not_person(x, i) + when() + person(x, a) + end()
    return


def not_c_period(x, i, a):
    global modeltext
    modeltext += not_carrier(x, i) + when() + carrier(x, a) + end()
    return


def not_c_i_period(x, i, a):
    global modeltext
    modeltext += not_infected_from_carrier(x, i) + when() + infected_from_carrier(x, a) + end()
    return


def not_d_period(x, i, a):
    global modeltext
    modeltext += not_deceased(x, i) + when() + deceased(x, a) + end()
    return


def not_e_period(x, i, a):
    global modeltext
    modeltext += not_exposed(x, i) + when() + exposed(x, a) + end()
    return


def not_i_period(x, i, a):
    global modeltext
    modeltext += not_infected(x, i) + when() + infected(x, a) + end()
    return


def not_i_c_period(x, i, a):
    global modeltext
    modeltext += not_infected_from_carrier(x, i) + when() + infected_from_carrier(x, a) + end()
    return


def not_m_period(x, i, a):
    global modeltext
    modeltext += not_mdi(x, i) + when() + mdi(x, a) + end()
    return


def not_re_period(x, i, a):
    global modeltext
    modeltext += not_recovered(x, i) + when() + recovered(x, a) + end()
    return


def not_rs_period(x, i, a):
    global modeltext
    modeltext += not_resistant(x, i) + when() + recovered(x, a) + also() + resistant(x, a) + end()
    return


def not_s_period(x, i, a):
    global modeltext
    modeltext += not_susceptible(x, i) + when() + susceptible(x, a) + end()
    return


def not_v_period(x, i, a):
    global modeltext
    modeltext += not_vaccinated(x, i, vaccine_numberX) + when() + vaccinated(x, a, vaccine_numberX) + end()
    return


def not_p(x, i):
    global modeltext
    modeltext += not_person(x, i + 1) + when() + person(x, i) + end()
    return


def not_c_i(x, i):
    global modeltext
    modeltext += not_infected_from_carrier(x, i + 1) + when() + person(x, i) + end()
    return


def not_c(x, i):
    global modeltext
    modeltext += not_carrier(x, i + 1) + when() + person(x, i) + end()
    return


def not_d(x, i):
    global modeltext
    modeltext += not_deceased(x, i + 1) + when() + person(x, i) + end()
    return


def not_e(x, i):
    global modeltext
    modeltext += not_exposed(x, i + 1) + when() + person(x, i) + end()
    return


def not_i(x, i):
    global modeltext
    modeltext += not_infected(x, i + 1) + when() + person(x, i) + end()
    return


def not_i_c(x, i):
    global modeltext
    modeltext += not_infected_from_carrier(x, i + 1) + when() + person(x, i) + end()
    return


def not_m(x, i):
    global modeltext
    modeltext += not_mdi(x, i + 1) + when() + person(x, i) + end()
    return


def not_re(x, i):
    global modeltext
    modeltext += not_recovered(x, i + 1) + when() + person(x, i) + end()
    return


def not_rs(x, i):
    global modeltext
    modeltext += not_resistant(x, i + 1) + when() + person(x, i) + end()
    return


def not_s(x, i):
    global modeltext
    modeltext += not_susceptible(x, i + 1) + when() + person(x, i) + end()
    return


def not_v(x, i):
    global modeltext
    modeltext += not_vaccinated(x, i + 1, vaccine_numberX) + when() + person(x, i) + end()
    return


def n_percent(x, percent, i):
    global modeltext
    modeltext += str(percent) + __() + infant(x, i) + when() + person(x, i) + end()
    return


def n_period(x, i):
    global modeltext
    modeltext += person(x, i) + when() + infant(x, i) + end()
    modeltext += infant(x, i + 1) + when() + infant(x, i) + end()
    return


def qn(person, time):
    global modeltext
    modeltext += query(infant(person, time)) + end()
    return


def s(x, percent, i):
    global modeltext
    modeltext += person(x, i + 1) + when() + person(x, i) + end()
    modeltext += str(percent) + __() + susceptible(x, i) + when() + person(x, i) + end()
    modeltext += susceptible(x, i + 1) + when() + susceptible(x, i) + end()
    # modeltext += isusceptible(x,i)+when()+susceptible(x,i)+end()
    return


def qs(personX, time):
    global modeltext
    modeltext += query(person(personX, time)) + end()
    modeltext += query(susceptible(personX, time)) + end()
    return


def i(x, chance, percent, i):
    global modeltext
    modeltext += susceptible(x, i + 1) + when() + infected(x, i) + also() + not_infected(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + infected(x, i) + end()
    # modeltext += not_infected(x, i) + when() + not_susceptible(x, i) + end()
    # modeltext += susceptible(x, i) + when() + not_infected(x, i) + end()
    modeltext += str(percent) + __() + infected(x, i + 1) + when() + susceptible(x, i) + also() + not_infected(x,i) + end()
    # modeltext += str(percent) + __() + infected(x, i + 1) + when() + susceptible(x, i) + end()
    modeltext += contact(y,x, i) + when() + contact(x, y, i) + end()
    modeltext += str(chance) + __() + infected(x, i + 1) + when() + contact(x, y, i) + also() + susceptible(x,i) + also() + infected(y, i) + end()
    # modeltext += str(chance) + __() + infected(x, i + 1) + when() + susceptible(x, i) + also() + infected(y,i) + also() + contact(x, y, i) + also() + not_infected(x, i) + end()
    # modeltext += str(chance) + __() + infected(x, i + 1) + when() + susceptible(x, i) + also() + infected(y,i) + also() + contact(x, y, i) + end()
    return


def qi(person, time):
    global modeltext
    modeltext += query(infected(person, time)) + end()
    return


def i_period(x, average, i, a):
    global modeltext
    # modeltext += str(average) + __() + infected(x, i) + when() + infected(x, a) + end()
    modeltext += str(average) + __() + infected(x, i) + when() + infected(x, a) + also() + susceptible(x,
                                                                                                       i) + also() + not_resistant(
        x, i) + end()

    return


def i0_period(x, i, a):
    global modeltext
    # modeltext += infected(x, i) + when() + infected(x, a) + also() + not_resistant(x, i) + end()
    modeltext += infected(x, i) + when() + infected(x, a) + end()
    return


def i_c0_period(x, i, a):
    global modeltext
    # modeltext += infected(x, i) + when() + infected(x, a) + also() + not_resistant(x, i) + end()
    modeltext += infected(x, i) + when() + infected(x, a) + end()
    return


def r(x, chance, i):
    global modeltext
    modeltext += susceptible(x, i + 1) + when() + infected(x, i) + also() + not_infected(x,
                                                                                         i + 1) + also() + not_recovered(
        x, i + 1) + end()
    modeltext += susceptible(x, i + 1) + when() + recovered(x, i) + also() + not_recovered(x,
                                                                                           i + 1) + also() + not_resistant(
        x, i + 1) + end()
    modeltext += susceptible(x, i + 1) + when() + resistant(x, i) + also() + not_resistant(x, i + 1) + end()
    # modeltext += not_infected(x,i) + when() + resistant(x, i)+ end()
    # modeltext += susceptible(x, i + 1) + when() + recovered(x, i) + also() + not_recovered(x,i + 1) + also() + not_resistant(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + recovered(x, i) + end()
    modeltext += not_susceptible(x, i) + when() + resistant(x, i) + end()
    modeltext += recovered(x, i + 1) + when() + infected(x, i) + also() + not_infected(x, i + 1) + end()
    modeltext += str(chance) + __() + resistant(x, i) + when() + recovered(x, i) + end()
    # modeltext += resistant(x, i) + when() + recovered(x, i) + end()
    # modeltext += recovered(x, i + 1) + when() + recovered(x, i) + end()
    # modeltext += resistant(x,i+1)+when()+resistant(x,i)+end()
    # modeltext += iresistant(x,i)+when()+resistant(x,i)+end()
    return


def qr(person, time):
    global modeltext
    modeltext += query(recovered(person, time)) + end()
    modeltext += query(resistant(person, time)) + end()
    return


def r_period(x, average, i, a):
    global modeltext
    modeltext += str(average) + __() + resistant(x, i) + when() + resistant(x, a) + end()
    return


def r0_period(x, i, a):
    global modeltext
    modeltext += resistant(x, i) + when() + resistant(x, a) + end()
    return


def d(x, rate, i):
    global modeltext
    modeltext += person(x, i + 1) + when() + person(x, i) + also() + not_deceased(x, i) + end()
    modeltext += not_susceptible(x, i) + when() + deceased(x, i) + end()
    modeltext += not_infected(x, i) + when() + deceased(x, i) + end()
    modeltext += not_recovered(x, i) + when() + deceased(x, i) + end()
    modeltext += not_resistant(x, i) + when() + deceased(x, i) + end()
    # modeltext += not_person(x, i) + when() + deceased(x, i) + end()
    modeltext += deceased(x, i + 1) + when() + deceased(x, i) + end()
    modeltext += str(rate) + __() + deceased(x, i + 1) + when() + infected(x, i) + end()
    # modeltext += ideceased(x,i)+when()+deceased(x,i)+end()
    return


def qd(person, time):
    global modeltext
    modeltext += query(deceased(person, time)) + end()
    return


def v(x, percent, rate, i):
    global modeltext
    chance = abs((float(vaccinated_chanceX) - float(percent)) / (1 - float(percent)))
    modeltext += susceptible(x, i + 1) + when() + vaccinated(x, i, vaccine_numberX) + also() + not_vaccinated(x, i + 1,
                                                                                                              vaccine_numberX) + end()
    # modeltext += not_susceptible(x, i) + when() + vaccinated(x, i, vaccine_numberX) + end()
    # modeltext += susceptible(x, i) + when() + not_vaccinated(x, i, vaccine_numberX) + end()
    if int(vaccine_numberX) == 1:
        modeltext += str(rate) + __() + vaccinated(x, i + 1, vaccine_numberX) + when() + person(x, i) + end()
    elif int(vaccine_numberX) > 1:
        modeltext += str(rate) + __() + vaccinated(x, i + 1, vaccine_numberX) + when() + vaccinated(x, i,
                                                                                                    int(vaccine_numberX) - 1) + end()
    modeltext += str(chance) + __() + infected(x, i + 1) + when() + susceptible(x,i) + also() +  vaccinated(x, i,
                                                                               vaccine_numberX) + also() + infected(y,
                                                                                                                    i) + also() + contact(
        x, y, i) + also() + not_infected(x, i) + end()
    modeltext += str(percent) + __() + infected(x, i + 1) + when() + susceptible(x,i) + also() + vaccinated(x, i,
                                                                                vaccine_numberX) + also() + not_infected(
        x, i) + end()
    # modeltext += ivaccinated(x,i, vaccine_numberX) + when() + vaccinated(x,i,vaccine_numberX) + end()
    return


def qv(person, time):
    global modeltext
    modeltext += query(vaccinated(person, time, vaccine_numberX)) + end()
    return


def v_period(x, average, i, a):
    global modeltext
    modeltext += str(average) + __() + vaccinated(x, i, vaccine_numberX) + when() + vaccinated(x, a, vaccine_numberX) + end()
    return


def v0_period(x, i, a):
    global modeltext
    modeltext += vaccinated(x, i, vaccine_numberX) + when() + vaccinated(x, a, vaccine_numberX) + end()
    return


def m(x, percent, i):
    global modeltext
    # modeltext += susceptible(x, i + 1) + when() + mdi(x, i) + also() + not_mdi(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + mdi(x, i) + end()
    modeltext += not_infected(x, i) + when() + mdi(x, i) + end()
    # modeltext += susceptible(x, i) + when() + not_mdi(x, i) + end()
    modeltext += str(percent) + __() + mdi(x, i) + when() + infant(x, i) + end()
    modeltext += susceptible(x, i + 1) + when() + mdi(x, i) + also() + not_mdi(x, i + 1) + end()
    modeltext += not_infant(x, i + 2) + when() + not_mdi(x, i + 1) + also() + mdi(x, i) + end()
    modeltext += not_infant(x, i + 1) + when() + not_mdi(x, i) + end()
    # modeltext += not_mdi(x, i + 1) + when() + infected(x, i) + end()
    # modeltext += not_mdi(x, i + 1) + when() + recovered(x, i) + end()
    # modeltext += not_mdi(x, i + 1) + when() + resistant(x, i) + end()
    # modeltext += not_mdi(x, i + 1) + when() + susceptible(x, i) + end()
    # modeltext += imdi(x,i)+when()+mdi(x,i)+end()
    return


def qm(person, time):
    global modeltext
    modeltext += query(mdi(person, time)) + end()
    return


def m_period(x, average, i, a):
    global modeltext
    modeltext += str(average) + __() + mdi(x, i) + when() + mdi(x, a) + end()
    return


def m0_period(x, i, a):
    global modeltext
    modeltext += mdi(x, i) + when() + mdi(x, a) + end()
    return


def e(x, chance, percent, i):
    global modeltext
    # modeltext += susceptible(x, i + 1) + when() + susceptible(x, i) + also() + not_exposed(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + exposed(x, i) + end()
    # modeltext += susceptible(x, i) + when() + not_exposed(x, i) + end()
    #modeltext += str(percent) + __() + exposed(x, i + 1) + when() + susceptible(x, i) + end()
    modeltext += str(percent) + __() + exposed(x, i + 1) + when() + not_exposed(x, i) + also() + susceptible(x,i) + end()
    modeltext += str(chance) + __() + exposed(x, i + 1) + when() + contact(x, y, i) + also() + susceptible(x,i) + also() + infected(y, i) + end()
    modeltext += infected(x, i + 1) + when() + exposed(x, i) + also() + not_exposed(x, i + 1) + end()
    # modeltext += iexposed(x,i)+when()+exposed(x,i)+end()
    return


def qe(person, time):
    global modeltext
    modeltext += query(exposed(person, time)) + end()
    return


def e_period(x, average, i, a):
    global modeltext
    modeltext += str(average) + __() + exposed(x, i) + when() + exposed(x, a) + end()
    return


def e0_period(x, i, a):
    global modeltext
    modeltext += exposed(x, i) + when() + exposed(x, a) + end()
    return


def c(x, chance, percent, recursion, infection_period, i):
    global modeltext
    # modeltext += susceptible(x, i + 1) + when() + susceptible(x, i) + also() + not_carrier(x, i + 1) + end()
    modeltext += susceptible(x, i + 1) + when() + infected(x, i) + also() + not_infected(x,
                                                                                         i + 1) + also() + not_resistant(
        x, i + 1) + also() + not_carrier(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + carrier(x, i) + end()
    modeltext += not_susceptible(x, i) + when() + infected_from_carrier(x, i) + end()
    modeltext += str(percent) + __() + carrier(x, i + 1) + when() + infected(x, i) + also() + not_infected(x,
                                                                                                           i + 1) + end()  # fix this
    modeltext += recovered(x, i + 1) + when() + infected(x, i) + also() + not_carrier(x, i + 1) + also() + not_infected(
        x, i + 1) + end()
    modeltext += str(recursion) + __() + carrier(x, i + infection_period + infection_period + 1) + when() + infected(x,
                                                                                                                     i + infection_period + 1) + also() + carrier(
        x, i + infection_period) + also() + infected(x, i) + end()  # fix this
    modeltext += infected(x, i + 1) + when() + carrier(x, i) + also() + not_carrier(x, i + 1) + end()
    modeltext += str(chance) + __() + infected_from_carrier(x, i + 1) + when() + contact(x, y,
                                                                                         i) + also() + susceptible(x,
                                                                                                                   i) + also() + carrier(
        y, i) + end()
    # modeltext += icarrier(x,i)+when()+carrier(x,i)+end()
    return


def qc(person, time):
    global modeltext
    modeltext += query(carrier(person, time)) + end()
    return


def c_period(x, average, i, a):
    global modeltext
    modeltext += str(average) + __() + carrier(x, i) + when() + carrier(x, a) + end()
    return


def c0_period(x, i, a):
    global modeltext
    modeltext += carrier(x, i) + when() + carrier(x, a) + end()
    return


def si_model(s_percent, i_percent, i_chance, i):
    global modeltext
    modeltext += person(x, i + 1) + when() + person(x, i) + end()
    modeltext += str(s_percent) + __() + susceptible(x, i) + when() + person(x, i) + end()
    modeltext += susceptible(x, i + 1) + when() + susceptible(x, i) + end()

    modeltext += susceptible(x, i + 1) + when() + infected(x, i) + also() + not_infected(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + infected(x, i) + end()
    modeltext += str(i_percent) + __() + infected(x, i + 1) + when() + not_infected(x, i) + also() + susceptible(x,
                                                                                                                 i) + end()
    modeltext += str(i_chance) + __() + infected(x, i + 1) + when() + contact(x, y, i) + also() + susceptible(x,
                                                                                                              i) + also() + infected(
        y, i) + end()
    return


def sir_model(s_percent, i_percent, i_chance, r_chance, i):
    global modeltext
    modeltext += person(x, i + 1) + when() + person(x, i) + end()
    modeltext += str(s_percent) + __() + susceptible(x, i) + when() + person(x, i) + end()
    modeltext += susceptible(x, i + 1) + when() + susceptible(x, i) + end()

    modeltext += susceptible(x, i + 1) + when() + infected(x, i) + also() + not_infected(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + infected(x, i) + end()
    modeltext += str(i_percent) + __() + infected(x, i + 1) + when() + not_infected(x, i) + also() + susceptible(x,
                                                                                                                 i) + end()
    modeltext += str(i_chance) + __() + infected(x, i + 1) + when() + contact(x, y, i) + also() + susceptible(x,
                                                                                                              i) + also() + infected(
        y, i) + end()

    modeltext += susceptible(x, i + 1) + when() + infected(x, i) + also() + not_infected(x,
                                                                                         i + 1) + also() + not_recovered(
        x, i + 1) + end()
    modeltext += susceptible(x, i + 1) + when() + recovered(x, i) + also() + not_recovered(x,
                                                                                           i + 1) + also() + not_resistant(
        x, i + 1) + end()
    modeltext += susceptible(x, i + 1) + when() + resistant(x, i) + also() + not_resistant(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + recovered(x, i) + end()
    modeltext += not_susceptible(x, i) + when() + resistant(x, i) + end()
    modeltext += recovered(x, i + 1) + when() + infected(x, i) + also() + not_infected(x, i + 1) + end()
    modeltext += str(r_chance) + __() + resistant(x, i) + when() + recovered(x, i) + end()
    return


def sirc_model(s_percent, i_percent, i_chance, r_chance, c_percent, c_chance, c_recursion, i):
    global modeltext
    modeltext += person(x, i + 1) + when() + person(x, i) + end()
    modeltext += str(s_percent) + __() + susceptible(x, i) + when() + person(x, i) + end()
    modeltext += susceptible(x, i + 1) + when() + susceptible(x, i) + end()

    modeltext += susceptible(x, i + 1) + when() + infected(x, i) + also() + not_infected(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + infected(x, i) + end()
    modeltext += str(i_percent) + __() + infected(x, i + 1) + when() + not_infected(x, i) + also() + susceptible(x,
                                                                                                                 i) + end()
    modeltext += str(i_chance) + __() + infected(x, i + 1) + when() + contact(x, y, i) + also() + susceptible(x,
                                                                                                              i) + also() + infected(
        y, i) + end()

    modeltext += susceptible(x, i + 1) + when() + infected(x, i) + also() + not_infected(x,
                                                                                         i + 1) + also() + not_recovered(
        x, i + 1) + end()
    modeltext += susceptible(x, i + 1) + when() + recovered(x, i) + also() + not_recovered(x,
                                                                                           i + 1) + also() + not_resistant(
        x, i + 1) + end()
    modeltext += susceptible(x, i + 1) + when() + resistant(x, i) + also() + not_resistant(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + recovered(x, i) + end()
    modeltext += not_susceptible(x, i) + when() + resistant(x, i) + end()
    modeltext += recovered(x, i + 1) + when() + infected(x, i) + also() + not_infected(x, i + 1) + end()
    modeltext += str(r_chance) + __() + resistant(x, i) + when() + recovered(x, i) + end()

    modeltext += susceptible(x, i + 1) + when() + infected(x, i) + also() + not_infected(x,
                                                                                         i + 1) + also() + not_resistant(
        x, i + 1) + also() + not_carrier(x, i + 1) + end()
    modeltext += not_susceptible(x, i) + when() + carrier(x, i) + end()
    modeltext += str(c_percent) + __() + carrier(x, i + 1) + when() + infected(x, i) + end()
    modeltext += recovered(x, i + 1) + when() + infected(x, i) + also() + not_carrier(x, i + 1) + also() + not_infected(
        x, i + 1) + end()
    modeltext += str(c_recursion) + __() + carrier(x, i + 3) + when() + infected(x, i + 2) + also() + carrier(x,
                                                                                                              i + 1) + also() + infected(
        x, i) + end()
    modeltext += infected(x, i + 1) + when() + carrier(x, i) + also() + not_carrier(x, i + 1) + end()
    modeltext += str(c_chance) + __() + infected_from_carrier(x, i + 1) + when() + contact(x, y,
                                                                                           i) + also() + susceptible(x,
                                                                                                                     i) + also() + carrier(
        y, i) + end()
    return
