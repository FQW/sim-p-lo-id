import time as ti

import _history

mst = ti.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <contact>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), mst))
import numpy as np
import _model_parser
import period
import person
import csv
import itertools
import random as rndm
import _parser

data_path = 'epidemical-dsl/module/data/'
contact_inputs = {'contact': {}}
path = 'epidemical-dsl/module/data/'
sizeX = 1
countX = 0
t = '1'


def count():
    """
    :usage: prints total count
    :return: None
    """
    print('total contact count:', countX)


def time(tim):
    """
    :usage: set the time of contact
    :example: contact time 10
    :param tim: time of contact
    :return: None
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <contact> <time> <{}>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                             st, tim))
    global t
    if str(tim).isdigit():
        t = tim
    else:
        print('invalid contact time')
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <contact> <time> <{}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            ft, tim, (ft - st)))
    return


def random(s, time='this'):
    """
    :usage:set the size of random contacts
    :example: contact random 100
              contact random 20 time=34
    :param s: size of random contacts
    :return: None
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <contact> <time> <{}> <time={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            st, s, time))
    global countX
    try:
        if str(time).isdigit():
            if str(s).isdigit():
                for i in range(int(s)):
                    person1 = np.random.choice(person.population, size=1)
                    person2 = np.random.choice(person.population, size=1)
                    _model_parser.contact_(person1[0], person2[0], time)
                    countX += 1
        elif str(time) == 'all':
            if str(s).isdigit():
                for i in range(int(s)):
                    person1 = np.random.choice(person.population, size=1)
                    person2 = np.random.choice(person.population, size=1)
                    for j in range(int(period.startX), int(period.endX), int(period.stepX)):
                        _model_parser.contact_(person1[0], person2[0], j)
                        countX += 1
        elif str(s).isdigit():
            for i in range(int(s)):
                person1 = np.random.choice(person.population, size=1)
                person2 = np.random.choice(person.population, size=1)
                _model_parser.contact_(person1[0], person2[0], np.random.randint(period.startX, period.endX))
                countX += 1
        else:
            print('invalid contact time')
    except ValueError:
        print('no population, please add people')
    ft = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <contact> <time> <{}> <time={}> :{} sec.'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
        ft, s, time, (ft - st)))
    return


def between(x, y, time='default'):
    """
    :usage: adds contact info to the model
    :example: contact time 10
              contact between p1 p2
              contact between p1 p3
              contact between p10 p20 time=15
              contact between ameen ayman all.time
    :param x: person 1, can also be an infected object
    :param y: perosn 2, can also be an infected object
    :keyword time: time of contact, takes, default: setup time with 'contact time', all: all the time, random: between period start and finish or any integer
    :return: None
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <contact> <time> <{}> <{}> <time={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            st, x, y, time))
    global countX
    if str(time).isdigit():
        _model_parser.contact_(x, y, time)
        countX += 1
        contact_inputs['contact']['between'] = {'x': x, 'y': y, 't': time}
        _history.add_param(contact_inputs)
    elif time == 'all':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.contact_(x, y, i)
            countX += 1
    elif t.isdigit() and time == 'default':
        _model_parser.contact_(x, y, t)
        countX += 1
    elif time == 'random':
        _model_parser.contact_(x, y, np.random.randint(period.startX, period.endX))
        countX += 1
    contact_inputs['contact']['between'] = {'x': x, 'y': y, 't': t}
    _history.add_param(contact_inputs)
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <contact> <time> <{}> <{}> <time={}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            ft, x, y, time, (ft - st)))
    return


def remove(person1, person2):
    """
    :usage: remove contact between two individuals
    :param person: person to remove
    :return: None
    """
    _model_parser.remove_phrases(person1, person2)
    return


def _random_files(file1, file2, size='default'):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <contact> <_random_files> <{}> <{}> <contact={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            st, file1, file2, size))
    if not str(file1).endswith('.csv'):
        file1 += '.csv'
    if not str(file2).endswith('.csv'):
        file2 += '.csv'
    group1 = []
    group2 = []
    option = size.lower()
    try:
        with open(path + file1, 'r') as data1:
            for line1 in csv.reader(data1):
                group1.append(str(line1[0]))
    except FileNotFoundError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter()))
        print('file1 not found')
    except FileExistsError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter()))
        print('file1 does not exist')
    try:
        with open(path + file2, 'r') as data2:
            for line2 in csv.reader(data2):
                group2.append(str(line2[0]))
    except FileNotFoundError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter()))
        print('file2 not found')
    except FileExistsError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter()))
        print('file2 does not exist')
    if len(group1) >= 1 and len(group2) >= 1:
        product_groups = set(itertools.product(group1, group2))
        if option == 'default' and len(group1) <= len(group2):
            return rndm.sample(product_groups, len(group2))
        elif option == 'default' and len(group1) > len(group2):
            return rndm.sample(product_groups, len(group1))
        elif option == 'min':
            return rndm.sample(product_groups, 1)
        elif option == 'max':
            return rndm.sample(product_groups, product_groups.__len__())
        elif size.isdigit():
            try:
                samples = rndm.sample(set(itertools.product(group1, group2)), int(size))
                return samples
            except ValueError:
                _history._logger(
                    '{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                              ti.perf_counter()))
                print('value is too high, there is not enough data')
        else:
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                          ti.perf_counter()))
            print('invalid times, please enter "default" or an integer')
    else:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter()))
        print('not enough data, check the files')
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <contact> <_random_files> <{}> <{}> <contact={}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            ft, file1, file2, size, (ft - st)))
    return


def _cross_files(file1, file2, number='default'):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <contact> <_random_files> <{}> <{}> <contact={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            st, file1, file2, number))
    if not str(file1).endswith('.csv'):
        file1 += '.csv'
    if not str(file2).endswith('.csv'):
        file2 += '.csv'
    group1 = []
    group2 = []
    option = number.lower()
    try:
        with open(path + file1, 'r') as data1:
            for line1 in csv.reader(data1):
                group1.append(str(line1[0]))
    except FileNotFoundError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter()))
        print('file1 not found')
    except FileExistsError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter()))
        print('file1 does not exist')
    try:
        with open(path + file2, 'r') as data2:
            for line2 in csv.reader(data2):
                group2.append(str(line2[0]))
    except FileNotFoundError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter()))
        print('file2 not found')
    except FileExistsError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter()))
        print('file2 does not exist')
    if len(group1) >= 1 and len(group2) >= 1:
        groups_product = set(itertools.product(group1, group2))
        if option == 'default' and len(group1) <= len(group2):
            return rndm.sample(groups_product, len(group2))
        elif option == 'default' and len(group1) > len(group2):
            return rndm.sample(groups_product, len(group1))
        elif option == 'min':
            return rndm.sample(groups_product, len(group1) + len(group2))
        elif option == 'max':
            return rndm.sample(groups_product, len(group1) * len(group2))
        elif number.isdigit():
            try:
                samples = rndm.sample(set(itertools.product(group1, group2)),int(number))
                return samples
            except ValueError:
                _history._logger(
                    '{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                              ti.perf_counter()))
                print('value is too high, there is not enough data')
        else:
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                          ti.perf_counter()))
            print('invalid times, please enter "default" or an integer')
    else:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                   ti.perf_counter()))
        print('not enough data, check the files')
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <contact> <_random_files> <{}> <{}> <contact={}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            ft, file1, file2, number, (ft - st)))
    return


def groups(file1, file2, size='default', time='random'):
    """
    :usage: add contact between two groups each in one csv file
    :param file1: first group file
    :param file2: second group ffile
    :keyword size: takes default: size of the bigger group, min: size of group1 * group2, or max: all possible permutations
    :keyword time: time of contact, takes, default: setup time with 'contact time', all: all the time, random: between period start and finish or any integer
    :return: None
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <contact> <group> <{}> <{}> <contact={}> <time={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            st, file1, file2, size, time))
    samples = _random_files(file1, file2, size=size)
    for s in samples:
        between(s[0], s[1], time=time)
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <contact> <group> <{}> <{}> <contact={}> <time={}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            ft, file1, file2, size, time, (ft - st)))
    return


def group(file_name, number, time='random'):
    """
    :usage: adds contact file and add a contact number within the group/file
    :example: contact group list.csv 5 random.time
    :param file_name: file to be added
    :param number: percentage of contact
    :return:
    """
    names_list = person.file(file_name)
    sample = rndm.sample(set(itertools.combinations(names_list, 2)), int(number))
    for s in sample:
        between(s[0], s[1], time=time)
    return


def cross(file1, file2, total_number='default', file1_number='default', file2_number='default',
          time='random'):
    """
    :usage: add contact between two groups each in one csv file
    :param file1: first group file
    :param file2: second group ffile
    :keyword total_number: takes default: size of the bigger group, big: size of group1 * group2, or a digit
    :keyword time: time of contact, takes, default: setup time with 'contact time', all: all the time, random: between period start and finish or any integer
    :return: None
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <contact> <cross> <{}> <{}> <{}> <time={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            st, file1, file2, total_number, time))
    group(file1, file1_number)
    group(file2, file2_number)
    total_samples = _cross_files(file1, file2, number=total_number)
    for s in total_samples:
        between(s[0], s[1], time=time)
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <contact> <group> <{}> <{}> <contact={}> <time={}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            ft, file1, file2, total_number, time, (ft - st)))
    return


def library(p_file, c_file):
    """
    :usage: adds contact data from an external csv file to the model
    :example: contact library perosn.csv contact.csv
    :param p_file: CSV file with one column consists of a list of people
    :param c_file: CSV file with three columns, person 1, person 2 and time of contact
    :return: None
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <contact> <file> <{}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            st, c_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    if not str(c_file).endswith('.csv'):
        c_file += '.csv'
    try:
        with open(path + c_file, 'r') as file:
            file.close()
        person.file(p_file)
        _model_parser.contacts_data(p_file, c_file)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                      ti.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileExistsError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                    ti.perf_counter()))
        print('file does not exists')
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <contact> <file> <{}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            ft, c_file, (ft - st)))
    return


def file(c_file):
    """
    :usage: adds contact data from an external csv file to the model
    :example: contact file contact.csv
    :param c_file: CSV file with three columns, person 1, person 2 and time of contact
    :return: None
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <contact> <file> <{}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            st, c_file))
    if not str(c_file).endswith('.csv'):
        c_file += '.csv'
    try:
        with open(path + c_file, 'r') as file:
            file.close()
        _model_parser.load_file(c_file, _model_parser.infection_transmissionX + 'contact')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                      ti.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileExistsError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                    ti.perf_counter()))
        print('file does not exists')
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <contact> <file> <{}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            ft, c_file, (ft - st)))
    return


def parser(csv_fileX, time=1):
    """
    :usage: parse persons or contacts csv file into model text
    :param csv_fileX: persons or contacts csv file, comma separated
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <model> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st,
                                                           csv_fileX))
    global countX
    if not str(csv_fileX).endswith('.csv'):
        csv_fileX += '.csv'
    csv_file = open(data_path + csv_fileX, 'r')
    reader = csv.reader(csv_file, delimiter=',')
    num_col = len(next(reader))
    csv_file.seek(0)
    if num_col == 1:
        for line in reader:
            _model_parser.p(line[0], period.startX)
    elif num_col == 2:
        for line in reader:
            _model_parser.contact_(line[0], line[1], time)
            countX += 1
    elif num_col == 3:
        for line in reader:
            _model_parser.contact_(line[0], line[1], line[2])
            countX += 1
    else:
        print('invalid file format, persons file must have one column and contact file must have three')
    return


mft = ti.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <contact> :{} sec.'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), mft,
                                                       (mft - mst)))
