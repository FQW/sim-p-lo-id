import time as tim

import _history

mst = tim.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <infant>'.format(tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()),
                                                          tim.perf_counter()))
import carrier as ca
import deceased as de
import exposed as ex
import infected as inf
import mdi as md
import recovered as rec
import susceptible as su
import vaccinated as va
import person as prsn
import _model_parser
import period
import csv
import _parser

infant_population = []
infant_inputs = {'infant': {}}
countX = 0
x = 'X'
path = 'epidemical-dsl/module/data/'


def count():
    """
    :usage: prints total count
    :return: None
    """
    print('total infant population count:', countX)


def name(infantX):
    """
    :usage: adds a infant to the population
    :example: population infant p1
    :param infantX: infant to be added
    :return: None
    """
    st = tim.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infant> <name> <{}>'.format(tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()), st,
                                                             infantX))
    global countX
    countX += 1
    infantX = _parser.name(infantX)
    infant_population.append(infantX)
    prsn.population.append(infantX)
    _model_parser.nf(infantX, period.startX)
    for i in range(int(period.startX), int(period.endX), int(period.stepX)):
        _model_parser.n_period(x, i)
    infant_inputs['infant']['name'] = infantX
    _history.add_param(infant_inputs)
    ft = tim.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infant> <name> <{}> :{} sec.'.format(
            tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()), ft,
            infantX, (ft - st)))
    return


def remove(infant):
    """
    :usage: remove infant from population, which is used in query, and removes infant from prolog model text
    :param infant: infant to remove
    :return: None
    """
    global query_list
    population.remove(infant)
    _model_parser.remove_line(infant)
    return


def group(sizeX):
    """
    :usage: generates a population of the given size
    :example: infant group 100
    :param sizeX: size of the group
    :return: None
    """
    st = tim.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infant> <group> <{}>'.format(tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()), st,
                                                              sizeX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            name('infant' + str(i))
        infant_inputs['infant']['size'] = sizeX
        _history.add_param(infant_inputs)
    else:
        print('invalid population size')
    return


def percent(percentage, person='default', time='default'):
    """
    :usage: assigns percentage of infants in the population
    :example: infant percent 0.1
    :param percentage: percentage of infants
    :return: None
    """
    st = tim.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <infant> <percent> <{}>'.format(
        tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()), st, percentage))
    n = percentage.replace('.', '', 1).replace('%', '')
    if str(n).isdigit() and person == 'default' and time == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.n_percent(x, percentage, i)

    elif str(n).isdigit() and not person == 'default' and time == 'default':
        for i in range(int(period.startX), int(period.endX), int(period.stepX)):
            _model_parser.n_percent(person, percentage, i)

    elif str(n).isdigit() and person == 'default' and not time == 'default':
        _model_parser.n_percent(x, percentage, time)

    elif str(n).isdigit() and not person == 'default' and not time == 'default':
        _model_parser.n_percent(person, percentage, time)

    else:
        print('invalid infant percent')
        return
    infant_inputs['infant']['percent'] = percentage
    _history.add_param(infant_inputs)
    print('infant percent is set to ', percentage)
    ft = tim.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <infant> <percent> <{}> :{} sec.'.format(
        tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()), tim.perf_counter(), percentage, (ft - st)))

    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: infant file infant.csv
    :param p_file: CSV file with a list of names, each name is a infant
    :return: None
    """
    st = tim.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <infant> <file> <{}>'.format(tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()), st,
                                                             p_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                name(_parser.name(p_name))
    except FileNotFoundError:
        print('file not found')
    except FileExistsError:
        print('file does not exist')
    return


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: infant library infant.csv
    :param p_library: CSV file with a list of names, each name is a carrier infant
    :return: None
    """
    st = tim.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <carrier> <library> <{}>'.format(tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()),
                                                                 tim.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_file(p_library, 'infant')
    except FileNotFoundError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()),
                                                                    tim.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()),
                                                                    tim.perf_counter()))
        print('file does not exist')
    ft = tim.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <infant> <library> <{}> :{} sec.'.format(
            tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()),
            tim.perf_counter(), p_library, (ft - st)))
    return


def carrier(infantX):
    """
    :usaga: activate compartment for specific infant with the current settings and period
    :param infantX: infant to be activated
    :return: None
    """
    ca._activate(infantX)
    return


def deceased(infantX):
    """
    :usaga: activate compartment for specific infant with the current settings and period
    :param infantX: infant to be activated
    :return: None
    """
    de._activate(infantX)
    return


def exposed(infantX):
    """
    :usaga: activate compartment for specific infant with the current settings and period
    :param infantX: infant to be activated
    :return: None
    """
    ex._activate(infantX)
    return


def infected(infantX):
    """
    :usaga: activate compartment for specific infant with the current settings and period
    :param infantX: infant to be activated
    :return: None
    """
    inf._activate(infantX)
    return


def mdi(infantX):
    """
    :usaga: activate compartment for specific infant with the current settings and period
    :param infantX: infant to be activated
    :return: None
    """
    md._activate(infantX)
    return


def recovered(infantX):
    """
    :usaga: activate compartment for specific infant with the current settings and period
    :param infantX: infant to be activated
    :return: None
    """
    rec._activate(infantX)
    return


def susceptible(infantX):
    """
    :usaga: activate compartment for specific infant with the current settings and period
    :param infantX: infant to be activated
    :return: None
    """
    su._activate(infantX)
    return


def vaccinated(infantX):
    """
    :usaga: activate compartment for specific infant with the current settings and period
    :param infantX: infant to be activated
    :return: None
    """
    va._activate(infantX)
    return


def new():
    """
    :usage: clear population list which used for query
    :return: None
    """
    global population
    population = []
    return


mft = tim.perf_counter()
_history._logger('{}[INFO]{}: 🟨 finished <infant> :{} sec.'.format(tim.strftime("%S-%M-%H-%d-%m-%y", tim.localtime()),
                                                                    tim.perf_counter(), (mft - mst)))
