import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <recovered>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                             time.perf_counter()))
import csv
import configparser
import re
import _parser
import _model_parser
import period as _period
import graph as grf
import person as prsn

path = 'epidemical-dsl/module/data/'
models_path = 'epidemical-dsl/module/models/'
recovered_population = []
countX = 0
x = 'X'
st = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <deceased>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
config = configparser.SafeConfigParser(allow_no_value=True)
try:
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
chanceX = config['RECOVERED']['chance']
resistance_periodX = config['RECOVERED']['period']
ft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <recovered> loading default.ini :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, (ft - st)))
recovered_inputs = {'recovered': {}}


def activate():
    """
    :usage: adds recovered and resistant to the model
    :example: recovered activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <recovered> <activate> '.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.r(x, chanceX, i)
        if int(resistance_periodX) < 0:
            _model_parser.r0_period(x, i + 1, i)
        elif int(resistance_periodX) == 0:
            _model_parser.not_rs(x, i)
            _model_parser.not_rs_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(resistance_periodX)))):
                if int(i) == (int(i) + int(resistance_periodX) - int(a) - 1):
                    continue
                _model_parser.r0_period(x, i + int(resistance_periodX) - a - 1, i)
            _model_parser.not_rs_period(x, i + int(resistance_periodX), i)
    recovered_inputs['recovered']['activate'] = 'true'
    _history.add_param(recovered_inputs)
    print('recovered compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <recovered> <activate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))
    return


def deactivate():
    """
    usage: remove recovered from the model
    example: recovered deactivate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <recovered> <deactivate>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               time.perf_counter()))
    _model_parser.remove_line('recovered')
    _model_parser.remove_line('resistant')

    recovered_inputs['recovered']['activate'] = 'false'
    _history.add_param(recovered_inputs)
    print('recovered compartment is deactivated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <recovered> <deactivate> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), (ft - st)))
    return


def _activate(x):
    """
    :usage: adds recovered and resistant to the model
    :example: recovered activate
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <recovered> <_activate> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                   st, x))
    for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
        _model_parser.r(x, chanceX, i)
        if int(resistance_periodX) < 0:
            _model_parser.r0_period(x, i + 1, i)
        elif int(resistance_periodX) == 0:
            _model_parser.not_rs(x, i)
            _model_parser.not_rs_period(x, i + 1, i)
        else:
            for a in reversed(list(range(int(resistance_periodX)))):
                if int(i) == (int(i) + int(resistance_periodX) - int(a) - 1):
                    continue
                _model_parser.r0_period(x, i + int(resistance_periodX) - a - 1, i)
            _model_parser.not_rs_period(x, i + int(resistance_periodX), i)
    recovered_inputs['recovered']['activate'] = 'true'
    _history.add_param(recovered_inputs)
    print('recovered compartment is activated')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <recovered> <_activate> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))
    return


def chance(chanceY, recovered='this'):
    """
    :usage: assigns the chance of getting resistance to the infection
    :example: recovered chance 1
    :param chanceY: the chance of getting resistance to the infection
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <recovered> <chance> <{}> <recovered={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, chanceY, recovered))
    global chanceX
    n = chanceY.replace('.', '', 1).replace('%', '')
    option = recovered.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        chanceX = _parser.probability(config['RECOVERED']['chance'])
        recovered_inputs['recovered']['chance'] = config['RECOVERED']['chance']
        _history.add_param(recovered_inputs)
        print('recovered chance is set to {}'.format(config['RECOVERED']['chance']))
    if str(n).isdigit():
        chanceX = _parser.probability(chanceY)
        recovered_inputs['recovered']['chance'] = chanceX
        _history.add_param(recovered_inputs)
    else:
        print('invalid recovered chance')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <recovered> <chance> <{}> <recovered={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), ft, chanceY, recovered, (ft - st)))
    return


def period(duration, period='this'):
    """
    :usage: assigns the average period length for getting resistance to the infection
    :example: recovered period 24
    :param duration: the average period length for getting resistance
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <recovered> <period> <{}> <period={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, duration, period))
    global resistance_periodX
    option = period.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        resistance_periodX = _parser.probability(config['RECOVERED']['period'])
        recovered_inputs['recovered']['period'] = config['RECOVERED']['period']
        _history.add_param(recovered_inputs)
        print('recovered period is set to {}'.format(config['RECOVERED']['period']))
    elif str(duration).isdigit():
        resistance_periodX = int(duration)
        recovered_inputs['recovered']['period'] = resistance_periodX
        _history.add_param(recovered_inputs)
        print('recovered duration is set to ', resistance_periodX, _period.base_stringX + 's')
    elif str(duration) == '-1':
        resistance_periodX = -1
        print('recovered duration is set to infinity')
    else:
        print('invalid recovered period')
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <recovered> <percent> <{}> <period={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), duration, period, (ft - st)))
    return


def default(file='default'):
    """
    :usage: resets the defaults from the default.ini file
    :example: recovered default
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <recovered> <default>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    chance(config['RECOVERED']['chance'])
    period(config['RECOVERED']['period'])
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <recovered> <default> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), x, (ft - st)))

    return


def plot(graph='pos', cases='default', save='default'):
    """
    :usage: graph (plot) recovered result data
    :example: recovered plot positive-days
    :example: recovered plot negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <recovered> <plot> <recovered={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.recovered0(), 'plot', cases=cases, save=save)
        grf._neg_graph(_model_parser.recovered0(), 'plot', cases=cases, save=save)
        grf._post_graph(_model_parser.recovered0(), 'plot', cases=cases, save=save)
        grf._negt_graph(_model_parser.recovered0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.recovered0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.recovered0(), 'plot', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.recovered0(), 'plot', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.recovered0(), 'plot', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <recovered> <plot> <recovered={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def bar(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) recovered result data
    :example: recovered bar positive-weeks
    :example: recovered bar negative-months
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <recovered> <bar> <recovered={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.recovered0(), 'bar', cases=cases, save=save)
        grf._neg_graph(_model_parser.recovered0(), 'bar', cases=cases, save=save)
        grf._post_graph(_model_parser.recovered0(), 'bar', cases=cases, save=save)
        grf._negt_graph(_model_parser.recovered0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.recovered0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.recovered0(), 'bar', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.recovered0(), 'bar', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.recovered0(), 'bar', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <recovered> <bar> <recovered={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def pie(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) recovered result data
    :example: recovered bar positive
    :example: recovered bar negative
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <recovered> <pie> <recovered={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.recovered0(), 'pie', cases=cases, save=save)
        grf._neg_graph(_model_parser.recovered0(), 'pie', cases=cases, save=save)
        grf._post_graph(_model_parser.recovered0(), 'pie', cases=cases, save=save)
        grf._negt_graph(_model_parser.recovered0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.recovered0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.recovered0(), 'pie', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.recovered0(), 'pie', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.recovered0(), 'pie', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <recovered> <pie> <recovered={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def scatter(graph='pos', cases='default', save='default'):
    """
    :usage: graph (bar) recovered result data
    :example: recovered bar positive-times
    :example: recovered bar negative-times
    :keyword graph: takes pos|neg and/or day|week|month|year|quarter|season|time
    :keyword cases: takes default|all|total|new|remove|freq|time
    :keyword save: takes graph|true|yes|log|pos|neg|pos-time|neg-time
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 <recovered> <scatter> <recovered={}> <save={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, graph, save))
    if re.search('all|default', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.recovered0(), 'scatter', cases=cases, save=save)
        grf._neg_graph(_model_parser.recovered0(), 'scatter', cases=cases, save=save)
        grf._post_graph(_model_parser.recovered0(), 'scatter', cases=cases, save=save)
        grf._negt_graph(_model_parser.recovered0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._pos_graph(_model_parser.recovered0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and not re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._neg_graph(_model_parser.recovered0(), 'scatter', cases=cases, save=save)
    if re.search('pos', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._post_graph(_model_parser.recovered0(), 'scatter', cases=cases, save=save)
    if re.search('neg', graph, re.IGNORECASE) and re.search('we|da|mo|ye|qu|se|tim', graph, re.IGNORECASE):
        grf._negt_graph(_model_parser.recovered0(), 'scatter', cases=cases, save=save)
    ft = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <recovered> <scatter> <recovered={}> <save={}> :{} sec.'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), time.perf_counter(), graph, save, (ft - st)))


def person(personX, population='add'):
    """
    :usage: adds a recovered person to the population, at period start
    :example: recovered person p1
    :param personX: person to be added
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <recovered> <person> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  st,
                                                                  personX))
    global countX
    population = population.lower()
    countX += 1
    personX = _parser.name(personX)
    recovered_population.append(personX)
    if population == 'add' or population == 'append':
        prsn.population.append(personX)
    _model_parser.rc0(personX, _period.startX)
    recovered_inputs['recovered']['person'] = personX
    _history.add_param(recovered_inputs)
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <recovered> <person> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), personX, (ft - st)))
    return


def count():
    """
    :usage: prints total recovered count
    :return: None
    """
    _history._logger(
        '{}[INFO]{}: 🟩 <vaccinated> <count> -> count={}'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), countX))
    print('total recovered population count:', countX)


def group(sizeX, population='add'):
    """
    :usage: generates a population of the given size
    :example: population group 100
    :param sizeX: size of the group
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <recovered> <group> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 st, sizeX))
    if str(sizeX).isdigit():
        for i in range(int(sizeX)):
            person('r' + str(i), population=population)
        recovered_inputs['recovered']['size'] = sizeX
        _history.add_param(recovered_inputs)
    else:
        print('invalid population size')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <recovered> <group> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), sizeX, (ft - st)))
    return


def file(p_file):
    """
    :usage: adds population data from an external csv file to the model
    :example: person file person.csv
    :param p_file: CSV file with a list of names, each name is a recovered person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <recovered> <file> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                st, p_file))
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                person(_parser.name(p_name), population='add')
    except FileNotFoundError:
        print('file not found')
    except FileExistsError:
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <recovered> <file> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_file, (ft - st)))
    return


def library(p_library):
    """
    :usage: adds population data from an external csv file to the model
    :example: person library person.csv
    :param p_library: CSV file with a list of names, each name is a recovered person
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <recovered> <library> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                   time.perf_counter(), p_library))
    if not str(p_library).endswith('.csv'):
        p_library += '.csv'
    try:
        with open(path + p_library, 'r') as data:
            data.close()
        _model_parser.load_file(p_library, 'recovered')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <recovered> <library> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_library, (ft - st)))
    return


def query(persons):
    """
    :usage: queries recovered population from a file for the whole period
    :example: recovered query person.csv
    :param persons: csv file with a list of people
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <recovered> <query> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 st,
                                                                 persons))
    global modeltext
    if not str(persons).endswith('.csv'):
        persons += '.csv'
    try:
        with open(persons) as personsfile:
            subject = csv.reader(personsfile)
            subjectsList = list(subject)
        for i in range(int(_period.startX), int(_period.endX), int(_period.stepX)):
            for j in range(len(subjectsList)):
                _model_parser.query_recovered(str(subjectsList[j][0]), i)
        recovered_inputs['recovered']['query'] = persons
        _history.add_param(recovered_inputs)
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <recovered> <query> <{} :{} sec.>'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), persons, (ft - st)))
    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <recovered> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                          time.perf_counter(), (mft - mst)))
