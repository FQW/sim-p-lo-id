import time as ti

import _history

mst = ti.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <show>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), mst))
import re
import pandas as pd
import run
import _model_parser
import _parser
import period
import infection
import person as prsn
import query as qu
import webbrowser
from itertools import groupby
from operator import itemgetter
from sqlalchemy import create_engine
from tabulate import tabulate
from IPython.display import display

c_t = ti.localtime()
current_time = ti.strftime("%y-%m-%d-%H-%M-%S", c_t)
path = 'epidemical-dsl/module/docs/'
show_inputs = {'show': {}}
code_textX = ''
number_of_infected = None

def results(format='pandas'):
    count = 0
    for r in run.results:
        count += 1
        print('samele:', count)
        df = pd.DataFrame.from_dict(r, orient='index')
        if format == 'pandas':
            print(df.to_string())
        elif format == 'html':
            html = df.to_html(classes='table table-stripped', border=None, justify='center')
            html += '\n'
            html_file = open(path + current_time + 'results' + '.html', "w")
            html_file.write(html)
            html_file.close()
            print(path + current_time + 'results' + '.html saved')
            webbrowser.open((path + current_time + 'results' + '.html'))
        elif format == 'display':
            display(df)
        elif re.search(
                'plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv',
                format):
            print(tabulate(df, headers='keys', tablefmt=format, showindex='never'))
        else:
            print(df.to_string())


def _save_model(code_textY):
    global code_textX
    code_textX += code_textY
    return


def code():
    """
    :usage: show all compiled text
    :example: show code
    :return: None
    """
    print(code_textX)
    return


def _range(table):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_range> <{}>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st,
                                                             table))
    tbl = []
    for col in table:
        colm = []
        for k, g in groupby(enumerate(col), lambda x: x[0] - x[1]):
            case = (map(itemgetter(1), g))
            case = list(map(int, case))
            colm.append(len(case))
        tbl.append(colm)
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <_range> <{}> :{} sec.'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                       ft,
                                                                       table, (ft - st)))
    return tbl


def _new(ls):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_new> <{}>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, ls))
    new = [i for i in ls[0] + ls[1] if i not in ls[0]]
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <_new> <{}> :{} sec.'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                     ft, ls, (ft - st)))
    return new


def _removed(ls):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_removed> <{}>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st,
                                                               ls))
    removed = [i for i in ls[0] + ls[1] if i not in ls[1]]
    ft = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟨 finished <show> <_removed> <{}> :{} sec.'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, ls, (ft - st)))
    return removed


def _list_new(ls):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_list_new> <{}>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st,
                                                                ls))
    new_ls = [ls[0]]
    for i in range(len(ls)):
        if i >= len(ls) - 1:
            return new_ls
        new_ls.append(_new(ls[i:i + 2]))
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <_list_new> <{}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft,
            ls, (ft - st)))
    return new_ls


def _list_remove(ls):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_list_remove> <{}>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st,
                                                                   ls))
    new_ls = [[]]
    for i in range(len(ls)):
        if i >= len(ls) - 1:
            return new_ls
        new_ls.append(_removed(ls[i:i + 2]))
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <_list_remove> <{}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
            ft, ls, (ft - st)))
    return new_ls


def _sum_list(ls):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_sum_list> <{}>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st,
                                                                ls))
    sums = []
    for i in ls:
        sums.append(len(i))
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <_sum_list> <{}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft,
            ls, (ft - st)))
    return sums


def _sum_row(dict):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_sum_row> <{}>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st,
                                                               dict))
    all_values = dict.values()
    sum_row = []
    for ls in all_values:
        sum_row.append(len(ls))
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <_sum_row> <{}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft,
            dict, (ft - st)))
    return sum_row


def _person_table(dic, title, cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none',
                  person='default', sql='none', text='none', time='default', txt='none', format='display'):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_person_table> <{}> <{}> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <text={}>  <txt={}>  <format={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, dic, title, clipboard, csv, excel, html, latex, cases,
            sql, txt, txt, format))
    cases_list = []
    d_keys = []
    person = person.lower()
    time = time.lower()
    format = format.lower()
    title = title.replace('\+', 'not_')
    for k, v in dic.items():
        d_keys.append(k)
        cases_list.append(v)
    try:
        # current cases table
        df = pd.DataFrame.from_dict(dic, orient='index')
        df = df.transpose()
        df.loc[len(df.index)] = _sum_row(dic)
        tot_cols = []
        for i in range(len(df.axes[0])):
            tot_cols.append('total cases')
        df = pd.concat([pd.DataFrame.from_dict({period._base_string(): tot_cols}), df], axis=1)
        # df.insert(loc=0, column=period._base_string(), value=title)
        df.iat[-1, 0] = 'total cases'
        df.fillna('', inplace=True)
        if person == 'default':
            if time == 'default':
                pass
            elif time == 'all':
                df = df[df[dic.keys()].isin(prsn.population).any(axis=1)]
            else:
                df = df[df[int(time)].isin(prsn.population).any(axis=1)]
        elif person == 'query' or person == 'group' or person == 'list':
            if time == 'default':
                df = df[df.isin(qu.query_list).any(axis=1)]
            elif time == 'all':
                df = df[df[dic.keys()].isin(qu.query_list).any(axis=1)]
            else:
                df = df[df[int(time)].isin(qu.query_list).any(axis=1)]
        elif person == 'all':
            if time == 'default':
                df = df[df.isin(prsn.population).any(axis=1)]
            elif time == 'all':
                df = df[df[dic.keys()].isin(prsn.population).any(axis=1)]
            else:
                df = df[df[int(time)].isin(prsn.population).any(axis=1)]
        else:
            if time == 'default':
                df = df[df.isin([person]).any(axis=1)]
            elif time == 'all':
                df = df[df[dic.keys()].isin([person]).any(axis=1)]
            else:
                df = df[df[int(time)].isin([person]).any(axis=1)]
        if re.search('all|total|person|default', cases, re.IGNORECASE):
            print(title + ':')
            if format == 'pandas':
                print(df.to_string(index=False))
            elif format == 'display':
                display(df)
            elif re.search(
                    'plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv',
                    format):
                print(tabulate(df, tablefmt=format, showindex='never'))
            else:
                print(tabulate(df, tablefmt='fancy_grid', showindex='never'))
            print(str(len(dic.keys())) + ' ' + period._base_string() + 's total')
            print('\n')
        # new cases table
        new_table = _list_new(cases_list)
        ndf = pd.DataFrame(new_table, index=dic.keys())
        ndf = ndf.transpose()
        ndf.loc[len(ndf.index)] = _sum_list(new_table)
        ndf.fillna('', inplace=True)
        new_cols = []
        for i in range(len(ndf.axes[0])):
            new_cols.append('new cases')
        ndf = pd.concat([pd.DataFrame.from_dict({period._base_string(): new_cols}), ndf], axis=1)
        # ndf.insert(loc=0, column=period._base_string(), value='new case')
        ndf.iat[-1, 0] = 'total new cases'
        if person == 'default':
            if time == 'default':
                pass
            elif time == 'all':
                ndf = ndf[ndf[dic.keys()].isin(qu.query_list).any(axis=1)]
            else:
                ndf = ndf[ndf[int(time)].isin(qu.query_list).any(axis=1)]
        elif person == 'query' or person == 'group' or person == 'list':
            if time == 'default':
                ndf = ndf[ndf.isin(qu.query_list).any(axis=1)]
            elif time == 'all':
                ndf = ndf[ndf[dic.keys()].isin(qu.query_list).any(axis=1)]
            else:
                ndf = ndf[ndf[int(time)].isin(qu.query_list).any(axis=1)]
        elif person == 'all':
            if time == 'default':
                ndf = ndf[ndf.isin(prsn.population).any(axis=1)]
            elif time == 'all':
                ndf = ndf[ndf[dic.keys()].isin(prsn.population).any(axis=1)]
            else:
                ndf = ndf[ndf[int(time)].isin(prsn.population).any(axis=1)]
        else:
            if time == 'default':
                ndf = ndf[ndf.isin(person).any(axis=1)]
            elif time == 'all':
                ndf = ndf[ndf[dic.keys()].isin(person).any(axis=1)]
            else:
                ndf = ndf[ndf[int(time)].isin(person).any(axis=1)]
        if re.search('all|new|default', cases, re.IGNORECASE):
            print(title + ' new cases:')
            if format == 'pandas':
                print(ndf.to_string(index=False))
            elif format == 'display':
                display(ndf)
            elif re.search(
                    'plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv',
                    format):
                print(tabulate(ndf, tablefmt=format, showindex='never'))
            else:
                print(tabulate(ndf, tablefmt='fancy_grid', showindex='never'))
            print(str(len(dic.keys())) + ' ' + period._base_string() + 's total')
            print('\n')
        # removed cases table
        removed_table = _list_remove(cases_list)
        rdf = pd.DataFrame(removed_table, index=dic.keys())
        rdf = rdf.transpose()
        rdf.loc[len(rdf.index)] = _sum_list(removed_table)
        rdf.fillna('', inplace=True)
        rem_cols = []
        for i in range(len(rdf.axes[0])):
            rem_cols.append('removed cases')
        rdf = pd.concat([pd.DataFrame.from_dict({period._base_string(): rem_cols}), rdf], axis=1)
        # rdf.insert(loc=0, column=period._base_string(), value='removed')
        rdf.iat[-1, 0] = 'total removed cases'
        if person == 'default':
            if time == 'default':
                pass
            elif time == 'all':
                rdf = rdf[rdf[dic.keys()].isin(qu.query_list).any(axis=1)]
            else:
                rdf = rdf[rdf[int(time)].isin(qu.query_list).any(axis=1)]
        elif person == 'query' or person == 'group' or person == 'list':
            if time == 'default':
                rdf = rdf[rdf.isin(qu.query_list).any(axis=1)]
            elif time == 'all':
                rdf = rdf[rdf[dic.keys()].isin(qu.query_list).any(axis=1)]
            else:
                rdf = rdf[rdf[int(time)].isin(qu.query_list).any(axis=1)]
        elif person == 'all':
            if time == 'default':
                rdf = rdf[rdf.isin(prsn.population).any(axis=1)]
            elif time == 'all':
                rdf = rdf[rdf[dic.keys()].isin(prsn.population).any(axis=1)]
            else:
                rdf = rdf[rdf[int(time)].isin(prsn.population).any(axis=1)]
        else:
            if time == 'default':
                rdf = rdf[rdf.isin(person).any(axis=1)]
            elif time == 'all':
                rdf = rdf[rdf[dic.keys()].isin(person).any(axis=1)]
            else:
                rdf = rdf[rdf[int(time)].isin(person).any(axis=1)]
        if re.search('all|rem|default', cases, re.IGNORECASE):
            print(title + ' removed cases:')
            if format == 'pandas':
                print(rdf.to_string(index=False))
            elif format == 'display':
                display(rdf)
            elif re.search(
                    'plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv',
                    format):
                print(tabulate(rdf, tablefmt=format, showindex='never'))
            else:
                print(tabulate(rdf, tablefmt='fancy_grid', showindex='never'))
            print(str(len(dic.keys())) + ' ' + period._base_string() + 's total')
            print('\n')
        # saving
        if re.search('txt', clipboard, re.IGNORECASE):
            tdf = pd.DataFrame(df)
            tdf.append(ndf)
            tdf.append(rdf)
            tdf.to_clipboard(excel=False, sep=', ')
            print('copied to your clipboard in text format')
        if re.search('excel|default', clipboard, re.IGNORECASE):
            tdf = pd.DataFrame(df)
            tdf.append(ndf)
            tdf.append(rdf)
            tdf.to_clipboard(excel=True)
            print('copied to your clipboard in excel format')
        if re.search('save|true', csv, re.IGNORECASE):
            df.to_csv(path + current_time + '-total-' + _model_parser.inf_var_name() + title + '.csv', index=False)
            print(path + current_time + '-total-' + _model_parser.inf_var_name() + title + '.csv saved')
            ndf.to_csv(path + current_time + '-new-' + _model_parser.inf_var_name() + title + '.csv', index=False)
            print(path + current_time + '-new-' + _model_parser.inf_var_name() + title + '.csv saved')
            rdf.to_csv(path + current_time + '-removed-' + _model_parser.inf_var_name() + title + '.csv', index=False)
            print(path + current_time + '-removed-' + _model_parser.inf_var_name() + title + '.csv saved')
        if re.search('save|true', excel, re.IGNORECASE):
            with pd.ExcelWriter(path + '-total-' + _model_parser.inf_var_name() + title + '.xlsx') as writer:
                df.to_excel(writer, sheet_name=title + ' cases')
                ndf.to_excel(writer, sheet_name='new cases')
                rdf.to_excel(writer, sheet_name='removed cases')
            print(path + current_time + '-total-' + _model_parser.inf_var_name() + title + '.xlsx saved')
        if re.search('show|save|true', html, re.IGNORECASE):
            html = df.to_html(classes='table table-stripped', border=None, justify='center')
            html += '\n'
            html += ndf.to_html(classes='table table-stripped', border=None, justify='center')
            html += '\n'
            html += rdf.to_html(classes='table table-stripped', border=None, justify='center')
            html_file = open(path + current_time + '-' + _model_parser.inf_var_name() + title + '.html', "w")
            html_file.write(html)
            html_file.close()
            print(path + current_time + '-' + _model_parser.inf_var_name() + title + '.html saved')
            webbrowser.open((path + current_time + '-' + _model_parser.inf_var_name() + title + '.html'))
        if re.search('save|true', latex, re.IGNORECASE):
            ltx = df.to_latex
            ltx += '\n'
            ltx = ndf.to_latex
            ltx += '\n'
            ltx += rdf.to_latex
            ltx_file = open(path + current_time + '-' + _model_parser.inf_var_name() + title + '.tex', "w")
            ltx_file.write(ltx)
            ltx_file.close()
            print(path + current_time + '-' + title + '.tex saved')
        if re.search('save|true', sql, re.IGNORECASE):
            engine = create_engine(
                'sqlite:///' + path + current_time + '-total-' + _model_parser.inf_var_name() + title + '.db',
                echo=True)
            sqlite_connection = engine.connect()
            df.to_sql(title + ' cases', sqlite_connection, if_exists='replace')
            ndf.to_sql('new cases', sqlite_connection, if_exists='replace')
            rdf.to_sql('removed cases', sqlite_connection, if_exists='replace')
            sqlite_connection.close()
            print(path + current_time + '-total-' + _model_parser.inf_var_name() + title + '.db saved')
        if re.search('save|true', txt, re.IGNORECASE):
            df.to_csv(path + current_time + '-total-' + _model_parser.inf_var_name() + title + '.txt', index=False,
                      sep='\t')
            print(path + current_time + '-total-' + _model_parser.inf_var_name() + title + '.txt saved')
            ndf.to_csv(path + current_time + '-new-' + _model_parser.inf_var_name() + title + '.txt', index=False,
                       sep='\t')
            print(path + current_time + '-new-' + _model_parser.inf_var_name() + title + '.txt saved')
            rdf.to_csv(path + current_time + '-removed-' + _model_parser.inf_var_name() + title + '.txt', index=False,
                       sep='\t')
            print(path + current_time + '-removed-' + _model_parser.inf_var_name() + title + '.txt saved')
        if re.search('save|true', text, re.IGNORECASE):
            text_file = open(path + current_time + '-' + _model_parser.inf_var_name() + title + '.txt', "w")
            text_file.write(tabulate(df, tablefmt=format, showindex='never'))
            text_file.write(tabulate(ndf, tablefmt=format, showindex='never'))
            text_file.write(tabulate(rdf, tablefmt=format, showindex='never'))
            text_file.close()
            print(path + current_time + _model_parser.inf_var_name() + title + '.txt saved')
    except ValueError:
        _history._logger(
            '{}[ERROR]{}: 🛑 ValueError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ti.perf_counter()))
        print('No values found for ' +  title + ' individuals table')
    except KeyError:
        _history._logger(
            '{}[ERROR]{}: 🛑 KeyError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ti.perf_counter()))
        print('No values in {} {}'.format(period._base_string(), time))
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <_person_table> <{}> <{}> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <text={}>  <txt={}>  <format={}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, dic, title, clipboard, csv, excel, html, latex,
            cases, sql, text, txt, format, (ft - st)))
    return


def _time_table(dic, title, cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none',
                person='default', sql='none', text='none', time='default', txt='none', format='display'):
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_time_table> <{}> <{}> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <text={}> <txt={}> <format={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, dic, title, clipboard, csv, excel, html, latex,
            cases, sql, text, txt, format))
    times_list = []
    time = time.lower()
    format = format.lower()
    title = title.replace('\+', 'not_')
    for v in dic.values():
        times_list.append(v)
    try:
        # current cases table
        df = pd.DataFrame.from_dict(dic, orient='index')
        df = df.transpose()
        df.loc[len(df.index)] = _sum_row(dic)
        time_cols = []
        for i in range(len(df.axes[0])):
            time_cols.append(period._base_string())
        df = pd.concat([pd.DataFrame.from_dict({title: time_cols}), df], axis=1)
        # df.insert(loc=0, column=title, value=period._base_string())
        df.iat[-1, 0] = 'total period length'
        float_columns = df.select_dtypes(include=['number'])
        for col in float_columns.columns.values:
            df[col] = pd.to_numeric(df[col], errors='coerce').astype(pd.Int64Dtype()).astype(object)
        df.fillna('', inplace=True)
        if time == 'default':
            if person == 'default':
                pass
            elif person == 'query' or person == 'group' or person == 'list':
                df = df[df[qu.query_list].isin(range(period.startX, period.endX)).any(axis=1)]
            elif person == 'all':
                df = df[df[dic.keys()].isin(range(period.startX, period.endX)).any(axis=1)]
        else:
            if person == 'default':
                df = df[df.isin([int(time)]).any(axis=1)]
            elif person == 'query' or person == 'group' or person == 'list':
                df = df[df[qu.query_list].isin([int(time)]).any(axis=1)]
            elif person == 'all':
                df = df[df[dic.keys()].isin([int(time)]).any(axis=1)]
        # df.apply(lambda x[]: int(x) if type(x) == float else x)
        if re.search('all|total|time|default', cases, re.IGNORECASE):
            print(title + ':')
            if format == 'pandas':
                print(df.to_string(index=False))
            elif format == 'display':
                display(df)
            elif re.search(
                    'plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv',
                    format):
                print(tabulate(df, tablefmt=format, showindex='never'))
            else:
                print(tabulate(df, tablefmt='fancy_grid', showindex='never'))
            print(str(len(dic.keys())) + ' ' + title)
            print('\n')
        # frequency of cases table
        dfr = pd.DataFrame(_range(times_list), index=dic.keys())
        dfr = dfr.transpose()
        dfr.loc[len(dfr.index)] = _sum_list(_range(times_list))
        freq_cols = []
        # for i in range(len(dfr.axes[0])-1):
        #    freq_cols.append('number of ' + period._base_string() + 's')
        # dfr = pd.concat([pd.DataFrame.from_dict({title: time_cols}), dfr], axis=1)
        dfr.insert(loc=0, column=title, value='number of ' + period._base_string() + 's')
        dfr.iat[-1, 0] = 'number of times'
        for col in float_columns.columns.values:
            dfr[col] = pd.to_numeric(dfr[col], errors='coerce').astype(pd.Int64Dtype()).astype(object)
        dfr.fillna('', inplace=True)
        if time == 'default':
            if person == 'default':
                pass
            elif person == 'query' or person == 'group' or person == 'list':
                dfr = dfr[dfr[qu.query_list].isin(range(period.startX, period.endX)).any(axis=1)]
            elif person == 'all':
                dfr = dfr[dfr[dic.keys()].isin(range(period.startX, period.endX)).any(axis=1)]
        else:
            if person == 'default':
                dfr = dfr[dfr.isin([time]).any(axis=1)]
            elif person == 'query' or person == 'group' or person == 'list':
                dfr = dfr[dfr[qu.query_list].isin([time]).any(axis=1)]
            elif person == 'all':
                dfr = dfr[dfr[dic.keys()].isin([time]).any(axis=1)]
        if re.search('all|fr|default', cases, re.IGNORECASE):
            print(title + ' frequency:')
            if format == 'pandas':
                print(dfr.to_string(index=False))
            elif format == 'display':
                display(dfr)
            elif re.search(
                    'plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv',
                    format):
                print(tabulate(dfr, tablefmt=format, showindex='never'))
            else:
                print(tabulate(dfr, tablefmt='fancy_grid', showindex='never'))
            print(str(len(dic.keys())) + ' ' + title)
            print('\n')
        # saving
        if re.search('txt|set|false', clipboard, re.IGNORECASE):
            tdf = pd.DataFrame(df)
            tdf.append(dfr)
            tdf.to_clipboard(excel=False, sep=', ')
            print('copied to your clipboard in text format')
        if re.search('excel|true', clipboard, re.IGNORECASE):
            tdf = pd.DataFrame(df)
            tdf.append(dfr)
            tdf.to_clipboard(excel=True)
            print('copied to your clipboard in excel format')
        if re.search('save|true', csv, re.IGNORECASE):
            df.to_csv(path + _model_parser.inf_var_name() + title + '-' + current_time + '.csv', index=False)
            dfr.to_csv(path + 'frequency-' + _model_parser.inf_var_name() + title + '-' + current_time + '.csv',
                       index=False)
            print(path + _model_parser.inf_var_name() + title + '-' + current_time + '.csv saved')
            print(path + 'frequency-' + _model_parser.inf_var_name() + title + '-' + current_time + '.csv saved')
        if re.search('save|true', excel, re.IGNORECASE):
            with pd.ExcelWriter(path + _model_parser.inf_var_name() + title + '-' + current_time + '.xlsx') as writer:
                df.to_excel(writer, sheet_name=title + ' cases', index=False)
                dfr.to_excel(writer, sheet_name='cases frequency', index=False)
                print(path + _model_parser.inf_var_name() + title + '-' + current_time + '.xlsx saved')
        if re.search('show|save|true', html, re.IGNORECASE):
            html = df.to_html(classes='table table-stripped', border=None, justify='center')
            html += '\n'
            html += dfr.to_html(classes='table table-stripped', border=None, justify='center')
            html_file = open(path + _model_parser.inf_var_name() + title + '-' + current_time + '.html', "w")
            html_file.write(html)
            html_file.close()
            print(path + _model_parser.inf_var_name() + title + '-' + current_time + '.html saved')
            webbrowser.open(path + _model_parser.inf_var_name() + title + '-' + current_time + '.html')
        if re.search('save|true', latex, re.IGNORECASE):
            ltx = df.to_latex
            ltx += '\n'
            ltx += dfr.to_latex
            ltx_file = open(path + _model_parser.inf_var_name() + title + '-' + current_time + '.tex', "w")
            ltx_file.write(ltx)
            ltx_file.close()
            print(path + _model_parser.inf_var_name() + title + '-' + current_time + '.tex saved')
        if re.search('save|true', sql, re.IGNORECASE):
            engine = create_engine(
                'sqlite:///' + path + _model_parser.inf_var_name() + title + '-' + current_time + '.db', echo=True)
            sqlite_connection = engine.connect()
            df.to_sql(title + ' cases', sqlite_connection, if_exists='replace')
            dfr.to_sql('removed cases', sqlite_connection, if_exists='replace')
            sqlite_connection.close()
            print(path + _model_parser.inf_var_name() + title + '-' + current_time + '.db saved')
        if re.search('save|true', txt, re.IGNORECASE):
            df.to_csv(path + _model_parser.inf_var_name() + title + '-' + current_time + '.txt', index=False, sep='\t')
            print(path + _model_parser.inf_var_name() + title + '-' + current_time + '.txt saved')
            dfr.to_csv(path + 'frequency-' + _model_parser.inf_var_name() + title + '-' + current_time + '.txt',
                       index=False, sep='\t')
            print(path + 'frequency-' + _model_parser.inf_var_name() + title + '-' + current_time + '.txt saved')
        if re.search('save|true', text, re.IGNORECASE):
            text_file = open(path + current_time + '-' + _model_parser.inf_var_name() + title + '.txt', "w")
            text_file.write(tabulate(df, tablefmt=format, showindex='never'))
            text_file.write(tabulate(dfr, tablefmt=format, showindex='never'))
            text_file.close()
            print(path + current_time + '-' + _model_parser.inf_var_name() + title + '.txt saved')
    except ValueError:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('No values found for ' + title + ' time table')
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <_time_table> <{}> <{}> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <text={}>  <txt={}>  <format={}> :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, dic, title, clipboard, csv, excel, html, latex,
            cases, sql, text, txt, format, (ft - st)))
    return


def susceptible(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
                query='default', sql='none', test='positive', text='none', time='default', txt='none',
                format='display'):
    """
    :usage: shows the susceptible results
    :example: show susceptible save.text pretty.format
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <susceptible> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = _model_parser.not_susceptible0()
        not_current_query = _model_parser.susceptible0()
    else:
        current_query = _model_parser.susceptible0()
        not_current_query = _model_parser.not_susceptible0()
    try:
        for susceptible_per_week, not_susceptible_per_week, positive_times_per_susceptible, negative_times_per_susceptible in run.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        _sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(susceptible_per_week, current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_susceptible, current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_susceptible_per_week, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time,
                              txt=txt, format=format)
                _time_table(negative_times_per_susceptible, not_current_query, clipboard=clipboard, csv=csv,
                            excel=excel, html=html, latex=latex, cases=cases, person=person, sql=sql, text=text,
                            time=time, txt=txt,
                            format=format)
    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more susceptible data available')
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <susceptible> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return



def infected(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
             query='default', sql='none', test='positive', text='none', time='default', txt='none', format='display'):
    """
    :usage: shows the infected results
    :example: show infected save.csv
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <infected> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = _model_parser.not_infected0()
        not_current_query = _model_parser.infected0()

    else:
        current_query = _model_parser.infected0()
        not_current_query = _model_parser.not_infected0()

    try:
        for infected_per_week, not_infected_per_week, positive_times_per_infected, negative_times_per_infected in run._sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(infected_per_week, current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_infected, current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_infected_per_week, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time,
                              txt=txt, format=format)
                _time_table(negative_times_per_infected, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more infected data available')
        return
    infection.rate()
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <infected> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def recovered(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
              query='default', sql='none', test='positive', text='none', time='default', txt='none', format='display'):
    """
    :usage: shows the recovered results
    :example: show recovered txt.clipboard
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <recovered> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = _model_parser.not_recovered0()
        not_current_query = _model_parser.recovered0()

    else:
        current_query = _model_parser.recovered0()
        not_current_query = _model_parser.not_recovered0()

    try:
        for recovered_per_week, not_recovered_per_week, positive_times_per_recovered, negative_times_per_recovered in run._sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(recovered_per_week, current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_recovered, current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_recovered_per_week, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time,
                              txt=txt, format=format)
                _time_table(negative_times_per_recovered, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more susceptible data available')
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <recovered> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def resistant(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
              query='default', sql='none', test='positive', text='none', time='default', txt='none', format='display'):
    """
    :usage: shows the resistant results
    :example: show resistant
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <resistant> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = _model_parser.not_resistant0()
        not_current_query = _model_parser.resistant0()

    else:
        current_query = _model_parser.resistant0()
        not_current_query = _model_parser.not_resistant0()

    try:
        for resistant_per_week, not_resistant_per_week, positive_times_per_resistant, negative_times_per_resistant in run._sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(resistant_per_week, current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_resistant, current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_resistant_per_week, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time,
                              txt=txt, format=format)
                _time_table(negative_times_per_resistant, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, sql=sql, text=text, time=time, txt=txt, format=format)

    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more resistant data available')
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <resistant> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def deceased(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
             query='default', sql='none', test='positive', text='none', time='default', txt='none', format='display'):
    """
    :usage: shows the deceased results
    :example: show deceased save.txt
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <deceased> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = _model_parser.not_deceased0()
        not_current_query = _model_parser.deceased0()

    else:
        current_query = _model_parser.deceased0()
        not_current_query = _model_parser.not_deceased0()

    try:
        for deceased_per_week, not_deceased_per_week, positive_times_per_deceased, negative_times_per_deceased in run._sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(deceased_per_week, current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_deceased, current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_deceased_per_week, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time,
                              txt=txt, format=format)
                _time_table(negative_times_per_deceased, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)

    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more deceased data available')
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <deceased> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def vaccinated(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
               query='default', sql='none', test='positive', text='none', time='default', txt='none', format='display'):
    """
    :usage: shows the vaccinated results
    :example: show vaccinated save.text simple.format
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <vaccinated> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = _model_parser.not_vaccinated0()
        not_current_query = _model_parser.vaccinated0()

    else:
        current_query = _model_parser.vaccinated0()
        not_current_query = _model_parser.not_vaccinated0()

    try:
        for vaccinated_per_week, not_vaccinated_per_week, positive_times_per_vaccinated, negative_times_per_vaccinated in run._sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(vaccinated_per_week, current_query, clipboard=clipboard,
                              csv=csv, excel=excel, html=html, latex=latex, cases=cases, person=person, sql=sql,
                              text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_vaccinated, current_query,
                            clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, cases=cases,
                            person=person, sql=sql,
                            text=text, time=time, txt=txt, format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_vaccinated_per_week, not_current_query,
                              clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, cases=cases,
                              person=person, sql=sql,
                              text=text, time=time, txt=txt, format=format)
                _time_table(negative_times_per_vaccinated, not_current_query,
                            clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, cases=cases,
                            person=person, sql=sql,
                            text=text, time=time, txt=txt, format=format)
    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more vaccinated data available')
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <vaccinated> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def mdi(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
        query='default', sql='none', test='positive', text='none', time='default', txt='none', format='display'):
    """
    :usage: shows the mdi results
    :example: show mdi save.html
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <mdi> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = _model_parser.not_mdi0()
        not_current_query = _model_parser.mdi0()

    else:
        current_query = _model_parser.mdi0()
        not_current_query = _model_parser.not_mdi0()

    try:
        for mdi_per_week, not_mdi_per_week, positive_times_per_mdi, negative_times_per_mdi in run._sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(mdi_per_week, current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                              latex=latex,
                              cases=cases, person=person, sql=sql, text=text, time=time, txt=txt, format=format)
                _time_table(positive_times_per_mdi, current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                            latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_mdi_per_week, not_current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(negative_times_per_mdi, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html,
                            latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)

    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more mdi data available')
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <mdi> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def exposed(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
            query='default', sql='none', test='positive', text='none', time='default', txt='none', format='display'):
    """
    :usage: shows the exposed results
    :example: show exposed new.show
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <exposed> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = _model_parser.not_exposed0()
        not_current_query = _model_parser.exposed0()

    else:
        current_query = _model_parser.exposed0()
        not_current_query = _model_parser.not_exposed0()

    try:
        for exposed_per_week, not_exposed_per_week, positive_times_per_exposed, negative_times_per_exposed in run._sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(exposed_per_week, current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_exposed, current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html,
                            latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_exposed_per_week, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(negative_times_per_exposed, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)

    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more exposed data available')
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <exposed> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def carrier(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
            query='default', sql='none', test='positive', text='none', time='default', txt='none', format='display'):
    """
    :usage: shows the carrier results
    :example: show carrier
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <carrier> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = _model_parser.not_carrier0()
        not_current_query = _model_parser.carrier0()

    else:
        current_query = _model_parser.carrier0()
        not_current_query = _model_parser.not_carrier0()

    try:
        for carrier_per_week, not_carrier_per_week, positive_times_per_carrier, negative_times_per_carrier in run._sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(carrier_per_week, current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_carrier, current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html,
                            latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_carrier_per_week, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(negative_times_per_carrier, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more carrier data available')
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <carrier> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def carrier_infected(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none',
                     person='default',
                     query='default', sql='none', test='positive', text='none', time='default', txt='none',
                     format='display'):
    """
    :usage: shows the infected form carrier results
    :example: show carrier_infected save.csv
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <carrier_infected> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = _model_parser.not_infected_from_carrier0()
        not_current_query = _model_parser.infected_from_carrier0()

    else:
        current_query = _model_parser.infected_from_carrier0()
        not_current_query = _model_parser.not_infected0()

    try:
        for infected_from_carrier_per_week, not_infected_from_carrier_per_week, positive_times_per_infected_from_carrier, negative_times_per_infected_from_carrier in run._sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(infected_from_carrier_per_week, current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_infected_from_carrier, current_query, clipboard=clipboard, csv=csv,
                            excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_infected_from_carrier_per_week, not_current_query, clipboard=clipboard, csv=csv,
                              excel=excel,
                              html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time,
                              txt=txt, format=format)
                _time_table(negative_times_per_infected_from_carrier, not_current_query, clipboard=clipboard, csv=csv,
                            excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more carrier infected data available')
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <carrier_infected> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def all(cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
        query='default', sql='none', test='positive', text='none', time='default', txt='none',
        format='display'):
    model_letters = _parser.compartments(_model_parser.modeltext)
    if 'M' in model_letters:
        mdi(cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, person=person,
            query=query, sql=sql, test=test, text=text, time=time, txt=txt,
            format=format)
    if 'S' in model_letters:
        susceptible(cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, person=person,
                    query=query, sql=sql, test=test, text=text, time=time, txt=txt,
                    format=format)
    if 'E' in model_letters:
        exposed(cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, person=person,
                query=query, sql=sql, test=test, text=text, time=time, txt=txt,
                format=format)
    if 'I' in model_letters:
        infected(cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, person=person,
                 query=query, sql=sql, test=test, text=text, time=time, txt=txt,
                 format=format)
    if 'C' in model_letters:
        carrier(cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, person=person,
                query=query, sql=sql, test=test, text=text, time=time, txt=txt,
                format=format)
        carrier_infected(cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, person=person,
                         query=query, sql=sql, test=test, text=text, time=time, txt=txt,
                         format=format)
    if 'R' in model_letters:
        recovered(cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, person=person,
                  query=query, sql=sql, test=test, text=text, time=time, txt=txt,
                  format=format)
        resistant(cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, person=person,
                  query=query, sql=sql, test=test, text=text, time=time, txt=txt,
                  format=format)
    if 'V' in model_letters:
        vaccinated(cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, person=person,
                   query=query, sql=sql, test=test, text=text, time=time, txt=txt,
                   format=format)
    if 'D' in model_letters:
        deceased(cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, person=person,
                 query=query, sql=sql, test=test, text=text, time=time, txt=txt,
                 format=format)
    return


def query(find, cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none', person='default',
          query='default', sql='none', test='positive', text='none', time='default', txt='none', format='display'):
    """
    :usage: shows the carrier results
    :example: show search infected
    :param find: query to find in the results
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword person: takes person name
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword time: take a time in integer form
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <search> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = 'not_' + find
        not_current_query = find

    else:
        current_query = find
        not_current_query = 'not_' + find

    try:
        for carrier_per_week, not_carrier_per_week, positive_times_per_carrier, negative_times_per_carrier in run._sample(
                current_query):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all':
                _person_table(carrier_per_week, current_query, clipboard=clipboard, csv=csv, excel=excel, html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_carrier, current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html,
                            latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all':
                _person_table(not_carrier_per_week, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(negative_times_per_carrier, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)

    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more {} data available'.format(find))
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <search> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def search(search_query, person='none', time='none', cases='all', clipboard='none', csv='none', excel='none',
           html='none', latex='none', query='default', sql='none', test='positive', text='none', txt='none',
           format='display'):
    """
    :usage: shows the carrier results
    :example: show search infected
    :param search_query: query to find in the result ex. infected
    :keyword person: takes person name
    :keyword time: take a time in integer form
    :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
    :keyword clipboard: takes txt, excel, none or default
    :keyword csv: takes none, save or true
    :keyword excel: takes none, save or true
    :keyword html: takes none, save or true
    :keyword latex: takes none, save or true
    :keyword query: takes default, positive, or negative
    :keyword sql: takes none, save or true
    :keyword text: takes none, save or true
    :keyword txt: takes none, save or true
    :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
    :return: none
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <search> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, clipboard, csv, excel, html, latex,
            cases, sql, txt))
    test = test.lower()
    sample_number = 1
    if query.lower().startswith('neg'):
        current_query = 'not_' + search_query
        not_current_query = search_query

    else:
        current_query = search_query
        not_current_query = 'not_' + search_query
    try:
        for positive_persons_per_time, negative_persons_per_time, positive_times_per_person, negative_times_per_person in run._search(
                search_query, person=person, time=time):
            print("sample number: {}".format(sample_number))
            sample_number += sample_number
            if test == 'positive' or test == 'all' or test == 'true':
                _person_table(positive_persons_per_time, current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(positive_times_per_person, current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html,
                            latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)
            if test == 'negative' or test == 'all' or test == 'false':
                _person_table(negative_persons_per_time, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                              html=html,
                              latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                              format=format)
                _time_table(negative_times_per_person, not_current_query, clipboard=clipboard, csv=csv, excel=excel,
                            html=html, latex=latex, cases=cases, person=person, sql=sql, text=text, time=time, txt=txt,
                            format=format)

    except StopIteration:
        _history._logger('{}[ERROR]{}: 🛑 FileNotFoundError'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('no more {} data available'.format(search_query))
        return
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <show> <search> <clipboard={}> <csv={}> <excel={}> <html={}> <latex={}> <show={}> <sql={}> <txt={}>  :{} sec.'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, clipboard, csv, excel, html, latex,
            cases, sql, txt, (ft - st)))
    return


def person(person_name, time='none', cases='all', clipboard='none', csv='none', excel='none', html='none', latex='none',
           query='default', sql='none', test='positive', text='none', txt='none', format='display'):
    """
        :usage: shows the person results
        :example: show person p66
        :param person_name: name of perons to query
        :keyword time: take a time in integer form
        :keyword cases: takes all|removed|new|time|frequency|person|default to select what to show
        :keyword clipboard: takes txt, excel, none or default
        :keyword csv: takes none, save or true
        :keyword excel: takes none, save or true
        :keyword html: takes none, save or true
        :keyword latex: takes none, save or true
        :keyword query: takes default, positive, or negative
        :keyword sql: takes none, save or true
        :keyword text: takes none, save or true
        :keyword txt: takes none, save or true
        :keyword format: takes default|pandas|display|plain|simple|github|grid|fancy_grid|pipe|orgtbl|jira|presto|pretty|psql|rst|mediawiki|moinmoin|youtrack|html|unsafehtml|latex|latex_raw|latex_booktabs|latex_longtable|textile|tsv
        :return: none
        """
    print('showing results for', person_name)
    model_letters = _parser.compartments(_model_parser.modeltext)
    if 'M' in model_letters:
        search(_model_parser.inf_var_name() + 'mdi', person=person_name, time=time, cases=cases, clipboard=clipboard,
               csv=csv, excel=excel, html=html, latex=latex, query=query, sql=sql, test=test, text=text, txt=txt,
               format=format)
    if 'S' in model_letters:
        search(_model_parser.inf_var_name() + 'susceptible', person=person_name, time=time, cases=cases,
               clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, query=query, sql=sql, test=test,
               text=text, txt=txt, format=format)
    if 'E' in model_letters:
        search(_model_parser.inf_var_name() + 'exposed', person=person_name, time=time, cases=cases,
               clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, query=query, sql=sql, test=test,
               text=text, txt=txt, format=format)
    if 'I' in model_letters:
        search(_model_parser.inf_var_name() + 'infected', person=person_name, time=time, cases=cases,
               clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, query=query, sql=sql, test=test,
               text=text, txt=txt, format=format)
    if 'R' in model_letters:
        search(_model_parser.inf_var_name() + 'recovered', person=person_name, time=time, cases=cases,
               clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, query=query, sql=sql, test=test,
               text=text, txt=txt, format=format)
        search(_model_parser.inf_var_name() + 'resistant', person=person_name, time=time, cases=cases,
               clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, query=query, sql=sql, test=test,
               text=text, txt=txt, format=format)
    if 'C' in model_letters:
        search(_model_parser.inf_var_name() + 'carrier', person=person_name, time=time, cases=cases,
               clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, query=query, sql=sql, test=test,
               text=text, txt=txt, format=format)
        search(_model_parser.inf_var_name() + 'infected_from_carrier', person=person_name, time=time, cases=cases,
               clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, query=query, sql=sql, test=test,
               text=text, txt=txt, format=format)
    if 'V' in model_letters:
        search(_model_parser.inf_var_name() + 'vaccinated' + _model_parser.vaccine_numberX, person=person_name,
               time=time, cases=cases, clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, query=query,
               sql=sql, test=test, text=text, txt=txt, format=format)
    if 'D' in model_letters:
        search(_model_parser.inf_var_name() + 'deceased', person=person_name, time=time, cases=cases,
               clipboard=clipboard, csv=csv, excel=excel, html=html, latex=latex, query=query, sql=sql, test=test,
               text=text, txt=txt, format=format)
    return


def R0():
    """
    :usage:print the R0 value
    :example: show R0
    :return: None
    """
    infection.rate()
    return
def _input(module_name, function_name):
    """
    :usage: get last value entered by the user
    :example: option period base
    :param module_name: module name (first Keyword)
    :param function_name: function name (second keyword)
    :print: <module_name> <function_name> <value>
    :return: saved value
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_input>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st))
    try:
        pic = _history.param_inputsX
        print('>>', module_name, function_name, pic[module_name][function_name])
    except KeyError:
        _history._logger('{}[WARRNING]{}: 🟨 <_input>: entered value is not found in the parameters dictionary'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ti.perf_counter()))
        print('value is not entered')
        print('entered values: ')
        _inputs()
    except IndexError:
        _history._logger(
            '{}[WARRNING]{}: 🟨 <_input>: missing parameter'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                    ti.perf_counter()))
        print('missing argument')
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <_input>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft))


def _inputs():
    """
    :usage: shows all saved user inputs
    :print: all user inputs
    :return: none
    """
    _history.all_param()
    return


def _param_input(module_name, function_name, parameter):
    """
    :usage: get last value entered by the user
    :example: option period base
    :param module_name: module name (first Keyword)
    :param function_name: function name (second keyword)
    :param parameter: functions parameter (one of the arguments)
    :print: <module_name> <function_name> <value>
    :return: saved value
    """
    st = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <show> <_param_input> <{}> <{}> <{}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, module_name, function_name, parameter))
    try:
        pic = _history.param_inputsX
        print('>>', module_name, function_name, pic[module_name][function_name][parameter])
    except KeyError:
        _history._logger(
            '{}[WARRNING]{}: 🟨 <_param_input>: entered value is not found in the parameters dictionary'.format(
                ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st))
        print('value is not entered')
        print('entered values: ')
        _inputs()
    except IndexError:
        _history._logger('{}[WARRNING]{}: 🟨 <_param_input>: missing parameter'.format(
            ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st))
        print('missing parameter')
    ft = ti.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <show> <_param_input> <{}> <{}> <{}>'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, module_name, function_name, parameter))


def line(l):
    """
    :usage: print any line without spaces
    :param l: line containing no spaces
    :return: none
    """
    print(l)
    return


def model():
    """
    :usage: print out the written problog model, usually for debugging
    :return: none
    """
    print(_model_parser.modeltext)
    return


def population():
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <population>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st))
    print('Population list:')
    for p in prsn.population:
        print(p)
    ft = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <show> <population>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft))
    return


mft = ti.perf_counter()
_history._logger('{}[INFO]{}: 🟨 finished <show> :{} sec.'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()),
                                                                  ti.perf_counter(), (mft - mst)))
