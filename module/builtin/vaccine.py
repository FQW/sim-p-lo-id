import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <vaccine>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
import _parser, _model_parser
import configparser

vaccine_inputs = {'vaccine': {}}
models_path = 'epidemical-dsl/module/models/'


config = configparser.SafeConfigParser(allow_no_value=True)
try:
    config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
except FileNotFoundError:
    config.read_file(open('epidemical-dsl/module/models/default.ini'))
    print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))

vaccine_nameX = config['VACCINE']['name']
vaccine_numberX = config['VACCINE']['number']
vaccine_efficiencyX = config['VACCINE']['efficiency']


def name(nameY, name='this'):
    """
    :usage: sets the name of the vaccine, if not set, the value is taken from the default.ini file or <model_name>.ini file
    :example: vaccine name xxx vaccine() #clear also vaccine name xxx none.vaccine
              vaccine name xxx (vaccine) #default also vaccine name xxx default.vaccine
    :param nameY: the name of the vaccine
    :keyword name: takes none to clear the name, default for default values from default.ini or <model_name>.ini file
    :print: confirmation message
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccine> <name> <{}> <vaccine={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, nameY, name))
    option = name.lower()
    if option == 'none' or option == 'clear' or option == 'false':
        _model_parser.vaccine_name('')
        vaccine_inputs['vaccine']['name'] = ''
        print("vaccine name is cleared".format(nameY))
    elif option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 default.ini not found'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        _model_parser.vaccine_name(config['VACCINE']['name'])
        vaccine_inputs['vaccine']['name'] = config['VACCINE']['name']
        print("vaccine name is set to {0}".format(config['VACCINE']['name']))
    else:
        nameY = _parser.name(nameY)
        _model_parser.vaccine_name(nameY)
        vaccine_inputs['vaccine']['name'] = nameY
        print("vaccine name is set to {0}".format(nameY))
    _history.add_param(vaccine_inputs)
    return


def number(numberY, number='this'):
    """
    :usage: sets the vaccine number, if not set, the value is taken from the default.ini file or <model_name>.ini file
    :example: vaccine number 1
              vaccine number * (vaccine)
    :param numberY: vaccine number
    :keyword number: takes Default for default value form default.ini or <model_name>.ini file
    :print: confirmation message
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccine> <number> <{}> <vaccine={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, numberY, number))
    option = number.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
            _history.add_param(vaccine_inputs)
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 default.ini not found'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        _model_parser.vaccine_number(config['VACCINE']['number'])
        print("vaccine number is set to {0}".format(config['VACCINE']['number']))
        vaccine_inputs['vaccine']['number'] = config['VACCINE']['number']
    elif str(numberY).isdigit():
        _model_parser.vaccine_number(numberY)
        print("vaccine number is set to {0}".format(numberY))
        vaccine_inputs['vaccine']['number'] = numberY
        _history.add_param(vaccine_inputs)
    else:
        print('vaccine number entry is invalid')
    return


def efficiency(efficiencyY, efficiency='this'):
    """
    :usage: sets the vaccine efficiency, if set to 95, a vaccinated person has 5 percent chance of getting infected when there is contact. if not set, the value is taken from the default.ini file
    :example: vaccine efficiency 0.95
              vaccine efficiency * default.vaccine
    :param efficiencyY: vaccine efficiency
    :keyword efficiency: takes Default for default value form default.ini
    :print: confirmation message
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <vaccine> <efficiency> <{}> <vaccine={}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, efficiencyY, efficiency))
    n = efficiencyY.replace('.', '', 1).replace('%', '')
    option = efficiency.lower()
    if option == 'default':
        config = configparser.SafeConfigParser(allow_no_value=True)
        try:
            config.read_file(open('epidemical-dsl/module/models/' + _parser.config_file_name + '.ini'))
        except FileNotFoundError:
            config.read_file(open('epidemical-dsl/module/models/default.ini'))
            _history._logger(
                '{}[ERROR]{}: 🛑 default.ini not found'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               time.perf_counter()))
            print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
        _model_parser.vaccine_efficiency(config['VACCINE']['efficiency'])
        print("vaccine efficiency is set to {0}".format(config['VACCINE']['efficiency']))
        vaccine_inputs['vaccine']['efficiency'] = config['VACCINE']['efficiency']
        _history.add_param(vaccine_inputs)
    elif str(n).isdigit():
        efficiencyX = _parser.probability(efficiencyY)
        _model_parser.vaccine_efficiency(efficiencyX)
        print("vaccine efficiency is set to {0}".format(efficiencyY))
        vaccine_inputs['vaccine']['efficiency'] = efficiencyX
        _history.add_param(vaccine_inputs)
    else:
        print('vaccine efficiency entry is invalid')
    return


def default(file='default'):
    """
    :usage: resets the vaccine name, number, efficiency defaults from the default.ini file or <model_name>.ini file
    :example: vaccine default
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <vaccine> <default>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    config = configparser.SafeConfigParser(allow_no_value=True)
    try:
        if file == 'default':
            config.read_file(open(models_path + _parser.config_file_name + '.ini'))
        else:
            config.read_file(open(models_path + file + '.ini'))
    except FileNotFoundError:
        config.read_file(open('epidemical-dsl/module/models/default.ini'))
        _history._logger(
            '{}[ERROR]{}: 🛑 default.ini not found'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter()))
        print('{}.ini file is not found, it will be replaced with default.ini'.format(_parser.config_file_name))
    name(config['VACCINE']['name'])
    number(config['VACCINE']['number'])
    efficiency(config['VACCINE']['efficiency'])
    return


mft = time.perf_counter()
_history._logger(
    '{}[INFO]{}: 🟨 finished <vaccine> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                        time.perf_counter(), (mft - mst)))
