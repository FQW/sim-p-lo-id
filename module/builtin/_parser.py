import time as ti

import _history

mst = ti.perf_counter()
_history._logger('{}[INFO]{}: <_parser>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), mst))
import re
from collections import defaultdict

model_letters = ''
debugX = False
config_file_name = 'default'
version = 'v1.0'
welcome_message = """
🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻WELCOME🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻
                                            
Welcome to SimPLoID with built in Epidemical-dsl {}, a domain specific language made for epidemiologists.
SimPLoID, allows epidemiologists to create any compartmental model, by mixing any of the eight compartments:
   
[susceptible, infected, recovered, deceased, vaccinated, exposed, carrier, and mdi (maternally derived immune)].

🟩 The model can be entered line by line
🟩 The model can be compiled in the SimPLoID interpreter using compile <file_name.model>.
🟩 It can also be compiled from .model file using ./compile.py <file_name.model>
🟩 The defaults are saved in a .ini file with the same name as the model and referred to when no value is given
🟩 The defaults are also saved in default.ini file and referred to when no model specific .ini file or value is given
🟥 [IMPORTANT]: Model files (.model) and config files (.ini) must be saved in models directory 
  
💻 SimPLoID allows the user to:

🟢 Simulate any number of samples (ex: model samples 5)
🟢 set infection name (ex: infection name covid) or not (ex: infection name * name!)
🟢 set variant name (ex: infection variant delta) or not (ex: infection variant * variant!)
🟢 set any period length (ex: period length 52), period base (ex: period week)
🟢 set a new start, step, end for every change (new variant, new vaccine etc.)
🟢 add any number of infections and any number of variants and set each differently
🟢 add different vaccines for each infection-variant, and set each differently
🟢 add any number of vaccine doses, and set each differently
🟢 query and show any type of result
🟢 graph any queries 
🛑 [WARNING NOTE]: longer period with lower period base would trade computational power for more details""".format(
    version)
usage_message = """
🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻USAGE🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻
                                            
'interpret' <file_name.model>                   -->       🟢 run .model file located in the docs directory
<module_name> <function_name>                   -->       🟢 run function
'addon'|'$' <module_name> <function_name>       -->       🟢 run addon function from addon directory
'.'<directory_name> <module_name> <function_name>->       🟢 run module function from any directory under /epidemical-dsl
'help' <module_name> <optional:function_name>   -->       🟢 show documentation
'clear'                                         -->       🟢 clear the screen
'exit'|'quit'|'q'                               -->       🟢 exit
'#' <any_text>                                  -->       🟢 comment
'^'                                             -->       🟢 show last user input
'<' <new_parameters>                            -->       🟢 execute last input with new parameter values
'>'                                             -->       🟢 show all user inputs
'?'                                             -->       🟢 show all saved parameter values
'@' <module_name> <function_name>               -->       🟢 show the corresponding values entered by the user 
'<number>.' <new_parameter>                     -->       🟢 execute the corresponding user input from the user input list with new parameter values"""
func_message = """
🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻ARGUMENTS🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻

keyword arguments are optional, but when given, it must be in order (see the documentation), they can be given in three ways:

keyword=value                                   -->       🟢 used for numbers, ex: ti=44
keyword:value                                   -->       🟢 used for answers such as, yes, no, true and false, ex: show:yes
value.keyword                                   -->       🟢 used for micro-functions such as, format, graph, positive, etc. ex: display.format
default.keyword                                 -->       🟢 can be also given as       -->        keyword()
true.keyword                                    -->       🟢 can be also given as       -->        (keyword)
false.keyword                                   -->       🟢 can be also given as       -->        !keyword
none.keyword                                    -->       🟢 can be also given as       -->        keyword!
this.keyword                                    -->       🟢 can be also given as       -->        keyword{}
all.keyword                                     -->       🟢 can be also given as       -->        keyword[]
save.keyword                                    -->       🟢 can be also given as       -->        [keyword]"""
debug_message = """
🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻DEBUG🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻
run debug                                       -->       🟢 enables displaying log messages in the terminal
run log                                         -->       🟢 enables writing log messages in log file
run logger                                      -->       🟢 takes three optional arguments; enable, disable and order
                                                          🔴 for more info: help run logger """
help_message = """
🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻HELP🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻

'help'                                          -->       🟢  help usage
'help <module_name> <optional:function_name>    -->       🟢  module/function documentation
'help usage'                                    -->       🟢  usage help
'help module'                                   -->       🟢  available modules list
'help args'                                     -->       🟢  arguments help
'help debug'                                    -->       🟢  debugging and logging help"""


def successful_model():
    print("📥 model is simulated successfully")
    return


def results_parser(key):
    between_parenthesis = str(re.findall(r'\(.*?\)', str(key)).pop())
    new_key = between_parenthesis.rstrip().split(',')[0].rstrip('(')[1:]
    period = between_parenthesis.rstrip().split(',').pop().rstrip(')')
    return (new_key, period)


def probability(probabilityX):
    probabilityX = probabilityX.replace('%', '')
    if float(probabilityX) > 1 and float(probabilityX) <= 100:
        return str(float(probabilityX) / 100)
    elif float(probabilityX) <= 1 and float(probabilityX) >= 0:
        return str(probabilityX)
    else:
        print('invalid number entry detected ❗')


def sort(sample, query_type):
    """
    :usage: get four lists of dictionaries: positive_persons_per_time, negative_persons_per_time, positive_times_per_person, negative_times_per_person
    :param sample: problog sample ex {infected(p0, 1):True,infected(p2, 7):False,...}
    :param query_type: search query such as infected, carrier...
    :return: dictionaries (person:time); positive persons per time, negative persons per time,(time:person); positive tis per person, negative tis per person
             ex: {1:[p1,p2,p3],3:[p:]
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: <_parser> <sort> <{}> <{}>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st,
                                                        sample, query_type))
    positive_persons_per_time = defaultdict(list)
    negative_persons_per_time = defaultdict(list)
    positive_times_per_person = defaultdict(list)
    negative_times_per_person = defaultdict(list)
    for key, value in sample.items():
        if str(query_type).startswith('not_') or str(query_type).startswith('\+'):
            if re.search(str(query_type).replace('not_', '\+'), str(key)):
                state = results_parser(key)
                if value == True and state[1].isdigit():
                    positive_persons_per_time[int(state[1])].append(state[0])
                    positive_times_per_person[state[0]].append(int(state[1]))
                elif value == False and state[1].isdigit():
                    negative_persons_per_time[int(state[1])].append(state[0])
                    negative_times_per_person[state[0]].append(int(state[1]))
        else:
            if re.search(query_type, str(key)) and not re.search('\\+', str(key)):
                state = results_parser(key)
                if value == True and state[1].isdigit():
                    positive_persons_per_time[int(state[1])].append(state[0])
                    positive_times_per_person[state[0]].append(int(state[1]))
                elif value == False and state[1].isdigit():
                    negative_persons_per_time[int(state[1])].append(state[0])
                    negative_times_per_person[state[0]].append(int(state[1]))
    _history._logger('{}[INFO]{}: 🟩 <_parser> <sort> -> <positive_persons_per_time> <{}> '.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, positive_persons_per_time))
    _history._logger('{}[INFO]{}: 🟩 <_parser> <sort> -> <positive_times_per_person> <{}> '.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, positive_times_per_person))
    _history._logger('{}[INFO]{}: 🟩 <_parser> <sort> -> <negative_persons_per_time> <{}> '.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, negative_persons_per_time))
    _history._logger('{}[INFO]{}: 🟩 <_parser> <sort> -> <negative_times_per_person> <{}> '.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, negative_times_per_person))
    ft = ti.perf_counter()
    _history._logger('{}[FINISH]{}: <_parser> <sort> <{}> <{}> : {} sec.'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, sample, query_type, (ft - st)))
    return positive_persons_per_time, negative_persons_per_time, positive_times_per_person, negative_times_per_person


def search(sample, query, person='none', time='none'):
    """
    :usage: get four lists of dictionaries: positive_persons_per_ti, negative_persons_per_ti, positive_tis_per_person, negative_tis_per_person
    :param sample: problog sample ex {infected(p0, 1):True,infected(p2, 7):False,...}
    :param query: search query such as infected, carrier...
    :return: dictionaries (person:ti); positive persons per ti, negative persons per ti,(ti:person); positive tis per person, negative tis per person
             ex: {1:[p1,p2,p3],3:[p:]
    """
    st = ti.perf_counter()
    _history._logger(
        '{}[INFO]{}: <_parser> <search> <{}> <{}>'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st,
                                                          sample, query))
    positive_persons_per_time = defaultdict(list)
    negative_persons_per_time = defaultdict(list)
    positive_tis_per_person = defaultdict(list)
    negative_tis_per_person = defaultdict(list)
    for key, value in sample.items():
        state = results_parser(key)
        if re.search(query, str(key)) and person == 'none' and time == 'none':

            if value == True:
                positive_persons_per_time[int(state[1])].append(state[0])
                positive_tis_per_person[state[0]].append(int(state[1]))
            elif value == False:
                negative_persons_per_time[int(state[1])].append(state[0])
                negative_tis_per_person[state[0]].append(int(state[1]))

        elif re.search(query, str(key)) and not person == 'none' and not time == 'none':
            if value == True:
                if person == state[0] or time == state[1]:
                    positive_persons_per_time[int(state[1])].append(state[0])
                    positive_tis_per_person[state[0]].append(int(state[1]))
            elif value == False:
                if person == state[0] or time == state[1]:
                    negative_persons_per_time[int(state[1])].append(state[0])
                    negative_tis_per_person[state[0]].append(int(state[1]))
        elif re.search(query, str(key)) and person == 'none' and not time == 'none':
            if value == True:
                if time == state[1]:
                    positive_persons_per_time[int(state[1])].append(state[0])
                    positive_tis_per_person[state[0]].append(int(state[1]))
            elif value == False:
                if time == state[1]:
                    negative_persons_per_time[int(state[1])].append(state[0])
                    negative_tis_per_person[state[0]].append(int(state[1]))

        elif re.search(query, str(key)) and not person == 'none' and time == 'none':
            if value == True:
                if person == state[0]:
                    positive_persons_per_time[int(state[1])].append(state[0])
                    positive_tis_per_person[state[0]].append(int(state[1]))
            elif value == False:
                if person == state[0]:
                    negative_persons_per_time[int(state[1])].append(state[0])
                    negative_tis_per_person[state[0]].append(int(state[1]))

    _history._logger('{}[INFO]{}: 🟩 <_parser> <search> -> <positive_persons_per_ti> <{}> '.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, positive_persons_per_time))
    _history._logger('{}[INFO]{}: 🟩 <_parser> <search> -> <positive_tis_per_person> <{}> '.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, positive_tis_per_person))
    _history._logger('{}[INFO]{}: 🟩 <_parser> <search> -> <negative_persons_per_ti> <{}> '.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, negative_persons_per_time))
    _history._logger('{}[INFO]{}: 🟩 <_parser> <search> -> <negative_tis_per_person> <{}> '.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), st, negative_tis_per_person))
    ft = ti.perf_counter()
    _history._logger('{}[FINISH]{}: <_parser> <search> <{}> <{}> : {} sec.'.format(
        ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), ft, sample, query, (ft - st)))
    return positive_persons_per_time, negative_persons_per_time, positive_tis_per_person, negative_tis_per_person


def not_found(function_name):
    return "{0} is not found...".format(function_name)


def compile_usage():
    print('usage  : ./interpreter.py <model_name.model>')
    return


def help_usage():
    print('usage  : help usage')
    print('usage  : help module')
    print('usage  : help args')
    print('usage  : help debug')
    print('usage  : help <module_name> ')
    print('usage  : help <module_name> <function_name>')
    return


def c_help_usage():
    print('usage  : ./interpreter.py -help <module_name> ')
    print('usage  : ./interpreter.py -h <module_name> <function_name>')
    return


def example1(module, function):
    print('Example: help {0} {1}'.format(module, function))
    return


def example0(module):
    print('Example: help {0}'.format(module))
    return


def print_modules(modules_list):
    print('\n')
    print("available keywords are:", end=' ')
    try:
        print(*modules_list, sep=' | ', end=' 📌')
    except TypeError:
        return
    except IndexError:
        return
    print('\n')
    return


def clear_screen(entry):
    if entry[0].startswith('clear'):
        for i in range(30):
            print('>>\n')
    return


def print_help_page(modules_list, module, function):
    help_usage()
    example0(module)
    example1(module, function)
    print_modules(modules_list)
    return


def config_name(name):
    global config_file_name
    if str(name).endswith('.model'):
        name = str(name).replace('.model', '')
    if str(name).endswith('.ini'):
        name = str(name).replace('.ini', '')
    config_file_name = name
    print('config file is set to {}.ini'.format(config_file_name))
    return


def debug(state):
    """
    :usage: debugging
    :print: if toggled (+confirmation), all problog sample results
    :return: None
    """
    global debugX
    debugX = state
    if state:
        print('will show sorted problog model results')
    if not state:
        print('will not show sorted problog model results')


def name(nameY):
    nameX = str(nameY)
    nameX = nameX[0].lower() + nameX[1:]
    nameX = nameX.replace('.', '')
    nameX = nameX.replace(',', '')
    return nameX


def compartments(modeltext):
    """
    :usage: checks the model and prints its name
    :return: None
    """
    global model_letters
    model_letters = ''
    if 'mdi' in modeltext:
        model_letters += 'M'
    if 'susceptible' in modeltext:
        model_letters += 'S'
    if 'exposed' in modeltext:
        model_letters += 'E'
    if 'infected' in modeltext:
        model_letters += 'I'
    if 'recovered' in modeltext:
        model_letters += 'R'
    if 'carrier' in modeltext:
        model_letters += 'C'
    if 'vaccinated' in modeltext:
        model_letters += 'V'
    if 'deceased' in modeltext:
        model_letters += 'D'
    if 'susceptible' in modeltext:
        model_letters += 'S'
    return model_letters


mft = ti.perf_counter()
_history._logger(
    '{}[INFO]{}: <_parser> : {} sec.'.format(ti.strftime("%S-%M-%H-%d-%m-%y", ti.localtime()), mft, (mft - mst)))
