import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <run>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
from problog.program import PrologString
from problog.tasks import sample as p_sample
import model as mdl
import _parser
import _model_parser
import csv

path = 'epidemical-dsl/module/data/'
c_t = time.localtime()
current_time = time.strftime("%y-%m-%d-%H-%M-%S", c_t)
results = []
model_letters = ''


def model():
    """
    :usage: simulates the compartmental model, at least two compartments need to be activated beforehand for it to function properly
    :example: susceptible activate
              infected activate
              simulate model
    :print: confirmation message
    :return: Nome
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <run> <model>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st))
    results.clear()
    model_letters = _parser.compartments(_model_parser.modeltext)
    if len(model_letters) >= 2:
        print('{} model is detected'.format(model_letters))
    else:
        print('not enough compartments activated, check your model please!')
        return
    print('model is being simulated, please wait... ⏳')
    _history._logger('{}[INFO]{}: 🟩 starting simulation'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter()))
    modelX = PrologString(_model_parser.modeltext)
    _history._logger('{}[INFO]{}: 🟩 problog text added'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                time.perf_counter()))
    result = p_sample.sample(modelX, n=int(mdl.samples_quantityX), format=mdl.format, propagate_evidence=mdl.propagate_evidenceX, distributions=mdl.distributionsX, progress=mdl.progressX)
    _history._logger('{}[INFO]{}: 🟩 samples initiated'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                               time.perf_counter()))
    count = 0
    for r in result:
        count += 1
        r_start = time.perf_counter()
        _history._logger(
            '{}[INFO]{}: 🟩 sample {} started'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), r_start,
                                                      count))
        _history._logger('{}[INFO]{}: 🟩 sample {}: \n{}'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), count, r))

        try:
            results.append(r)
        except StopIteration:
            _history._logger(
                '{}[ERROR]{}: 🛑 StopIteration'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
            continue
        r_stop = time.perf_counter()
        _history._logger(
            '{}[INFO]{}: 🟩 sample {} finished'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter(), count))
        #print("done in {} seconds".format(r_stop - r_start))

        print('\n🟨 processing next sample... ⏳')
        print('press Ctrl + C to interrupt')
    print('🟥 all samples simulated ⌛')
    _history._logger('{}[INFO]{}: 🟩 all samples finished'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  time.perf_counter()))
    _parser.successful_model()
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <run> <model>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter(), (ft - st)))
    print("finished in {} seconds".format(ft - st))
    return


def _sample(query_type):
    """
    :param query_type: susceptible or infected or recovered etc...
    :yield: positive_persons_per_time, negative_persons_per_time, positive_times_per_person, negative_times_per_person
    :return: None
    """
    for r in results:
        positive_persons_per_time, negative_persons_per_time, positive_times_per_person, negative_times_per_person = _parser.sort(
            r, query_type)
        yield positive_persons_per_time, negative_persons_per_time, positive_times_per_person, negative_times_per_person
    return


def _search(query, person='none', time='dafault'):
    """
        :param query_type: susceptible or infected or recovered etc...
        :yield: positive_persons_per_time, negative_persons_per_time, positive_times_per_person, negative_times_per_person
        :return: None
        """
    for r in results:
        positive_persons_per_time, negative_persons_per_time, positive_times_per_person, negative_times_per_person = _parser.search(
            r, query, person=person, time=time)
        yield positive_persons_per_time, negative_persons_per_time, positive_times_per_person, negative_times_per_person
    return


def logger(enable='file', disable='none', order='this', clear='false'):
    """
    :usage: setup the logger
    :example: run logger file.enable terminal.disable default.order
    :keyword enable: all|none|file|console|terminal|default which is 'none'
    :keyword disable: all|none|file|console|terminal|default which is 'all'
    :keyword order: ascending|descending|lifo|filo|default which is ascending
    :keyword clear: all|true|none|false to clear the log file content
    :return:
    """
    _history.log(enable=enable, disable=disable, order=order, clear=clear)
    return


def debug():
    """
    :usage: debugging, displays log messages in terminal
    :return: None
    """
    print('log messages will be displayed in the terminal')
    _history.log(enable='terminal', disable='file', order='default')
    return


def log():
    """
    :usage: logging, saves log messages in log file
    :return: None
    """
    print('log messages will be saved in /module/log')
    _history.log(enable='file', disable='terminal', order='default')


def fixer(p_file):
    """
    :usage: fixed the names in person file to be problog compatible
    :example: run fixer person.csv
    :param p_file: CSV file with a list of names
    :return: None
    """
    st = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟩 started <run> <fixer> <{}>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                           time.perf_counter(), p_file))
    population = []
    if not str(p_file).endswith('.csv'):
        p_file += '.csv'
    try:
        with open(path + p_file, 'r') as data:
            for line in csv.reader(data):
                p_name = str(line[0])
                population.append(_parser.name(p_name))
            data.close()
        with open(path + p_file, 'w') as data:
            writer = csv.writer(data)
            for person in population:
                writer.writerow('"' + str(person) + '"')
    except FileNotFoundError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file not found')
    except FileExistsError:
        _history._logger(
            '{}[ERROR]{}: 🛑 FileNotFoundError'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                       time.perf_counter()))
        print('file does not exist')
    ft = time.perf_counter()
    _history._logger(
        '{}[INFO]{}: 🟨 finished <run> <fixer> <{}> :{} sec.'.format(
            time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
            time.perf_counter(), p_file, (ft - st)))
    return


mft = time.perf_counter()
_history._logger('{}[INFO]{}: 🟨 finished <run> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                 time.perf_counter(), (mft - mst)))
