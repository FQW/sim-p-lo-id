import time

import _history

mst = time.perf_counter()
_history._logger('{}[INFO]{}: 🟩 started <test>'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                        time.perf_counter()))
import re
import _model_parser
import period

test_inputs = {'test': {}}


def susceptible(p, t, case):
    """
    :usage: sets a person susceptible either positive or negative in a specific time
    :example: test susceptible p1 1 neg
    :param p: person
    :param t: time
    :param case: takes pos, positive, yes, true +, neg, negative, no, false - (ignoring letter case)
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <test> <susceptible> <{}> <{}> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, p, t, case))
    if re.search('pos|yes|true|\+', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.s_positive(p, i)
        else:
            _model_parser.s_positive(p, t)
    elif re.search('neg|no|false|\-', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.s_negative(p, i)
        else:
            _model_parser.s_negative(p, t)
    test_inputs['test']['susceptible'] = {'person': p, 'time': t, 'case': case}
    _history.add_param(test_inputs)
    return


def infected(p, t, case):
    """
    :usage: sets a person infected either positive or negative in a specific time
    :example: test infected p2 1 pos
    :param p: person
    :param t: time
    :param case: takes pos, positive, yes, true +, neg, negative, no, false - (ignoring letter case)
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <test> <infected> <{}> <{}> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, p, t, case))

    if re.search('pos|yes|true|\+', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.i_positive(p, i)
        else:
            _model_parser.i_positive(p, t)
    elif re.search('neg|no|false|\-', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.i_negative(p, i)
        else:
            _model_parser.i_negative(p, t)
    test_inputs['test']['infected'] = {'person': p, 'time': t, 'case': case}
    _history.add_param(test_inputs)
    return


def recovered(p, t, case):
    """
    :usage: sets a person recovered either positive or negative in a specific time
    :example: test recovered p3 5 TRUE
    :param p: person
    :param t: time
    :param case: takes pos, positive, yes, true +, neg, negative, no, false - (ignoring letter case)
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <test> <recovered> <{}> <{}> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, p, t, case))

    if re.search('pos|yes|true|\+', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.r_positive(p, i)
        else:
            _model_parser.r_positive(p, t)
    elif re.search('neg|no|false|\-', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.r_negative(p, i)
        else:
            _model_parser.r_negative(p, t)
    test_inputs['test']['recovered'] = {'person': p, 'time': t, 'case': case}
    _history.add_param(test_inputs)
    return


def deceased(p, t, case):
    """
    :usage: sets a person deceased either positive or negative in a specific time
    :example: test deceased p6 9 yes
    :param p: person
    :param t: time
    :param case: takes pos, positive, yes, true +, neg, negative, no, false - (ignoring letter case)
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <test> <deceased> <{}> <{}> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, p, t, case))

    if re.search('pos|yes|true|\+', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.d_positive(p, i)
        else:
            _model_parser.d_positive(p, t)
    elif re.search('neg|no|false|\-', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.d_negative(p, i)
        else:
            _model_parser.d_negative(p, t)
    test_inputs['test']['deceased'] = {'person': p, 'time': t, 'case': case}
    _history.add_param(test_inputs)
    return


def vaccinated(p, t, n, case):
    """
    :usage: sets a person vaccinated either positive or negative in a specific time
    :example: test vaccinated p5 7 false
    :param p: person
    :param t: time
    :param case: takes pos, positive, yes, true +, neg, negative, no, false - (ignoring letter case)
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <test> <vaccinated> <{}> <{}> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, p, t, case))

    if re.search('pos|yes|true|\+', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.v_positive(p, i, n)
        else:
            _model_parser.v_positive(p, t, n)
    elif re.search('neg|no|false|\-', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.v_negative(p, i, n)
        else:
            _model_parser.v_negative(p, t, n)
    test_inputs['test']['vaccinated'] = {'person': p, 'time': t, 'case': case}
    _history.add_param(test_inputs)
    return


def mdi(p, t, case):
    """
    :usage: sets a person maternally derived immune either or not in a specific time
    :example: test mdi p6 1 neg
    :param p: person
    :param t: time
    :param case: takes pos, positive, yes, true +, neg, negative, no, false - (ignoring letter case)
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <test> <mdi> <{}> <{}> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, p, t, case))

    if re.search('pos|yes|true|\+', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.m_positive(p, i)
        else:
            _model_parser.m_positive(p, t)
    elif re.search('neg|no|false|\-', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.m_negative(p, i)
        else:
            _model_parser.m_negative(p, t)
    test_inputs['test']['mdi'] = {'person': p, 'time': t, 'case': case}
    _history.add_param(test_inputs)
    return


def exposed(p, t, case):
    """
    :usage: sets a person exposed either positive or negative in a specific time
    :example: test exposed p7 3 POS
    :param p: person
    :param t: time
    :param case: takes pos, positive, yes, true +, neg, negative, no, false - (ignoring letter case)
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <test> <exposed> <{}> <{}> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, p, t, case))

    if re.search('pos|yes|true|\+', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.e_positive(p, i)
        else:
            _model_parser.e_positive(p, t)
    elif re.search('neg|no|false|\-', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.e_negative(p, i)
        else:
            _model_parser.e_negative(p, t)
    test_inputs['test']['exposed'] = {'person': p, 'time': t, 'case': case}
    _history.add_param(test_inputs)
    return


def carrier(p, t, case):
    """
    :usage: sets a person carrier either positive or negative in a specific time
    :example: test carrier p8 3 POSITIVE
    :param p: person
    :param t: time
    :param case: takes pos, positive, yes, true +, neg, negative, no, false - (ignoring letter case)
    :return: None
    """
    st = time.perf_counter()
    _history._logger('{}[INFO]{}: 🟩 started <test> <carrier> <{}> <{}> <{}>'.format(
        time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()), st, p, t, case))

    if re.search('pos|yes|true|\+', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.c_positive(p, i)
        else:
            _model_parser.c_positive(p, t)
    elif re.search('neg|no|false|\-', case, re.IGNORECASE):
        if re.search('all', t, re.IGNORECASE):
            for i in range(int(period.startX), int(period.endX), int(period.stepX)):
                _model_parser.c_negative(p, i)
        else:
            _model_parser.c_negative(p, t)
    test_inputs['test']['carrier'] = {'person': p, 'time': t, 'case': case}
    _history.add_param(test_inputs)
    return


mft = time.perf_counter()
_history._logger('{}[INFO]{}: 🟨 finished <test> :{} sec.'.format(time.strftime("%S-%M-%H-%d-%m-%y", time.localtime()),
                                                                  time.perf_counter(), (mft - mst)))
