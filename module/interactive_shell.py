#!/usr/bin/env python3
import importlib
import os
import random
import re
import sys

# this is where to look for modules
sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'builtin/'))
from builtin import _parser, model
from builtin import _history
from builtin import show

models_dir = os.path.join(os.path.dirname(__file__), 'models/')
# allowed keywords
modules_listX = ['infant', 'person', 'carrier', 'deceased', 'exposed', 'infected', 'infection', 'mdi', 'recovered',
                 'resistant', 'susceptible', 'vaccinated', 'vaccine', 'model', 'period', 'contact', 'query', 'show',
                 'run', 'test', 'graph']
random_module = random.choice(modules_listX)
echoX = False
# welcome messages
print(_parser.welcome_message)
print(_parser.usage_message)
print(_parser.help_message)
model_file_txt = ''


def toggle_echo():
    global echoX
    if echoX:
        echoX = False
        print("will not echo commented and empty lines in the model file")
    else:
        echoX = True
        print("commented and empty lines in the model file will be echoed in the terminal")


def add_path(index, directory):
    """
    :usage: adds a path to the library directory relative to the current directory
    :param index: index where the path stored, ex. 0
    :param directory: the file name
    :return: None
    """
    sys.path.insert(index, os.path.join(os.path.dirname(__file__), directory + '/'))
    return


def module_not_available(module):
    """
    :usage: when module not found, confirms and prints available modules
    :param module: module name
    :return: None
    """
    print(_parser.not_found(module))
    _parser.print_modules(modules_listX)
    return


def available_modules(module):
    """
    :usage: finds out which functions are avilable
    :param module: module name
    :return: list of available functions in the given module
    """
    functions_list = []
    # import modules dynamically
    try:
        mod = importlib.import_module(module)
        for name in dir(mod):
            # filter out names starting with '_'
            if not name.startswith('_') or not name.endswith('X'):
                functions_list.append(name)
    except AttributeError:
        return
    except ModuleNotFoundError:
        return
    except TypeError:
        return
    except IndexError:
        return
    return functions_list


def get_args(dsl_args):
    """
    :usage: categorises the arguments given by the user
    :param dsl_args: list of user-entered arguments
    :return args: arguments given by the user
    :return kwargs: dictionary of special arguments of the form something=something
    """
    # args list
    args = []
    # kwargs dictionary
    kwargs = {}
    # splits the arguments into a list and a dictionary
    for dsl_arg in dsl_args:
        # (keyword)
        if dsl_arg.startswith('(') and dsl_arg.endswith(')'):
            kwargs[dsl_arg[1:-1]] = 'true'
        # [keyword]
        elif dsl_arg.startswith('[') and dsl_arg.endswith(']'):
            kwargs[dsl_arg[1:-1]] = 'save'
        # !keyword
        elif dsl_arg.startswith('!'):
            kwargs[dsl_arg[1:]] = "false"
        # keyword()
        elif dsl_arg.endswith('()'):
            kwargs[dsl_arg[:-2]] = 'default'
        # keyword{}
        elif dsl_arg.endswith('{}'):
            kwargs[dsl_arg[:-2]] = 'this'
        # keyword[]
        elif dsl_arg.endswith('[]'):
            kwargs[dsl_arg[:-2]] = 'all'
        # keyword!
        elif dsl_arg.endswith('!'):
            kwargs[dsl_arg[:-1]] = 'none'
        # keyword=value
        elif '=' in dsl_arg:
            k, v = dsl_arg.split('=', 1)
            # stores the arguments that come after '='
            kwargs[k] = v
        # value.keyword
        elif '.' in dsl_arg and '.txt' not in dsl_arg and '.db' not in dsl_arg and '.sql' not in dsl_arg and '.csv' not in dsl_arg and '.model' not in dsl_arg and '.ini' not in dsl_arg and dsl_arg.replace(
                '.', '').isalpha():
            v, k = dsl_arg.split('.', 1)
            # stores the arguments that come after '='
            kwargs[k] = v
        # keyword:value
        elif ':' in dsl_arg:
            k, v = dsl_arg.split(':', 1)
            # stores the arguments that come after '='
            kwargs[k] = v
        else:
            args.append(dsl_arg)
    return args, kwargs


def get_help(module_name):
    """
    :usage: prints the documentation for the given module
    :param module_name: module name
    :return: None
    """
    # import modules dynamically
    if module_name in modules_listX:
        mod = importlib.import_module(module_name)
        print(mod.__doc__ or '')
        # loop over all attribute names in the module
        for name in dir(mod):
            # filter out names starting with '_'
            if not name.startswith('_'):
                try:
                    attr = getattr(mod, name)
                    print(attr.__name__)
                    print(attr.__doc__ or '', '\n')
                except AttributeError:
                    continue
                except ModuleNotFoundError:
                    continue
                except TypeError:
                    continue
                except IndexError:
                    continue
    else:
        module_not_available(module_name)
    return


def get_function_help(module_name, function_name):
    """
    :usage: prints the documentation for the given function
    :param module_name: module name
    :param function_name: function name
    :return: True if the function name is found in the given module, false otherwise
    """
    # import modules dynamically
    found = False
    if module_name in modules_listX:
        mod = importlib.import_module(module_name)
        print(mod.__doc__ or '')
        # loop over all attribute names in the module
        for name in dir(mod):
            # filter out names starting with '_'
            if not name.startswith('_'):
                try:
                    attr = getattr(mod, name)
                    if attr.__name__ == function_name:
                        found = True
                        print(attr.__name__)
                        print(attr.__doc__ or '', '\n')
                except AttributeError:
                    continue
                except ModuleNotFoundError:
                    continue
                except TypeError:
                    continue
                except IndexError:
                    continue
    else:
        module_not_available(module_name)
    return found


def interpret(model_file):
    """
    :usage: runs a model file (.model) from the docs directory
    :param model_file: model name
    :return: None
    """
    # change the name of the .ini file to match the model name
    global model_file_txt
    try:
        with open(models_dir + model_file, 'r') as file:
            model_file_txt = ''
            filename = model_file.replace('.model', '')
            # running _parser.config_name(model_file)
            mod = importlib.import_module('_parser')
            args = [model_file]
            kwargs = {}
            getattr(mod, 'config_name', 'config_name is not available')(*args, **kwargs)
            # running model.defaults(file=filename)
            mod = importlib.import_module('model')
            args = []
            kwargs = {'file': filename}
            getattr(mod, 'defaults', 'defaults is not available')(*args, **kwargs)
            # read the file line by line
            for line in file:
                model_file_txt += line
                line = line.strip()
                # to ignore comments in code
                if not line or line[0] == '#':
                    if echoX:
                        print(line)
                    continue
                parts = line.split()
                if parts[0] in modules_listX:
                    # mod stores a reference to the modules, the first arguments is the source file containing the function
                    # given in the second argument
                    mod = importlib.import_module(parts[0])
                    # third and more are the function parameters
                    args, kwargs = get_args(parts[2:])
                    # gets a reference to the function and calls it with its arguments
                    try:
                        getattr(mod, parts[1], parts[1] + ' is not available')(*args, **kwargs)
                    except AttributeError:
                        print(parts[1] + ' is not available')
                    except ModuleNotFoundError:
                        print(parts[1] + ' is not available')
                    except TypeError:
                        print(parts[1] + ' is not available')
                    except IndexError:
                        print(parts[1],
                              'is cleared or an entry is out of range (syntax error). please, check your model!')
                elif parts[0] == 'exit' or parts[0] == 'quit':
                    quit()
                else:
                    print(parts[0] + ' is not available')
    except FileNotFoundError:
        print('file not found')
    except FileExistsError:
        print('file does not exist')
    return

def open_module(entry):
    """
    :usage: runs <module> <function> given by the user
    :param entry: list of user entry, starting with a module name
    :return: None
    """
    modules_list = available_modules(entry[0])
    if entry[0] in modules_listX or entry[0] in modules_list:
        add_path(0, 'library')
        mod = importlib.import_module(entry[0])
        args, kwargs = get_args(entry[2:])
        try:
            getattr(mod, entry[1], _parser.not_found(entry[1]))(*args, **kwargs)
        except AttributeError:
            print(_parser.not_found(entry[1]))
            _parser.print_modules(modules_list)
        except ModuleNotFoundError:
            print(_parser.not_found(entry[1]))
            _parser.print_modules(modules_list)
        except TypeError:
            print(_parser.not_found(entry[1]))
            _parser.print_modules(modules_list)
        except IndexError:
            print('usage  : <module_name> <function_name>')
            _parser.example1(entry[0], random.choice(available_modules(entry[0])))
            _parser.print_modules(modules_list)
        sys.path.pop(0)
    return


def open_addon(entry, directory):
    """
    :usage: open modules from the given directory
    :param entry: list of user entries starting with addon or $
    :param directory: directory containing the addon
    :return: None
    """
    if entry[0].startswith('addon') or entry[0].startswith('$'):
        add_path(1, directory)
        modules_list = available_modules(entry[1])
        mod = importlib.import_module(entry[1])
        args, kwargs = get_args(entry[3:])
        try:
            getattr(mod, entry[2], _parser.not_found(entry[1]))(*args, **kwargs)
        except AttributeError:
            print(_parser.not_found(entry[2]))
            _parser.print_modules(modules_list)
        except ModuleNotFoundError:
            print(_parser.not_found(entry[2]))
            _parser.print_modules(modules_list)
        except TypeError:
            print(_parser.not_found(entry[2]))
            _parser.print_modules(modules_list)
        except IndexError:
            print('usage  : <module_name> <function_name>')
            _parser.example1(entry[1], random.choice(available_modules(entry[1])))
            _parser.print_modules(modules_list)
        sys.path.pop(1)
    return


def open_sub(entry):
    """
    :usage: open modules from /module subdirectory
    :param entry: list of user entries
    :return: None
    """
    add_path(1, entry[0])
    modules_list = available_modules(entry[1])
    mod = importlib.import_module(entry[1])
    args, kwargs = get_args(entry[3:])
    try:
        getattr(mod, entry[2], _parser.not_found(entry[1]))(*args, **kwargs)
    except AttributeError:
        print(_parser.not_found(entry[2]))
        _parser.print_modules(modules_list)
    except ModuleNotFoundError:
        print(_parser.not_found(entry[2]))
        _parser.print_modules(modules_list)
    except TypeError:
        print(_parser.not_found(entry[2]))
        _parser.print_modules(modules_list)
    except IndexError:
        print('usage  : <module_name> <function_name>')
        _parser.example1(entry[1], random.choice(available_modules(entry[1])))
        _parser.print_modules(modules_list)
    sys.path.pop(1)
    return


def open_help(entry):
    """
    :usage: prints documentation of either a model or a function if given
            help <module_name> or help <module_name> <function_name>
    :param entry: list of user entrys starting with help
    :return: None
    """
    global random_module
    random_module = random.choice(modules_listX)
    # help
    if entry[0].__contains__('help') and len(entry) <= 1:
        _parser.print_help_page(modules_listX, random_module, random.choice(available_modules(random_module)))
    # help module
    elif len(entry) <= 2 and len(entry) > 1:
        if re.search('us|start|com|addon|exe|run|exit|how|clear|<|>|\\?|@|#|\\$|\\^|quit', entry[1], re.IGNORECASE):
            print(_parser.usage_message)
        elif re.search('arg|key|param|func|val|true|false|=|:|}|]|\\)|!|\\.', entry[1], re.IGNORECASE):
            print(_parser.func_message)
        elif re.search('debug|log|logger', entry[1], re.IGNORECASE):
            print(_parser.debug_message)
        elif re.search('msg|help|message|me', entry[1], re.IGNORECASE):
            print(_parser.help_message)
        elif re.search('modul|avail', entry[1], re.IGNORECASE):
            _parser.print_modules(modules_listX)
        else:
            try:
                get_help(entry[1])
            except IndexError:
                _parser.print_help_page(modules_listX, random_module, random.choice(available_modules(random_module)))
            except AttributeError:
                print(module_not_available(entry[1]))
                _parser.example0(random_module)
            except ModuleNotFoundError:
                print(module_not_available(entry[1]))
                _parser.example0(random_module)
            except TypeError:
                print(module_not_available(entry[1]))
                _parser.example0(random_module)
    # help module function
    elif len(entry) >= 3:
        modules_list = available_modules(entry[1])
        try:
            if get_function_help(entry[1], entry[2]) == False:
                print(_parser.not_found(entry[2]))
                if entry[1] in modules_listX:
                    _parser.print_modules(available_modules(entry[1]))
        except IndexError:
            _parser.help_usage()
            _parser.example1(random_module, random.choice(available_modules(random_module)))
            _parser.print_modules(modules_listX)
            print('try help <module_name> for a list of functions')
        except AttributeError:
            _parser.print_modules(available_modules(entry[1]))
            _parser.example1(entry[1], random.choice(available_modules(entry[1])))
        except ModuleNotFoundError:
            _parser.print_modules(available_modules(entry[1]))
            _parser.example1(entry[1], random.choice(available_modules(entry[1])))
        except TypeError:
            _parser.print_modules(available_modules(entry[1]))
            _parser.example1(entry[1], random.choice(available_modules(entry[1])))
    return


def history(entry):
    """
    :usage: takes navigation input and runs the corresponding action
    :param entry: list of user enteris starting with '^', '>', '<' or <some_number.>
    :return: True if the entry is supported, false otherwise
    """
    if entry[0] == '^':
        _history.last_input()
        return True
    elif entry[0] == '>':
        _history.all_inputs()
        return True
    elif entry[0] == '<':
        try:
            li = _history.last_input()
            li = str(li)
            li = li.strip()
            ent = li.split()
            new_entry = [ent[0], ent[1]]
            for i in entry[1:]:
                new_entry.append(i)
            open_module(new_entry)
        except TypeError:
            return False
        except IndexError:
            return False
        return True
    elif str(entry[0]).endswith('.'):
        new_ent = str(entry[0]).replace('.', '')
        if str(new_ent[0]).isdigit():
            try:
                new_line = _history.user_inputsX[int(new_ent[0])]
                new_line = str(new_line).strip()
                old_entry = new_line.split()
                new_entry = [old_entry[0], old_entry[1]]
                for i in entry[1:]:
                    new_entry.append(i)
                open_module(new_entry)
            except IndexError:
                print('input not available')
                return False
            except TypeError:
                return False
            return True
    return False


def special(p_line):
    """
    :usage: takes user input if it starts with some special characters '#', '?' or '!' and act upon it
    :param p_line: user input as a line of text
    :return: true is entry is supported, false otherwise
    """
    if p_line[0].startswith('#'):
        print('>> ' + p_line)
        return True
    elif p_line[0].startswith('?'):
        model.values()
        return True
    elif p_line[0].startswith('!'):
        _history.clear_inputs()
        return True

    return False


def prompt():
    while True:
        """
        this is the main function that runs the interpreter
        """
        model._save()
        line = input('>> ')
        _history.user_inputsX.append(line)
        line = line.strip()
        entry = line.split()
        if not line:
            continue
        elif line and not special(line) and not history(entry):
            if entry[0].startswith('exit') or entry[0].startswith('quit') or entry[0] == 'q':
                sys.exit("terminated successfully")
            elif entry[0].startswith('@') and len(entry) <= 3:
                show._input(entry[1], entry[2])
                continue
            elif entry[0].startswith('@') and len(entry) <= 4:
                show._param_input(entry[1], entry[2], entry[3])
                continue
            elif entry[0].startswith('clear'):
                _parser.clear_screen(entry)
                continue
            elif entry[0].startswith('echo'):
                print(model_file_txt)
                continue
            elif entry[0].startswith('help'):
                open_help(entry)
                continue
            elif entry[0].startswith('interpret') or entry[0].startswith('compile') or entry[0].startswith('sim'):
                if entry[1] == '-echo' or entry[1] == '-e':
                    toggle_echo()
                    if entry[2].endswith('.model'):
                        module_name = entry[2]
                        interpret(module_name)
                    elif len(entry[2]) > 0:
                        module_name = entry[2] + '.model'
                        interpret(module_name)
                elif entry[1] == '-clear' or entry[1] == '-c' or entry[1] == '-new' or entry[1] == '-n':
                    # running _model_parser.new()
                    mod = importlib.import_module('model')
                    args = []
                    kwargs = {}
                    getattr(mod, 'new', 'new is not available')(*args, **kwargs)
                    if entry[2].endswith('.model'):
                        module_name = entry[2]
                        interpret(module_name)
                    elif len(entry[2]) > 0:
                        module_name = entry[2] + '.model'
                        interpret(module_name)
                elif entry[1].endswith('.model'):
                    module_name = entry[1]
                    interpret(module_name)
                elif len(entry[1]) > 0:
                    module_name = entry[1] + '.model'
                    interpret(module_name)
                continue
            elif entry[0] in modules_listX:
                open_module(entry)
                continue
            elif entry[0].startswith('addon') or entry[0].startswith('$'):
                open_addon(entry, 'addon')
                continue
            elif entry[0].startswith('.'):
                entry[0].replace('.', '')
                open_sub(entry)
            else:
                module_not_available(entry[0])
                print('you can also type help <module_name> 🙂')
                _parser.example0(random_module)

# prompt()
