#!/usr/bin/env python3
from __future__ import print_function

import os
import sys

import interactive_shell
from builtin import _parser

library = os.path.join(os.path.dirname(__file__), 'builtin/')
sys.path.insert(0, library)
module_name = 'sir.model'

if len(sys.argv) != 2:
    _parser.compile_usage()
    _parser.c_help_usage()
    sys.exit(1)

# allows the user to get help
if sys.argv[1].startswith('-h') or sys.argv[1].startswith('-help'):
    interactive_shell.open_help(sys.argv[1:])
else:
    if sys.argv[1].endswith('.model'):
        module_name = sys.argv[1]
    elif len(sys.argv[1]) > 0:
        module_name = sys.argv[1] + '.model'

interactive_shell.interpret(module_name)
