import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'module/'))
#sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'module/docs/'))
from module import interactive_shell

if __name__ == '__main__':
    interactive_shell.prompt()
